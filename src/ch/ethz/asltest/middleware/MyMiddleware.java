package ch.ethz.asltest.middleware;

import ch.ethz.asltest.utils.Utils;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.SocketOption;
import java.nio.ByteBuffer;
import java.nio.channels.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.*;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * This class is responsible for accepting clients, reading requests and enqueuing them to the internal queue, worker thread initialization and logging.
 *
 */
public class MyMiddleware {
    private static final int SOMAXCONN=1024;

    public static final int BUFFER_SIZE = 4096 * 11;
    public static final int REQUEST_BUFFER_SIZE = 4096 * 2;
    //    static final int SELECTOR_TIMEOUT = 7 * 1000; // 10 seconds
    private static final int LOGGING_FREQUENCY = 1000;
    private AtomicInteger requestCount = new AtomicInteger(0);

    private static StringBuffer requestSB = new StringBuffer();
    private static String requestLogFile;

    static StringBuffer middlewareSB = new StringBuffer();
    private static String middlewareLogFile;

    private static String systemLogFile;

    private final String myIp;
    private final int myPort;
    private final List<String> mcAddresses;
    private final int numThreadsPTP;
    private final boolean readSharded;
    public static boolean working;

    private InetSocketAddress listenSocketAddress;
    private Selector clientSelector;

    private LinkedBlockingQueue<Request> internalQueue;
    private RoundRobin counter;
    private List<CacheService> cacheServices;
    private List<InetSocketAddress> mcSocketAddresses;

    private ByteBuffer buffer;

    private AtomicInteger getCounter = new AtomicInteger(0);
    private AtomicInteger setCounter = new AtomicInteger(0);
    private AtomicInteger multiGetCounter = new AtomicInteger(0);
    private AtomicInteger multigetShardedCounter = new AtomicInteger(0);
    private AtomicInteger cacheMissesCounter = new AtomicInteger(0);

    static AtomicInteger secondCounter = new AtomicInteger(0);
    static AtomicInteger systemRequests = new AtomicInteger(0);

    //    public static AtomicInteger debugCounter = new AtomicInteger(0);
//    public static AtomicInteger debugGetCounter = new AtomicInteger(0);
//    public static AtomicInteger debugSetCounter = new AtomicInteger(0);
    private HashMap<String, byte[]> hashMap;
//    public static StringBuffer debugSB = new StringBuffer();
//    public static String reqResp = "reqResp.txt";
//    public static String respFile = "resp.txt";
//    public static String requestFile = "requestFile.txt";
//    public static String threadsFile = "threads.txt";
//    public static StringBuffer respSB = new StringBuffer();
//    private static StringBuffer threadsSB = new StringBuffer();

    private static String counterLogFile;
//    static final String REQUEST_LOG_TEMPLATE = "requestType=%s,startResponseTimer=%d,stopResponseTimer=%d,startNetQueueTimer=%d,stopNetQueueTimer=%d,startProcessingTimer=%d,stopProcessingTimer=%d,startServiceTimer=%d,stopServiceTimer=%d\n";

    private static final String requestLogHead = "second,id,requestType,threadId,responseTimer,internalQueueTimer,processingTimer,serviceTimer,waitingServiceTimer,returningResponseTimer\n";
    static final String REQUEST_LOG_TEMPLATE = "%d,%d,%s,%d,%d,%d,%d,%d,%d,%d\n";

    private static final String sysLogHead = "second,availableProcessors,totalMemory,freeMemory,usedMemory,queueCapacity,queueSize,expectedArrivalRate,systemRequests\n";
    private static final String SYS_LOG_TEMPLATE = "%d,%d,%d,%d,%d,%d,%d,%f,%d\n";

    private static final String counterLogHead = "second,get,set,multiget,multigetsharded,cachemisses\n";
    private static final String COUNTER_LOG_TEMPLATE = "%d,%d,%d,%d,%d,%d\n";

    private long prevRequestNumber = -1;


    private boolean started = false;

    public MyMiddleware(String myIp, int myPort, List<String> mcAddresses, int numThreadsPTP, boolean readSharded) {
        this.myIp = myIp;
        this.myPort = myPort;
        this.mcAddresses = mcAddresses;
        this.numThreadsPTP = numThreadsPTP;
        this.readSharded = readSharded;
    }

    private void initClientCommunication() throws IOException {
        try {
            ServerSocketChannel serverSocket = ServerSocketChannel.open();
            serverSocket.configureBlocking(false);
            serverSocket.socket().bind(listenSocketAddress, SOMAXCONN);


            clientSelector = Selector.open();
            serverSocket.register(clientSelector, SelectionKey.OP_ACCEPT);

//            middlewareLogger.info("Middleware listening on " + listenSocketAddress);
            middlewareSB.append("INFO: Middleware listening on ").append(listenSocketAddress).append("\n");
        } catch (IOException e) {
            e.printStackTrace();
            middlewareSB.append("SEVERE: ").append(e.toString()).append("\n");
            shutdownCleanup();
//            middlewareLogger.severe(e.toString());
            System.exit(-1);
        }
    }

    private void initServerCommunication() throws IOException {

        for (int i = 0; i < this.numThreadsPTP; i++) {
            Selector tmpSelector = Selector.open();

            ArrayList<SocketChannel> tmpSocketChannels = new ArrayList<>();
            for (InetSocketAddress mcAddress : mcSocketAddresses) {
                SocketChannel tmpChannel = SocketChannel.open(mcAddress);
//                printOptions(tmpChannel);

                tmpChannel.configureBlocking(false);
                tmpChannel.register(tmpSelector, SelectionKey.OP_READ);
                tmpSocketChannels.add(tmpChannel);
            }
            cacheServices.add(new CacheService(internalQueue, tmpSocketChannels, tmpSelector,
                    counter, readSharded, getCounter, setCounter, multiGetCounter, multigetShardedCounter, cacheMissesCounter));
        }

        for (CacheService cacheService : cacheServices) {
            cacheService.init();
            cacheService.start();
        }
    }

    public void init() throws IOException {
        working = true;
        listenSocketAddress = new InetSocketAddress(myIp, myPort);
        internalQueue = new LinkedBlockingQueue<>();
        counter = new RoundRobin(mcAddresses.size());
        cacheServices = new ArrayList<>();
        hashMap = new HashMap<>();

        buffer = ByteBuffer.allocate(REQUEST_BUFFER_SIZE);


        String fileAppendix = Utils.getFileAppendix(numThreadsPTP, readSharded);
        requestLogFile = "request" + fileAppendix + ".csv";
        middlewareLogFile = "info" + fileAppendix + ".txt";
        counterLogFile = "counter" + fileAppendix + ".txt";
        systemLogFile = "sys" + fileAppendix + ".csv";

        // create log files
        PrintWriter pw = new PrintWriter(requestLogFile);
        pw.write(requestLogHead);
        pw.close();

        pw = (new PrintWriter(systemLogFile));
        pw.write(sysLogHead);
        pw.close();

        pw = (new PrintWriter(counterLogFile));
        pw.write(counterLogHead);
        pw.close();

        (new PrintWriter(middlewareLogFile)).close();
//        (new PrintWriter(requestFile)).close();
//        (new PrintWriter(respFile)).close();
//        (new PrintWriter(threadsFile)).close();

        mcSocketAddresses = new ArrayList<>();
        for (String address : mcAddresses) {
            String[] tmp = address.split(":");
            mcSocketAddresses.add(new InetSocketAddress(tmp[0], Integer.parseInt(tmp[1])));
        }

        initClientCommunication();
        initServerCommunication();
    }

    private void register(SelectionKey key) throws IOException {
        ServerSocketChannel serverChannel = (ServerSocketChannel) key.channel();
        SocketChannel channel = serverChannel.accept();
        channel.configureBlocking(false);
        channel.register(clientSelector, SelectionKey.OP_READ);
//        channel.socket().setSoLinger(true, 10); // 10 seconds

        String addr = channel.socket().getRemoteSocketAddress().toString();
        hashMap.put(addr, null);
        middlewareSB.append("INFO: Middleware new listening for requests from client ").append(addr).append("\n");
//        middlewareLogger.info("Middleware new listening for requests from client " + channel.socket().getRemoteSocketAddress());
    }

    private void flushLoggers() throws IOException {
        String requestContent, middlewareContent, respContent; //, debugContent, , threadsContent;
        synchronized (this) {
            requestContent = requestSB.toString();
            requestSB.setLength(0);
        }
        synchronized (this) {
            middlewareContent = middlewareSB.toString();
            middlewareSB.setLength(0);
        }
//        synchronized (this){
//            debugContent = debugSB.toString();
//            debugSB.setLength(0);
//        }
//
//        synchronized (this){
//            respContent = respSB.toString();
//            respSB.setLength(0);
//        }
//        synchronized (this){
//            threadsContent = threadsSB.toString();
//            threadsSB.setLength(0);
//        }

        String counterContent = String.format(COUNTER_LOG_TEMPLATE, secondCounter.get(), getCounter.get(), setCounter.get(), multiGetCounter.get(), multigetShardedCounter.get(), cacheMissesCounter.get());

        Files.write(Paths.get(requestLogFile), requestContent.getBytes(), StandardOpenOption.APPEND);
        Files.write(Paths.get(middlewareLogFile), middlewareContent.getBytes(), StandardOpenOption.APPEND);
        Files.write(Paths.get(counterLogFile), counterContent.getBytes(), StandardOpenOption.APPEND);
//        Files.write(Paths.get(respFile), respContent.getBytes(), StandardOpenOption.APPEND);
//        Files.write(Paths.get(threadsFile), threadsContent.getBytes(), StandardOpenOption.APPEND);
    }

    private void recordMonitoring() throws IOException {
        Runtime runtime = Runtime.getRuntime();
        long availableProcessors = runtime.availableProcessors();
        long totalMemory = runtime.totalMemory();
        long freeMemory = runtime.freeMemory();
        long usedMemory = totalMemory - freeMemory;
        long queueCapacity = internalQueue.remainingCapacity();
        long queueSize = internalQueue.size();

        float expRate = 0;
        int currentRequest=requestCount.get();
        if (prevRequestNumber !=0){
            expRate =(currentRequest- prevRequestNumber)/(float)LOGGING_FREQUENCY;
        }
        prevRequestNumber = currentRequest;

        String log = String.format(SYS_LOG_TEMPLATE, secondCounter.get(), availableProcessors, totalMemory, freeMemory, usedMemory, queueCapacity, queueSize, expRate, systemRequests.get());
        Files.write(Paths.get(systemLogFile), log.getBytes(), StandardOpenOption.APPEND);
    }

    private void startLogging() {

        Timer t = new Timer();
        t.schedule(new TimerTask() {
            @Override
            public void run() {
                try {
//                        System.out.println("debugCounter="+debugCounter);
//                        System.out.println("debugGetCounter="+debugGetCounter);
//                        System.out.println("debugSetCounter="+debugSetCounter);
//                        System.out.println("internalQueue.size="+ internalQueue.size());
//                        System.out.println("internalQueue.remainingCapacity="+ internalQueue.remainingCapacity());
//                        long heapsize = Runtime.getRuntime().totalMemory();
//                        long usedMem = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
//                        usedMem /= ((long) (1024.0 * 1024.0 * 1024.0));
//                        System.out.println("usedMem="+ usedMem+"GiB");
//                        System.out.println("################################\n\n");
                    secondCounter.incrementAndGet();
                    recordMonitoring();
                    flushLoggers();
                } catch (IOException e) {
                    e.printStackTrace();
                    System.exit(-1);
                }
            }
        }, LOGGING_FREQUENCY, LOGGING_FREQUENCY);
    }
    static void printOptions(NetworkChannel channel) throws IOException {
        System.out.println(channel.getClass().getSimpleName() + " supports:");
        for (SocketOption<?> option : channel.supportedOptions()) {
            System.out.println(option.name() + ": " + channel.getOption(option));
        }
        System.out.println();
//        channel.close();
    }
    public void run() throws IOException {
        System.out.println("Middleware is ready");
        middlewareSB.append("Middleware is ready\n");
        try {
            while (working) {
                clientSelector.select(500);
                Set<SelectionKey> selectedKeys = clientSelector.selectedKeys();
                Iterator<SelectionKey> iter = selectedKeys.iterator();
                while (iter.hasNext()) {
                    SelectionKey key = iter.next();
                    iter.remove();

                    if (!key.isValid()) continue;

                    if (key.isAcceptable()) {
                        register(key);
                        if (!started) {
                            started = true;
                            startLogging();
                        }
                    } else if (key.isReadable()) {
                        readRequest(key);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
//            middlewareLogger.severe(e.toString());
            middlewareSB.append("SEVERE: ").append(e.toString()).append("\n");
            shutdownCleanup();
            System.exit(-1);
        }
    }

    public void stop() throws IOException {
        shutdownCleanup();
        working = false;
    }

    private void shutdownCleanup() throws IOException {
        System.out.println("Turning off caches");
        for (CacheService cacheService : cacheServices) {
            cacheService.turnoff();
        }
        System.out.println("Flushing loggers");
        flushLoggers();
        System.out.println("Saving counters");
        System.out.println("Exiting");
    }

    private void readRequest(SelectionKey key) throws Exception {
        SocketChannel client = (SocketChannel) key.channel();
        buffer.clear();
        int size = client.read(buffer);


        if (size < 0) {
            middlewareSB.append("INFO: Client connection closed ").append(client.socket().getRemoteSocketAddress()).append("\n");
            client.close();
            key.cancel();
            return;
        }
        String addr = client.socket().getRemoteSocketAddress().toString();

        buffer.flip();
        byte[] data = new byte[size];
        System.arraycopy(buffer.array(), 0, data, 0, size);

        if (hashMap.get(addr) != null) {
            data = Utils.concatenate(hashMap.get(addr), data);
//            hashMap.put(addr, data);
        }

        if (Utils.isRequestComplete(data)) {
            systemRequests.incrementAndGet();
            Request request = new Request(requestCount.getAndIncrement(), client, data, requestSB);

            request.startResponse();
            request.startInternalQueue();
            internalQueue.add(request);
            hashMap.put(addr, null);
        } else{
            hashMap.put(addr, data);
//            middlewareSB.append("WARNING: hashMap.put(addr, data);\t").append(client.socket().getRemoteSocketAddress()).append("\n");
//            middlewareSB.append("\ndata=").append(new String(data)).append("\n");
        }
    }

    static byte[] readChannel(SocketChannel channel, ByteBuffer buffer, byte[] prevData) throws Exception {
        int bufferSize;
        byte[] tmpData;
        buffer.clear();
        while ((bufferSize = channel.read(buffer)) > 0) {
            buffer.flip();
            tmpData = new byte[bufferSize];
            System.arraycopy(buffer.array(), 0, tmpData, 0, bufferSize);
            buffer.clear();

            prevData = prevData == null ? tmpData : Utils.concatenate(prevData, tmpData);
        }
        return prevData;
    }

}
