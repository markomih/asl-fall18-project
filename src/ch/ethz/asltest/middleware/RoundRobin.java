package ch.ethz.asltest.middleware;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * This class is atomic round robin counter
 *
 */
public class RoundRobin {
    private AtomicInteger c = new AtomicInteger(0);
    private final int size;

    public RoundRobin(int size) {
        this.size = size;
    }

    public int next() {
        return c.getAndUpdate(i -> i=(i+1)%size);
    }
}
