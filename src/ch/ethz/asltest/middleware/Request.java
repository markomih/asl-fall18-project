package ch.ethz.asltest.middleware;


import java.nio.channels.SocketChannel;
import java.util.logging.Logger;
/**
 * This class represents client's request with additional timers that are used for logging.
 *
 */
public class Request {
    private long id;

    private final SocketChannel channel;
    private final String content;
    private final byte[] byteContent;
    private RequestType requestType;
    private int mcServerId = -1;

    private StringBuffer requestSB;

//    private long startResponseTimer;
//    private long stopResponseTimer;
//
//    private long startNetQueueTimer;
//    private long stopNetQueueTimer;
//
//    private long startProcessingTimer;
//    private long stopProcessingTimer;
//
//    private long startServiceTimer;
//    private long stopServiceTimer;
//
//    private long startWaitingService;
//
//    private long startPostProcessingTimer;
//    private long stopPostProcessingTimer;
    private long responseTimer;
    private long internalQueueTimer;
    private long processingTimer;
    private long serviceTimer;
    private long waitingServiceTimer;
    private long returningResponseTimer;

//    private long startTime, leaveTime;

    private byte[] response;

    public Request(long id, SocketChannel channel, byte[] byteContent, StringBuffer requestSB) {
        this.id = id;
        this.channel = channel;
        this.byteContent = byteContent;
        this.content = new String(byteContent);
        this.requestSB = requestSB;
    }

    public void setResponse(byte[] response) {
        this.response = response;
    }

    public byte[] getResponse() {
        return response;
    }

    void setRequestType(RequestType requestType) {
        this.requestType = requestType;
    }

    RequestType getRequestType() {
        return requestType;
    }

    public String getContent() {
        return content;
    }

    void setMcServerId(int mcServerId) {
        this.mcServerId = mcServerId;
    }

    public byte[] getByteContent() {
        return byteContent;
    }

    SocketChannel getChannel() {
        return channel;
    }

    int getMcServerId() {
        return mcServerId;
    }


//    void startProcessing() {
//        startProcessingTimer = System.currentTimeMillis();
//    }
//
//    void stopProcessing() {
//        stopProcessingTimer = System.currentTimeMillis();
//    }
//
//    void startPostProcessing() {
//        startPostProcessingTimer = System.currentTimeMillis();
//    }
//
//    void stopPostProcessing() {
//        stopPostProcessingTimer = System.currentTimeMillis();
//    }
//
//    void startNetQueue() {
//        startNetQueueTimer = System.currentTimeMillis();
//    }
//
//    void stopNetQueue() {
//        stopNetQueueTimer = System.currentTimeMillis();
//    }
//
//    void startResponse() {
//        startResponseTimer = System.currentTimeMillis();
//    }
//
//    long getStartResponseTimer(){
//        return startResponseTimer;
//    }
//
//    void stopResponse() {
//        stopResponseTimer = System.currentTimeMillis();
//    }
//
//    void startService() {
//        startServiceTimer = System.currentTimeMillis();
//    }
//
//    void stopService() {
//        stopServiceTimer = System.currentTimeMillis();
//    }

    long getId() {
        return id;
    }

    void logData() {
        long threadId = Thread.currentThread().getId();
        String tmp = String.format(MyMiddleware.REQUEST_LOG_TEMPLATE,
                MyMiddleware.secondCounter.get(), id, this.getRequestType().name(),(int) threadId,
                responseTimer, internalQueueTimer, processingTimer, serviceTimer,waitingServiceTimer, returningResponseTimer
                );
        requestSB.append(tmp);
    }

    void startResponse() {
//        startTime=System.currentTimeMillis();
        responseTimer = System.currentTimeMillis();//System.currentTimeMillis();
    }

    void stopResponse() {
        responseTimer = System.currentTimeMillis() - responseTimer;
//        leaveTime=System.currentTimeMillis();
    }
    void startInternalQueue() {
        internalQueueTimer = System.currentTimeMillis();
    }

    void stopInternalQueue() {
        internalQueueTimer = System.currentTimeMillis() - internalQueueTimer;
    }

    void startProcessing() {
        processingTimer=System.currentTimeMillis();
    }

    void stopProcessing() {
        processingTimer=System.currentTimeMillis()-processingTimer;
    }

    void startService() {
        serviceTimer=System.currentTimeMillis();
    }

    void stopService() {
        serviceTimer=System.currentTimeMillis()-serviceTimer;
    }

    void startWaitingService() {
        waitingServiceTimer = System.currentTimeMillis();
    }

    void stopWaitingService() {
        waitingServiceTimer = System.currentTimeMillis()-waitingServiceTimer;
    }

    void startReturningResponse() {
        returningResponseTimer = System.currentTimeMillis();
    }

    void stopReturningResponse() {
        returningResponseTimer = System.currentTimeMillis()-returningResponseTimer;
    }
}
