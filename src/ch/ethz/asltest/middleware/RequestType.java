package ch.ethz.asltest.middleware;

/**
 * Enum with supported request types
 *
 */
public enum RequestType {
    GET,
    MULTI_GET,
    MULTI_GET_SHARDED,
    SET
}
