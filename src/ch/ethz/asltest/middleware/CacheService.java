package ch.ethz.asltest.middleware;

import ch.ethz.asltest.utils.ServerFaultException;
import ch.ethz.asltest.utils.Utils;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.util.*;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

import static ch.ethz.asltest.utils.Utils.getRequestType;

/**
 * This class is responsible for communication with memchaced servers
 *
 * It's subscribed to the internal queue. It supports SET, GET, MULTI_GET, and MULTI_GET_SHARDED requests.
 *
 * When the results from memcached are aggregated they are returned to corresponding client, and the statistic is updated
 */
public class CacheService extends Thread {
    private final LinkedBlockingQueue<Request> internalQueue;
    private final ArrayList<SocketChannel> serviceChannels;
    private final Selector serverSelector;
    private final RoundRobin counter;
    private boolean working;
    private boolean readSharded;
    private ByteBuffer byteBuffer;
    private ByteBuffer requestBuffer;
    private byte[] tmpBuffer;

    private AtomicInteger getCounter, setCounter, multiGetCounter, multigetShardedCounter, cacheMissesCounter;
    private HashMap<String, byte[]> hashMap;

    CacheService(LinkedBlockingQueue<Request> internalQueue, ArrayList<SocketChannel> serviceChannels, Selector serverSelector, RoundRobin counter, boolean readSharded, AtomicInteger getCounter, AtomicInteger setCounter, AtomicInteger multiGetCounter, AtomicInteger multigetShardedCounter, AtomicInteger cacheMissesCounter) {
        this.internalQueue = internalQueue;
        this.serviceChannels = serviceChannels;
        this.serverSelector = serverSelector;
        this.counter = counter;
        this.readSharded = readSharded;

        this.getCounter = getCounter;
        this.setCounter = setCounter;
        this.multiGetCounter = multiGetCounter;
        this.multigetShardedCounter = multigetShardedCounter;
        this.cacheMissesCounter = cacheMissesCounter;

        working = true;
    }

    void init() {
        hashMap = new HashMap<>();
        byteBuffer = ByteBuffer.allocate(MyMiddleware.BUFFER_SIZE);
        requestBuffer = ByteBuffer.allocate(MyMiddleware.REQUEST_BUFFER_SIZE);

        tmpBuffer = new byte[MyMiddleware.BUFFER_SIZE];

        for (SocketChannel channel : serviceChannels) {
            hashMap.put(channel.socket().getRemoteSocketAddress().toString(), null);
        }
    }

    synchronized void turnoff() {
        working = false;
    }

    @Override
    public void run() {
        try {
            while (working) {
                Request request = internalQueue.take();
                request.stopInternalQueue();
                request.startProcessing();

                RequestType requestType = getRequestType(request.getContent(), readSharded);
                request.setRequestType(requestType);

                if (requestType != RequestType.SET)
                    request.setMcServerId(counter.next());

                switch (request.getRequestType()) {
                    case SET: {
                        sendSetRequest(request);
                    }
                    break;
                    case GET: {
                        sendGetRequest(request);
                    }
                    break;
                    case MULTI_GET: {
                        sendGetRequest(request);
                    }
                    break;
                    case MULTI_GET_SHARDED: {
                        sendShardedMultiGetRequest(request);
                    }
                    break;
                    default:
                        throw new Exception("Invalid command type");
                }

//                ByteBuffer buffer = ByteBuffer.wrap(request.getResponse());
//                request.getChannel().write(ByteBuffer.wrap(request.getResponse()));
//                while (buffer.hasRemaining()) channel.write(buffer);
                writeResponseToChannel(request.getChannel(), request);
                request.stopReturningResponse();
                request.stopResponse();
                counteStatistics(request);
                MyMiddleware.systemRequests.decrementAndGet();
                request.logData();

//                String tmp = request.getId()+"\t"+Thread.currentThread().getId()+"\t"+(new String(request.getResponse()))+"\n\n";
//                MyMiddleware.respSB.append(tmp);
            }
        } catch (Exception e) {
            e.printStackTrace();
            MyMiddleware.middlewareSB.append("ERROR: thread exiting"+e.toString());
            System.out.println("Thread exiting");
            System.exit(-1);
        }
    }

    private void writeResponseToChannel(SocketChannel channel, Request request) throws Exception {
        byteBuffer.clear();
        byteBuffer.put(request.getResponse());
        byteBuffer.flip();
        while (byteBuffer.hasRemaining())
        {
            int bufsize = channel.write(byteBuffer);
            if (bufsize==0){
                throw new Exception("BUFFSize==0\t"+request.getId());
            }
            if (bufsize!=request.getResponse().length){
                throw new Exception("bufsize!=request.getResponse().length\t"+request.getId());
            }
        }
    }

    private void writeRequestToChannel(SocketChannel channel, Request request) throws Exception {
        requestBuffer.clear();
        requestBuffer.put(request.getByteContent());
        requestBuffer.flip();
        while (requestBuffer.hasRemaining()){
            int bufsize = channel.write(requestBuffer);
            if (bufsize==0){
                throw new Exception("BUFFSize==0\t"+request.getId());
            }
        }
    }
    private void counteStatistics(Request request) {
        switch (request.getRequestType()) {
            case SET: {
                setCounter.incrementAndGet();
            }
            break;
            case GET: {
                getCounter.incrementAndGet();

                String resp = new String(Arrays.copyOfRange(request.getResponse(), 0, 3));
                if (resp.equals("END")) {
                    cacheMissesCounter.incrementAndGet();
                }
            }
            break;
            default: {
                int getKeys = Utils.getNumberOfKeys(request.getContent());
                int returnedValues = Utils.getNumberOfResponses(new String(request.getResponse()));
                int misses = getKeys - returnedValues;

                if (readSharded) {
                    multigetShardedCounter.incrementAndGet();
                    getCounter.addAndGet(getKeys);
                    if (misses > 0)
                        cacheMissesCounter.addAndGet(misses);
                } else {
                    multiGetCounter.incrementAndGet();
                    getCounter.addAndGet(getKeys);
                    if (misses > 0)
                        cacheMissesCounter.addAndGet(misses);
                }
            }
        }
    }

    private void sendGetRequest(Request request) throws Exception {
//        MyMiddleware.debugGetCounter.incrementAndGet();

        SocketChannel channel = serviceChannels.get(request.getMcServerId());
        // write
        request.stopProcessing();
        request.startService();
        writeRequestToChannel(channel, request);
        request.stopService();
        request.startWaitingService();

        byte[] data = null;
        while (!Utils.isGetDone(data)) {
            serverSelector.select(); // blocking
            Set<SelectionKey> selectedKeys = serverSelector.selectedKeys();
            if (selectedKeys.size() == 0){
                String tmp = "SEVERE GET:"+ request.getId()+"\nrequest="+request.getContent();
                System.out.println(tmp);
                MyMiddleware.middlewareSB.append(tmp);
            }
            Iterator<SelectionKey> keys = selectedKeys.iterator();
            while (keys.hasNext()) {
                SelectionKey key = keys.next();
                keys.remove();

                if (!key.isValid()) continue;

                if (key.isAcceptable()) throw new ServerFaultException("ERROR set -- key is not acceptable");

                if (key.isReadable()) {
                    data = MyMiddleware.readChannel((SocketChannel) key.channel(), byteBuffer, data);
                }
            }
        }

        request.stopWaitingService();
        request.startReturningResponse();

        request.setResponse(data);
//        MyMiddleware.debugGetCounter.decrementAndGet();
    }

    private void sendSetRequest(Request request) throws Exception {
//        MyMiddleware.debugSetCounter.incrementAndGet();
        request.stopProcessing();
        request.startService();
        for (SocketChannel socketChannel : serviceChannels) { // broadcast set request to all services
            writeRequestToChannel(socketChannel, request);
        }
        request.stopService();

        request.startWaitingService();

//        MyMiddleware.printOptions(serviceChannels.get(0));
        int numberOfAnswers = 0;
        while (numberOfAnswers != serviceChannels.size()) {
            serverSelector.select(); // blocking
            Set<SelectionKey> selectedKeys = serverSelector.selectedKeys();
            if (selectedKeys.size() == 0){
                String tmp = "SEVERE SET:"+ request.getId()+"\nrequest="+request.getContent();
                System.out.println(tmp);
                MyMiddleware.middlewareSB.append(tmp);
            }
            Iterator<SelectionKey> keys = selectedKeys.iterator();
            while (keys.hasNext()) {
                SelectionKey key = keys.next();
                keys.remove();

                if (!key.isValid()) continue;

                if (key.isAcceptable()) throw new ServerFaultException("ERROR set -- key is not acceptable");

                if (key.isReadable()) {
                    SocketChannel socketChannel = (SocketChannel) key.channel();
                    String addr = socketChannel.socket().getRemoteSocketAddress().toString();

                    byte[] data = MyMiddleware.readChannel(socketChannel, byteBuffer, hashMap.get(addr));
                    if (Utils.isSetDone(data)) {
                        hashMap.put(addr, null);
                        numberOfAnswers++;
                    } else {
                        hashMap.put(addr, data);
                        String response = new String(data);
                        String tmpOutput = "WARNING: SET response receiving in multiple packets\trequest=" + request.getContent() + "\nresponse=" + response + "\n";
                        MyMiddleware.middlewareSB.append(tmpOutput);
                    }
                }
            }
        }
        request.stopWaitingService();
        request.startReturningResponse();

        request.setResponse(Utils.SET_END);
//        MyMiddleware.debugSetCounter.decrementAndGet();
    }

    private void sendShardedMultiGetRequest(Request request) throws Exception {
//        MyMiddleware.debugCounter.incrementAndGet();
        List<Request> requests = Utils.splitMultiGetRequest(request, serviceChannels.size());

        int numberOfRequests = requests.size(), numberOfAnswers = 0;
        int serviceChannel = request.getMcServerId();

        List<String> order = new LinkedList<>();

        request.stopProcessing();
        request.startService();
        for (Request request1 : requests) {
            order.add(serviceChannels.get(serviceChannel).socket().getRemoteSocketAddress().toString());
            writeRequestToChannel(serviceChannels.get(serviceChannel), request1);
            serviceChannel = (serviceChannel + 1) % serviceChannels.size();
        }
        request.stopService();
        request.startWaitingService();

        while (numberOfAnswers != numberOfRequests) {
            serverSelector.select(); // blocking
            Set<SelectionKey> selectedKeys = serverSelector.selectedKeys();

            Iterator<SelectionKey> keys = selectedKeys.iterator();
            while (keys.hasNext()) {
                SelectionKey key = keys.next();
                keys.remove();

                if (!key.isValid()) continue;

                if (key.isAcceptable()) throw new ServerFaultException("ERROR set -- key is not acceptable");

                if (key.isReadable()) {
                    SocketChannel channel = (SocketChannel) key.channel();
                    String addr = channel.socket().getRemoteSocketAddress().toString();

                    byte[] finallData = MyMiddleware.readChannel(channel, byteBuffer, hashMap.get(addr));

                    if (Utils.isGetDone(finallData)) {
                        numberOfAnswers++;
                    }
                    hashMap.put(addr, finallData);
                }
            }
        }
        request.stopWaitingService();
        request.startReturningResponse();

        int index = 0;
        for (String addr : order) {
            byte[] data = hashMap.get(addr);
            System.arraycopy(data, 0, tmpBuffer, index, data.length - 5);
            index = index + data.length - 5;
            hashMap.put(addr, null);
        }
        System.arraycopy(Utils.GET_END, 0, tmpBuffer, index, 5);
        index += 5;


        request.setResponse(Arrays.copyOfRange(tmpBuffer, 0, index));

//        MyMiddleware.threadsSB.append(threadLog);
    }


}
