package ch.ethz.asltest.utils;

/**
 * Exceptions that are occasionaly used by middleware
 *
 */
public class ServerFaultException extends Exception{
    public ServerFaultException(String s) {
        super(s);
    }
}
