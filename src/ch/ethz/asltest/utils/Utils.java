package ch.ethz.asltest.utils;

import ch.ethz.asltest.middleware.Request;
import ch.ethz.asltest.middleware.RequestType;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * Helper static functions for splitting and aggregating requests functions
 *
 */
public class Utils {
    public final static byte[] GET_END = "END\r\n".getBytes();
    public final static byte[] SET_END = "STORED\r\n".getBytes();

    public final static byte[] GET = "get".getBytes();
    public final static byte[] SET = "set".getBytes();
    public final static byte[] RN = "\r\n".getBytes();


    public static RequestType getRequestType(String request, boolean shardedMode) throws Exception {
        if (request.startsWith("SET") || request.startsWith("set")) {
            return RequestType.SET;
        }
        if (request.startsWith("GET") || request.startsWith("get")) {
            int start;
            for (start = 4; start < request.length() - 1; start++) {
                if (request.charAt(start) == '\r') {
                    return RequestType.GET;
                }

                if (request.charAt(start) == ' ') {
                    return shardedMode ? RequestType.MULTI_GET_SHARDED : RequestType.MULTI_GET;
                }
            }
        }
        throw new Exception("Invalid request type");
    }

    public static int getNumberOfKeys(String request){
        String query = request.trim();
        String keyString = query.substring(4, query.length());

        int keys = 1;
        for (int i = 0; i < keyString.length(); i++) {
            if (keyString.charAt(i) == ' ') keys++;
        }
        return keys;
    }
    public static int getNumberOfResponses(String request){
        String sub="VALUE";
        int n = 0, m = 0, counter = 0, counterSub = 0;
        while(n < request.length()){
            counter = 0;
            m = 0;
            while(m < sub.length() && request.charAt(n) == sub.charAt(m)){
                counter++;
                m++; n++;
            }
            if (counter == sub.length()){
                counterSub++;
                continue;
            }
            else if(counter > 0){
                continue;
            }
            n++;
        }
        return counterSub;
    }

    public static List<Request> splitMultiGetRequest(Request request, int servers) {
        String query = request.getContent().trim();
        String keyString = query.substring(4, query.length());

        int keys = 1;
        for (int i = 0; i < keyString.length(); i++) {
            if (keyString.charAt(i) == ' ') keys++;
        }
        int fullRequests = (int) (keys / servers);
        int residual = keys % servers;

        List<Request> requests = new LinkedList<>();
        int i1 = 0, i2;
        while (i1 < keyString.length()) {
            int offset = residual == 0 ? fullRequests : fullRequests + 1;
            if (residual != 0) residual--;

            i2 = i1 + 1;
            for (int i = 0; i < offset; i++) {
                while (i2 < keyString.length() && keyString.charAt(i2) != ' ') i2++;
                if (i2 < keyString.length()) i2++;
            }

            String newReq = "get " + keyString.substring(i1, i2).trim() + "\r\n";
            Request newRequest = new Request(0, null, newReq.getBytes(), null);
            requests.add(newRequest);
            i1 = i2;
        }
        return requests;
    }
    public static String getFileAppendix(int numThreadsPTP, boolean readSharded){
        return "_log_"+numThreadsPTP+"_"+readSharded;
    }
    public static byte[] concatenate(byte[]a, byte[]b){
        byte[] c = new byte[a.length + b.length];
        System.arraycopy(a, 0, c, 0, a.length);
        System.arraycopy(b, 0, c, a.length, b.length);
        return c;
    }
    public static boolean isGetDone(byte[] data){
        if (data==null)
            return false;

        return Arrays.equals(Arrays.copyOfRange(data, data.length-5, data.length), GET_END);
    }
    public static boolean isSetDone(byte[] data){
        if (data==null)
            return false;

        return Arrays.equals(data, SET_END);
    }

    public static boolean isRequestComplete(byte[] data) {
        if (data.length < 4)
            return false;

//        if (data[data.length - 2] == RN[0] && data[data.length - 1] == RN[1]) return true;
//        return false;
//    }
        int i;
        byte[] type = Arrays.copyOfRange(data, 0, 3);
        if (Arrays.equals(type, GET)){
            for (i=0;i < data.length-1; i++)
                if (data[i]==RN[0] && data[i+1]==RN[1]) break;

            return i==data.length-2;

        } else if (Arrays.equals(type, SET)){
            for (i=0;i < data.length-1; i++)
                if (data[i]==RN[0] && data[i+1]==RN[1])
                    break;

            if (i++==data.length-1)
                return false;

            for (;i < data.length-1; i++)
                if (data[i]==RN[0] && data[i+1]==RN[1]) break;

            return i==data.length-2;
        }

        return false;
    }

    public static String multiGetResponseAggregation(List<String> responses, List<String> keyPairs) throws Exception {
        StringBuilder sb = new StringBuilder();
        ArrayList<String> resp = new ArrayList<>();
        for (String tmp : responses) {
            String t = tmp.trim();
            t = tmp.substring(0, t.length() - 3); // END removed
            if (!t.equals(""))
                resp.add(t);
        }
        for (int i = 0; i < keyPairs.size(); i++) {
            String keys[] = keyPairs.get(i).split(" ");
            boolean toBrake = false;
            for (String key : keys) {
                for (int j = 0; j < resp.size(); j++) {
                    if (resp.get(j).contains("VALUE " + key)) {
                        sb.append(resp.get(j));
                        resp.remove(j);
                        toBrake = true;
                        break;
                    }
                }
                if (toBrake) break;
            }
        }
//        if (resp.isEmpty()) throw new Exception("Invalid mutli-get output");

        sb.append("END\r\n");
        return sb.toString();
    }
}
