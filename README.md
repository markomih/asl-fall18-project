# Advanced Systems Lab 2018

Majority of middleware log files are ommited due to gitlab storage limit. They are all saved on my local machine.  

Source code is located in asl-fall18-project src/ch/ethz/asltest, while log files and offline scripts are in "resources" director.

"resources" direcotry compormises of "experiment_[1-10]", which contains log files and bash scripts used for running experiments, and "src_[1-10]" that contains python scripts and graphs for the corresponing experiment indexed by the number. The exception is "00_RR" dicrectory that contains files for load distribution empirical proof and "08_experiment_poisson" that contains log files for Poisson distribution proof. 

Clint log files are in format log-[number of clients per thread]-[workers].txt located in subdirectories for request type, repetitions, and client virutal machine. 

Server log files are in server[1-3] directory in format "server[VM number]_[workers]_[request type]_[repetition]_[clients per thread].csv".

Middleware files are in MW[1-2] directory, each contains subdirecotry "MW_[request type]_[workers]_[repetition]_[clients per thread]" contains 4 files generaged by middleware and 1 by dstat.

Some experiment files contain recorded ping between machnes. 


