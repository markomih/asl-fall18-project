import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt


# def plot_queue_size(df, dst,threads, x_label='Number of clients', y_label='Internal Queue size', legend='threads'):
#     tmp_df=df[df['mw_thread']==threads]
#     data = pd.DataFrame()
#     data[x_label] = tmp_df['client'] * CLIENT_FACTOR
#     data[y_label] = tmp_df['mw_queueLength']
#     data[legend] = tmp_df['mw_thread']
#
#     fig = plt.figure()
#     ax = fig.add_subplot(1, 1, 1)
#
#     if threads==8:
#         color=0
#     elif threads==64:
#         color=3
#     sns.lineplot(x=x_label, y=y_label, hue=legend, data=data, legend="full", ci=CI, marker=MARKER,palette=[PALETTE[color]])
#
#     x_thicks = [x for x in sorted(plt.xticks()[0].tolist() + [data[x_label].min()]) if x >= data[x_label].min()]
#     ax.set_xticks(x_thicks)
#
#     ax.set_ylim(bottom=0, top=180)
#     ax.set_xlim(left=0, right=data[x_label].max() + 1)
#
#     ax.grid(True)
#     fig.show()
#     fig.savefig(dst)
def plot_queue_size(df, dst, x_label='Number of clients', y_label='Internal Queue size', legend='threads'):
    data = pd.DataFrame()
    data[x_label] = df['client'] * CLIENT_FACTOR
    data[y_label] = df['mw_queueLength']
    data[legend] = df['mw_thread']

    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)
    sns.lineplot(x=x_label, y=y_label, hue=legend, data=data, legend="full", ci=CI, marker=MARKER, palette=PALETTE)

    x_thicks = [x for x in sorted(plt.xticks()[0].tolist() + [data[x_label].min()]) if x >= data[x_label].min()]
    ax.set_xticks(x_thicks)

    ax.set_ylim(bottom=0)
    ax.set_xlim(left=0, right=data[x_label].max() + 1)

    ax.grid(True)
    fig.show()
    fig.savefig(dst)


def plot_cpu_idle(df, dst, x_label='Number of clients', y_label='CPU idle percentage', legend='threads'):
    data = pd.DataFrame()
    data[x_label] = df['client'] * CLIENT_FACTOR
    data[y_label] = df['mw_idl']
    data[legend] = df['mw_thread']

    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)
    sns.lineplot(x=x_label, y=y_label, hue=legend, data=data, legend="full", ci=CI, marker=MARKER, palette=PALETTE)

    x_thicks = [x for x in sorted(plt.xticks()[0].tolist() + [data[x_label].min()]) if x >= data[x_label].min()]
    ax.set_xticks(x_thicks)

    ax.set_ylim(bottom=0, top=100)
    ax.set_xlim(left=0, right=data[x_label].max() + 1)

    ax.grid(True)
    fig.show()
    fig.savefig(dst)


def plot_memory(df, dst, x_label='Number of clients', y_label='Memory consumed by middleware [MBs]', legend='threads'):
    data = pd.DataFrame()
    data[x_label] = df['client'] * CLIENT_FACTOR
    data[y_label] = df['mw_usedMemory'] * (1./2**20)
    data[legend] = df['mw_thread']

    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)
    sns.lineplot(x=x_label, y=y_label, hue=legend, data=data, legend="full", ci=CI, marker=MARKER, palette=PALETTE)

    x_thicks = [x for x in sorted(plt.xticks()[0].tolist() + [data[x_label].min()]) if x >= data[x_label].min()]
    ax.set_xticks(x_thicks)

    ax.set_ylim(bottom=0)
    ax.set_xlim(left=0, right=data[x_label].max() + 1)

    ax.grid(True)
    fig.show()
    fig.savefig(dst)


def plot_client_send(df, dst, x_label='Number of clients', y_label='MBs received from clients', legend='threads'):
    data = pd.DataFrame()
    data[x_label] = df['client'] * CLIENT_FACTOR
    data[y_label] = df['client_send']
    data[legend] = df['mw_thread']

    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)
    sns.lineplot(x=x_label, y=y_label, hue=legend, data=data, legend="full", ci=CI, marker=MARKER, palette=PALETTE)

    x_thicks = [x for x in sorted(plt.xticks()[0].tolist() + [data[x_label].min()]) if x >= data[x_label].min()]
    ax.set_xticks(x_thicks)

    ax.set_ylim(bottom=0)
    ax.set_xlim(left=0, right=data[x_label].max() + 1)

    ax.grid(True)
    fig.show()
    fig.savefig(dst)


def plot_time_decomposition(df, dst, threads, x_label='Number of clients', y_label='Time request spend in MW [ms]'):
    data = df[df['mw_thread'] == threads]
    data[x_label] = data['client'].values * CLIENT_FACTOR
    # data[y_label] = df['client_send']
    palette = sns.color_palette("hls", 5)

    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)
    ax = sns.lineplot(x=x_label, y='mw_rt', data=data, legend=None, ci=CI, marker=MARKER, palette=palette,
                      label='Total time')
    ax = sns.lineplot(x=x_label, y='r_internalQueueTimer', data=data, legend=None, ci=CI, marker=MARKER,
                      palette=palette, label='Internal queue timer')
    ax = sns.lineplot(x=x_label, y='r_waitingServiceTimer', data=data, legend=None, ci=CI, marker=MARKER,
                      palette=palette, label='Waiting service timer')
    ax = sns.lineplot(x=x_label, y='r_processingTimer', data=data, legend=None, ci=CI, marker=MARKER,
                      palette=palette, label='Processing timer')
    ax = sns.lineplot(x=x_label, y='r_returningResponseTimer', data=data, legend=None, ci=CI, marker=MARKER,
                      palette=palette, label='Returning response timer')

    # plt.axvline(x=84, color='k', linestyle='--', label='Saturation')

    x_thicks = [x for x in sorted(plt.xticks()[0].tolist() + [data[x_label].min()]) if x >= data[x_label].min()]
    ax.set_xticks(x_thicks)

    plt.ylabel(y_label)
    plt.legend(loc=2)
    ax.set_ylim(bottom=0, top=55)
    ax.set_xlim(left=0, right=data[x_label].max() + 1)

    ax.grid(True)
    fig.show()
    fig.savefig(dst)


def main():
    set_df = pd.read_csv('stats_set.csv')

    threads = 8
    plot_queue_size(set_df, '05_experiment_QueueSize_set.jpg')
    plot_time_decomposition(set_df, '05_experiment_rtDecomp%s_set.jpg' % threads, threads)
    threads = 64
    plot_time_decomposition(set_df, '05_experiment_rtDecomp%s_set.jpg' % threads, threads)

    # client send
    scl = 1. / 2 ** 20  # in MBs
    set_df['server_recv'] = set_df['server_recv'] * scl
    set_df['server_send'] = set_df['server_send'] * scl
    set_df['mw_recv'] = set_df['mw_recv'] * scl
    set_df['mw_send'] = set_df['mw_send'] * scl

    set_df['client_send'] = set_df['mw_recv'] - set_df['server_send']
    set_df['client_recv'] = set_df['mw_send'] - set_df['server_recv']

    plot_client_send(set_df, '05_experiment_ClientSend_set.jpg')
    plot_cpu_idle(set_df, '05_experiment_CPUidle_set.jpg')
    plot_memory(set_df, '05_experiment_memory_set.jpg')


if __name__ == '__main__':
    PALETTE = sns.color_palette("hls", 4)

    MARKER = "o"
    CI = 'sd'

    NUMBER_OF_CLIENTS = 3
    NUMBER_OF_INSTANCES = 2
    NUMBER_OF_THREADS = 1

    CLIENT_FACTOR = NUMBER_OF_CLIENTS * NUMBER_OF_INSTANCES * NUMBER_OF_THREADS
    main()
