# import pandas as pd
#
#
# def il(N, X, R, Z):
#     return ((N / X) - Z) / R
#
#
# def print_interactive_law(df):
#     clients = sorted(set(df['client'].values.tolist()))
#     threads = sorted(set(df['mw_thread'].values.tolist()))
#
#     for thread in threads:
#         for client in clients:
#             N = client * CLIENT_FACTOR
#
#             tmp_df = df[(df['client'] == client) & (df['mw_thread'] == thread)]
#             X = tmp_df['mw_th'].mean() / 1000
#             R = tmp_df['mw_rt'].mean()
#             Rc = tmp_df['c_rt'].mean()
#             # Z = abs(R - Rc)
#             Z=0.8
#             print(thread, N, '\t%.2f' % il(N, X, R, Z))
#
#     # all_THs_full = get_th_all(request_type, clients)
#     # all_THs = [x.mean() / 1000 for x in all_THs_full]
#     #
#     # all_RTs_full = get_rt_all(request_type, clients)
#     # all_RTs = [np.mean(x) for x in all_RTs_full]
#     #
#     # clients = np.array(clients) * NUMBER_OF_CLIENTS * NUMBER_OF_INSTANCES * NUMBER_OF_THREADS
#     # alpha = [(N / X / R) for N, X, R in zip(clients, all_THs, all_RTs)]
#
#     # table='\n'.join([
#     #     '&\t'.join(['clients']+[str(c) for c in clients.tolist()])+r'\\',
#     #     '&\t'.join([r'TH [1000 ops/s]']+['%.2f' % x for x in all_THs])+r'\\',
#     #     '&\t'.join(['RT [ms]']+['%.2f' % r for r in all_RTs])+r'\\',
#     #     '&\t'.join([r'$\frac{celint/TH}{RT}$']+['%.2f' % a for a in alpha])+r'\\',
#     # ])
#     # print(table)
#
#
# def main():
#     set_df = pd.read_csv('stats_set.csv')
#
#     print_interactive_law(set_df)
import pandas as pd


def il(N, X, R, Z):
    return (N / X) / (R + Z)


def workload(request_type):
    if request_type == 'set':
        return 'Write'
    return 'Read'


def print_interactive_law():
    tmp = {'Workload': [], 'Workers': []}
    for client in CLIENTS:
        tmp['%s clients' % (client * CLIENT_FACTOR)] = []

    for request_type in ['set']:
        df = pd.read_csv('stats_%s.csv' % request_type)
        threads = sorted(set(df['mw_thread'].values.tolist()))

        for thread in threads:
            tmp['Workload'].append(workload(request_type))
            tmp['Workers'].append(thread)
            for client in CLIENTS:
                N = client * CLIENT_FACTOR

                tmp_df = df[(df['client'] == client) & (df['mw_thread'] == thread)]
                X = tmp_df['mw_th'].mean() / 1000
                R = tmp_df['mw_rt'].mean()
                # Rc = tmp_df['c_rt'].mean()
                # Z = abs(R - Rc)
                Z = 1
                print(thread, N, '\t%.2f' % il(N, X, R, Z))
                print('X_derv=%.2f' % (1000*(N / (R + Z))))

                tmp['%s clients' % N].append(il(N, X, R, Z))

    print(pd.DataFrame(tmp).round(2).to_latex(index=False))


def main():
    print_interactive_law()


if __name__ == '__main__':
    CLIENTS = [1, 2, 4, 8, 14, 22, 42, 64]

    NUMBER_OF_CLIENTS = 3
    NUMBER_OF_INSTANCES = 1
    NUMBER_OF_THREADS = 2

    CLIENT_FACTOR = NUMBER_OF_CLIENTS * NUMBER_OF_INSTANCES * NUMBER_OF_THREADS

    main()
