1         Threads
8         Connections per thread
70        Seconds


ALL STATS
========================================================================
Type        Ops/sec     Hits/sec   Misses/sec      Latency       KB/sec 
------------------------------------------------------------------------
Sets        1594.23          ---          ---      5.01300      6434.34 
Gets           0.00         0.00         0.00      0.00000         0.00 
Waits          0.00          ---          ---      0.00000          --- 
Totals      1594.23         0.00         0.00      5.01300      6434.34 
