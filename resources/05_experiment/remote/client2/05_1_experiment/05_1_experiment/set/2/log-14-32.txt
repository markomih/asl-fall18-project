1         Threads
14        Connections per thread
70        Seconds


ALL STATS
========================================================================
Type        Ops/sec     Hits/sec   Misses/sec      Latency       KB/sec 
------------------------------------------------------------------------
Sets        1477.64          ---          ---      9.46900      5963.79 
Gets           0.00         0.00         0.00      0.00000         0.00 
Waits          0.00          ---          ---      0.00000          --- 
Totals      1477.64         0.00         0.00      9.46900      5963.79 
