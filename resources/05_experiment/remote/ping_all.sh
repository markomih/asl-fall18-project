#!/usr/bin/env bash

function ping_all {
    CMD1="ping -c 10 ${MW1_PRIVATE}"
    CMD2="ping -c 10 ${MW2_PRIVATE}"

    ssh ${CLIENT1} ${CMD1} > "client1_mw1.txt"
    ssh ${CLIENT2} ${CMD1} > "client2_mw1.txt"
    ssh ${CLIENT3} ${CMD1} > "client3_mw1.txt"

    ssh ${CLIENT1} ${CMD2} > "client1_mw2.txt"
    ssh ${CLIENT2} ${CMD2} > "client2_mw2.txt"
    ssh ${CLIENT3} ${CMD2} > "client3_mw2.txt"

    CMD_MW="ping -c 10 ${SERVER1_PRIVATE}"
    ssh ${MW1} ${CMD_MW} > "MW1_server1.txt"
    ssh ${MW2} ${CMD_MW} > "MW2_server1.txt"


    CMD_MW="ping -c 10 ${SERVER2_PRIVATE}"
    ssh ${MW1} ${CMD_MW} > "MW1_server2.txt"
    ssh ${MW2} ${CMD_MW} > "MW2_server2.txt"


    CMD_MW="ping -c 10 ${SERVER3_PRIVATE}"
    ssh ${MW1} ${CMD_MW} > "MW1_server3.txt"
    ssh ${MW2} ${CMD_MW} > "MW2_server3.txt"
}

CLIENT1=storexg5xzpqildpbosshpublicip1.westeurope.cloudapp.azure.com
CLIENT2=storexg5xzpqildpbosshpublicip2.westeurope.cloudapp.azure.com
CLIENT3=storexg5xzpqildpbosshpublicip3.westeurope.cloudapp.azure.com

SERVER1=storexg5xzpqildpbosshpublicip6.westeurope.cloudapp.azure.com
SERVER2=storexg5xzpqildpbosshpublicip7.westeurope.cloudapp.azure.com
SERVER3=storexg5xzpqildpbosshpublicip8.westeurope.cloudapp.azure.com

MW1=storexg5xzpqildpbosshpublicip4.westeurope.cloudapp.azure.com
MW2=storexg5xzpqildpbosshpublicip5.westeurope.cloudapp.azure.com

SERVER1_PRIVATE=10.0.0.7
SERVER2_PRIVATE=10.0.0.10
SERVER3_PRIVATE=10.0.0.8

MW1_PRIVATE=10.0.0.6
MW2_PRIVATE=10.0.0.11

ping_all