import pandas as pd
import matplotlib.pyplot as plt

import seaborn as sns
import numpy as np

sns.set(style="whitegrid")


def plot_th(df, dst, x_label='Number of clients', y_label='Throughput 1000 Ops/sec', legend='threads', TH_SCALE=1000,
            legend_location=None):
    # df['client'] =
    data = pd.DataFrame()
    data[x_label] = df['client'].values * NUMBER_OF_CLIENTS * NUMBER_OF_INSTANCES * NUMBER_OF_THREADS
    data[y_label] = df['mw_th'] / TH_SCALE

    data[legend] = df['mw_thread']

    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)
    sns.lineplot(x=x_label, y=y_label, hue=legend, data=data, legend="full", ci=CI, marker=MARKER, palette=PALETTE)

    x_thicks = [x for x in sorted(plt.xticks()[0].tolist() + [data[x_label].min()]) if x >= data[x_label].min()]
    ax.set_xticks(x_thicks)

    ax.set_ylim(ymin=0, ymax=18.0)
    ax.set_xlim(left=0, right=data[x_label].max() + 1)

    ax.grid(True)
    fig.show()
    fig.savefig(dst)


def plot_client_th(df, dst, x_label='Number of clients', y_label='Throughput 1000 Ops/sec', legend='threads',
                   TH_SCALE=1000):
    # df['client'] =
    data = pd.DataFrame()
    data[x_label] = df['client'].values * NUMBER_OF_CLIENTS * NUMBER_OF_INSTANCES * NUMBER_OF_THREADS
    data[y_label] = df['c_th'] / TH_SCALE

    data[legend] = df['mw_thread']

    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)
    sns.lineplot(x=x_label, y=y_label, hue=legend, data=data, legend="full", ci=CI, marker=MARKER, palette=PALETTE)

    x_thicks = [x for x in sorted(plt.xticks()[0].tolist() + [data[x_label].min()]) if x >= data[x_label].min()]
    ax.set_xticks(x_thicks)

    ax.set_ylim(ymin=0, ymax=18.0)
    ax.set_xlim(left=0, right=data[x_label].max() + 1)

    ax.grid(True)
    fig.show()
    fig.savefig(dst)


def plot_rt(df, dst, x_label='Number of clients', y_label='Response Time [ms]', legend='threads'):
    # df['client'] =
    data = pd.DataFrame()
    data[x_label] = df['client'].values * NUMBER_OF_CLIENTS * NUMBER_OF_INSTANCES * NUMBER_OF_THREADS
    data[y_label] = df['mw_rt']
    data[legend] = df['mw_thread']

    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)
    sns.lineplot(x=x_label, y=y_label, hue=legend, data=data, legend="full", ci=CI, marker=MARKER, palette=PALETTE)

    x_thicks = [x for x in sorted(plt.xticks()[0].tolist() + [data[x_label].min()]) if x >= data[x_label].min()]
    ax.set_xticks(x_thicks)

    ax.set_ylim(ymin=0, ymax=140)
    ax.set_xlim(left=0, right=data[x_label].max() + 1)

    ax.grid(True)
    fig.show()
    fig.savefig(dst)


def plot_client_rt(df, dst, x_label='Number of clients', y_label='Response Time [ms]', legend='threads'):
    # df['client'] =
    data = pd.DataFrame()
    data[x_label] = df['client'].values * NUMBER_OF_CLIENTS * NUMBER_OF_INSTANCES * NUMBER_OF_THREADS
    data[y_label] = df['c_rt']
    data[legend] = df['mw_thread']

    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)
    sns.lineplot(x=x_label, y=y_label, hue=legend, data=data, legend="full", ci=CI, marker=MARKER, palette=PALETTE)

    x_thicks = [x for x in sorted(plt.xticks()[0].tolist() + [data[x_label].min()]) if x >= data[x_label].min()]
    ax.set_xticks(x_thicks)

    ax.set_ylim(ymin=0, ymax=140)
    ax.set_xlim(left=0, right=data[x_label].max() + 1)

    ax.grid(True)
    fig.show()
    fig.savefig(dst)


def make_th_plt(request_type='set'):
    df = pd.read_csv('stats_%s.csv' % request_type)

    plot_th(df, '04_experiment_TH_%s.jpg' % request_type)
    plot_client_th(df, '04_experiment_client_TH_%s.jpg' % request_type)


def make_rt_plt(request_type='set'):
    df = pd.read_csv('stats_%s.csv' % request_type)

    plot_rt(df, '04_experiment_RT_%s.jpg' % request_type)
    plot_client_rt(df, '04_experiment_client_RT_%s.jpg' % request_type)


if __name__ == '__main__':
    PALETTE = sns.color_palette("hls", 4)
    MARKER = "o"
    CI = 'sd'

    NUMBER_OF_CLIENTS = 3
    NUMBER_OF_INSTANCES = 2
    NUMBER_OF_THREADS = 1

    make_th_plt('get')
    make_th_plt('set')
    make_rt_plt('get')
    make_rt_plt('set')
