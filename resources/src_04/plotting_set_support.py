import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt


# def plot_arrival_rate(df, dst, x_label='Number of clients', y_label='Arrival Rate [requests/ms]', legend='threads'):
#     data = pd.DataFrame()
#     data[x_label] = df['client'] * CLIENT_FACTOR
#     data[y_label] = df['mw_arrivalRate']
#     data[legend] = df['mw_thread']
#
#     fig = plt.figure()
#     ax = fig.add_subplot(1, 1, 1)
#     sns.lineplot(x=x_label, y=y_label, hue=legend, data=data, legend="full", ci=CI, marker=MARKER, palette=PALETTE)
#
#     x_thicks = [x for x in sorted(plt.xticks()[0].tolist() + [data[x_label].min()]) if x >= data[x_label].min()]
#     ax.set_xticks(x_thicks)
#
#     ax.set_ylim(bottom=0)
#     ax.set_xlim(left=0, right=data[x_label].max() + 1)
#
#     ax.grid(True)
#     fig.show()
#     fig.savefig(dst)
#
#
def plot_queue_size(df, dst, x_label='Number of clients', y_label='Internal Queue size', legend='threads'):
    data = pd.DataFrame()
    data[x_label] = df['client'] * CLIENT_FACTOR
    data[y_label] = df['mw_queueLength']
    data[legend] = df['mw_thread']

    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)
    sns.lineplot(x=x_label, y=y_label, hue=legend, data=data, legend="full", ci=CI, marker=MARKER, palette=PALETTE)

    x_thicks = [x for x in sorted(plt.xticks()[0].tolist() + [data[x_label].min()]) if x >= data[x_label].min()]
    ax.set_xticks(x_thicks)

    ax.set_ylim(bottom=0)
    ax.set_xlim(left=0, right=data[x_label].max() + 1)

    ax.grid(True)
    fig.show()
    fig.savefig(dst)


def plot_client_send(df, dst, x_label='Number of clients', y_label='MBs received from clients', legend='threads'):
    data = pd.DataFrame()
    data[x_label] = df['client'] * CLIENT_FACTOR
    data[y_label] = df['client_send']
    data[legend] = df['mw_thread']

    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)
    sns.lineplot(x=x_label, y=y_label, hue=legend, data=data, legend="full", ci=CI, marker=MARKER, palette=PALETTE)

    x_thicks = [x for x in sorted(plt.xticks()[0].tolist() + [data[x_label].min()]) if x >= data[x_label].min()]
    ax.set_xticks(x_thicks)

    ax.set_ylim(bottom=0)
    ax.set_xlim(left=0, right=data[x_label].max() + 1)

    ax.grid(True)
    fig.show()
    fig.savefig(dst)


def plot_server_recv(df, dst, x_label='Number of clients', y_label='MBs server received', legend='threads'):
    data = pd.DataFrame()
    data[x_label] = df['client'] * CLIENT_FACTOR
    data[y_label] = df['server_recv']
    data[legend] = df['mw_thread']

    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)
    sns.lineplot(x=x_label, y=y_label, hue=legend, data=data, legend="full", ci=CI, marker=MARKER, palette=PALETTE)

    x_thicks = [x for x in sorted(plt.xticks()[0].tolist() + [data[x_label].min()]) if x >= data[x_label].min()]
    ax.set_xticks(x_thicks)

    ax.set_ylim(bottom=0)
    ax.set_xlim(left=0, right=data[x_label].max() + 1)

    ax.grid(True)
    fig.show()
    fig.savefig(dst)


def plot_time_decomposition(df, dst, x_label='Number of clients', y_label='Time request spend in MW [ms]'):
    data = df[df['mw_thread'] == 64]
    data[x_label] = df['client'] * CLIENT_FACTOR
    # data[y_label] = df['client_send']

    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)
    ax = sns.lineplot(x=x_label, y='mw_rt', data=data, legend=None, ci=CI, marker=MARKER, palette=PALETTE,
                      label='Total time')
    ax = sns.lineplot(x=x_label, y='r_internalQueueTimer', data=data, legend=None, ci=CI, marker=MARKER,
                      palette=PALETTE, label='Queueing time')
    ax = sns.lineplot(x=x_label, y='r_waitingServiceTimer', data=data, legend=None, ci=CI, marker=MARKER,
                      palette=PALETTE, label='Waiting memcached')

    plt.axvline(x=132, color='k', linestyle='--', label='Saturation')

    x_thicks = [x for x in sorted(plt.xticks()[0].tolist() + [data[x_label].min()]) if x >= data[x_label].min()]
    ax.set_xticks(x_thicks)

    plt.ylabel(y_label)
    plt.legend(loc=2)
    ax.set_ylim(bottom=0, top=80)
    ax.set_xlim(left=0, right=data[x_label].max() + 1)

    ax.grid(True)
    fig.show()
    fig.savefig(dst)


def main():
    set_df = pd.read_csv('stats_set.csv')

    # plot_arrival_rate(set_df, '03_experiment_ArrivalRate_set.jpg')
    # plot_queue_size(set_df, '04_experiment_QueueSize_set.jpg')
    #
    # # client send
    # scl = 1. / 2 ** 20  # in MBs
    # set_df['server_recv'] = set_df['server_recv'] * scl
    # set_df['server_send'] = set_df['server_send'] * scl
    # set_df['mw_recv'] = set_df['mw_recv'] * scl
    # set_df['mw_send'] = set_df['mw_send'] * scl
    #
    # set_df['client_send'] = set_df['mw_recv'] - set_df['server_send']
    # set_df['client_recv'] = set_df['mw_send'] - set_df['server_recv']
    #
    # plot_client_send(set_df, '04_experiment_ClientSend_set.jpg')
    # plot_server_recv(set_df, '04_experiment_ServerRecv_set.jpg')

    plot_time_decomposition(set_df, '04_experiment_rtDecomp64_set.jpg')


if __name__ == '__main__':
    PALETTE = sns.color_palette("hls", 4)
    MARKER = "o"
    CI = 'sd'

    NUMBER_OF_CLIENTS = 3
    NUMBER_OF_INSTANCES = 2
    NUMBER_OF_THREADS = 1

    CLIENT_FACTOR = NUMBER_OF_CLIENTS * NUMBER_OF_INSTANCES * NUMBER_OF_THREADS
    main()
