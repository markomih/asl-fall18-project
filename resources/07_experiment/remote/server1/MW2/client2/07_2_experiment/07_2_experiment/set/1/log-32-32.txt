1         Threads
32        Connections per thread
70        Seconds


ALL STATS
========================================================================
Type        Ops/sec     Hits/sec   Misses/sec      Latency       KB/sec 
------------------------------------------------------------------------
Sets        2349.67          ---          ---     13.61400      9483.33 
Gets           0.00         0.00         0.00      0.00000         0.00 
Waits          0.00          ---          ---      0.00000          --- 
Totals      2349.67         0.00         0.00     13.61400      9483.33 


Request Latency Distribution
Type     <= msec         Percent
------------------------------------------------------------------------
SET       1.400         0.00
SET       1.600         0.00
SET       2.300         0.00
SET       2.400         0.00
SET       2.500         0.01
SET       2.700         0.01
SET       2.800         0.01
SET       2.900         0.02
SET       3.000         0.02
SET       3.100         0.04
SET       3.200         0.05
SET       3.300         0.06
SET       3.400         0.09
SET       3.500         0.12
SET       3.600         0.15
SET       3.700         0.19
SET       3.800         0.22
SET       3.900         0.27
SET       4.000         0.31
SET       4.100         0.36
SET       4.200         0.39
SET       4.300         0.43
SET       4.400         0.46
SET       4.500         0.49
SET       4.600         0.53
SET       4.700         0.56
SET       4.800         0.58
SET       4.900         0.62
SET       5.000         0.66
SET       5.100         0.69
SET       5.200         0.73
SET       5.300         0.78
SET       5.400         0.83
SET       5.500         0.89
SET       5.600         0.94
SET       5.700         0.98
SET       5.800         1.03
SET       5.900         1.09
SET       6.000         1.13
SET       6.100         1.18
SET       6.200         1.22
SET       6.300         1.26
SET       6.400         1.31
SET       6.500         1.35
SET       6.600         1.40
SET       6.700         1.45
SET       6.800         1.51
SET       6.900         1.56
SET       7.000         1.62
SET       7.100         1.68
SET       7.200         1.73
SET       7.300         1.79
SET       7.400         1.85
SET       7.500         1.92
SET       7.600         1.99
SET       7.700         2.05
SET       7.800         2.12
SET       7.900         2.20
SET       8.000         2.27
SET       8.100         2.35
SET       8.200         2.45
SET       8.300         2.55
SET       8.400         2.67
SET       8.500         2.78
SET       8.600         2.90
SET       8.700         3.01
SET       8.800         3.15
SET       8.900         3.30
SET       9.000         3.47
SET       9.100         3.67
SET       9.200         3.88
SET       9.300         4.11
SET       9.400         4.34
SET       9.500         4.61
SET       9.600         4.94
SET       9.700         5.31
SET       9.800         5.71
SET       9.900         6.19
SET      10.000        10.73
SET      11.000        29.94
SET      12.000        49.86
SET      13.000        62.63
SET      14.000        70.28
SET      15.000        75.15
SET      16.000        78.94
SET      17.000        83.74
SET      18.000        88.99
SET      19.000        92.99
SET      20.000        95.76
SET      21.000        97.41
SET      22.000        98.38
SET      23.000        98.91
SET      24.000        99.19
SET      25.000        99.34
SET      26.000        99.44
SET      27.000        99.55
SET      28.000        99.63
SET      29.000        99.69
SET      30.000        99.75
SET      31.000        99.81
SET      32.000        99.84
SET      33.000        99.86
SET      34.000        99.87
SET      35.000        99.88
SET      36.000        99.89
SET      37.000        99.91
SET      38.000        99.93
SET      39.000        99.96
SET      40.000        99.97
SET      41.000        99.97
SET      42.000        99.97
SET      43.000        99.97
SET      44.000        99.98
SET      45.000        99.98
SET      58.000        99.98
SET      59.000        99.98
SET      60.000        99.98
SET      61.000        99.98
SET      62.000        99.98
SET      77.000        99.98
SET      81.000        99.98
SET     140.000        99.98
SET     150.000        99.98
SET     160.000        99.99
SET     170.000        99.99
SET     180.000        99.99
SET     200.000        99.99
SET     210.000       100.00
SET     220.000       100.00
SET     230.000       100.00
---
---
