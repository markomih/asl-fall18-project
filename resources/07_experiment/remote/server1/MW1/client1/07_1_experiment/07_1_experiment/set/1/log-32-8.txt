2         Threads
32        Connections per thread
70        Seconds


ALL STATS
========================================================================
Type        Ops/sec     Hits/sec   Misses/sec      Latency       KB/sec 
------------------------------------------------------------------------
Sets        2355.90          ---          ---     27.15900      9508.49 
Gets           0.00         0.00         0.00      0.00000         0.00 
Waits          0.00          ---          ---      0.00000          --- 
Totals      2355.90         0.00         0.00     27.15900      9508.49 


Request Latency Distribution
Type     <= msec         Percent
------------------------------------------------------------------------
SET      11.000         0.00
SET      12.000         0.00
SET      13.000         0.00
SET      15.000         0.00
SET      17.000         0.00
SET      18.000         0.01
SET      19.000         0.12
SET      20.000         0.69
SET      21.000         2.56
SET      22.000         6.87
SET      23.000        14.74
SET      24.000        25.83
SET      25.000        38.85
SET      26.000        51.51
SET      27.000        63.12
SET      28.000        72.57
SET      29.000        79.63
SET      30.000        84.96
SET      31.000        88.87
SET      32.000        91.58
SET      33.000        93.54
SET      34.000        94.92
SET      35.000        95.94
SET      36.000        96.70
SET      37.000        97.29
SET      38.000        97.78
SET      39.000        98.13
SET      40.000        98.38
SET      41.000        98.57
SET      42.000        98.76
SET      43.000        98.91
SET      44.000        99.06
SET      45.000        99.27
SET      46.000        99.50
SET      47.000        99.64
SET      48.000        99.72
SET      49.000        99.77
SET      50.000        99.82
SET      51.000        99.85
SET      52.000        99.88
SET      53.000        99.90
SET      54.000        99.91
SET      55.000        99.92
SET      56.000        99.93
SET      57.000        99.94
SET      58.000        99.94
SET      59.000        99.94
SET      60.000        99.94
SET      61.000        99.94
SET      62.000        99.95
SET      63.000        99.95
SET      65.000        99.95
SET      66.000        99.95
SET      67.000        99.95
SET      68.000        99.96
SET      69.000        99.98
SET      70.000        99.98
SET      71.000        99.98
SET      72.000        99.98
SET      73.000        99.98
SET      80.000        99.98
SET      97.000        99.98
SET     110.000        99.98
SET     130.000        99.98
SET     140.000        99.98
SET     150.000        99.99
SET     200.000        99.99
SET     210.000        99.99
SET     220.000       100.00
SET     230.000       100.00
---
---
