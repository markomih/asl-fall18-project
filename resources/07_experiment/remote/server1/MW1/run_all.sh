#!/usr/bin/env bash


CLIENT1=storexg5xzpqildpbosshpublicip1.westeurope.cloudapp.azure.com
CLIENT2=storexg5xzpqildpbosshpublicip2.westeurope.cloudapp.azure.com
CLIENT3=storexg5xzpqildpbosshpublicip3.westeurope.cloudapp.azure.com

SERVER1=storexg5xzpqildpbosshpublicip6.westeurope.cloudapp.azure.com

MW1=storexg5xzpqildpbosshpublicip4.westeurope.cloudapp.azure.com

SERVER1_PRIVATE=10.0.0.7

MW1_PRIVATE=10.0.0.6

SERVER_PORT=11211
MW_PORT=45231



CLIENT_DIR1='07_1_experiment'

#specifying MW
MW_THREADS=( 8 32 )
MW_JAR_PATH="./mw.jar"


DATA_SIZE=4096
KEY_MAX=10000

run=( 1 2 3 )
TEST_TIME=70
#run=( 1 )
#TEST_TIME=10


MW_WAITING_TIME=10

DSTAT_TIME=${TEST_TIME}
TOTAL_TIME=$((TEST_TIME + MW_WAITING_TIME + 5))

DATA_SIZE=4096

client=32

SHARDED="false"
MW_DSTAT_CMD="dstat --output dstat.csv 1 ${DSTAT_TIME}"


##########################################################

THREADS=2
SERVERS="${SERVER1_PRIVATE}:${SERVER_PORT}"

##########################################################
ssh ${SERVER1} "rm *.csv"
#################### multi_get_sharded ####################
TYPE='get'
SET_RATIO=0
GET_RATIO=1


# prepare directories
for experiment in "${run[@]}"
do
        MKDIR1="mkdir -p ${CLIENT_DIR1}/${TYPE}/${experiment}"

        RMFILES1="rm ${CLIENT_DIR1}/${TYPE}/${experiment}/*"

        ssh ${CLIENT1} "${MKDIR1};${RMFILES1}"

        ssh ${CLIENT2} "${MKDIR1};${RMFILES1}"

        ssh ${CLIENT3} "${MKDIR1};${RMFILES1}"
done

for experiment in "${run[@]}"
do
    for mw_thread in "${MW_THREADS[@]}"
    do
            DSTAT_CMD_SERVER="dstat --output server_${mw_thread}_${TYPE}_${experiment}_${client}.csv 1 ${DSTAT_TIME}"

            CDDIR1="cd ${CLIENT_DIR1}/${TYPE}/${experiment}"
            PARAMS1="-s ${MW1_PRIVATE} --port=${MW_PORT} --clients=${client} --threads=${THREADS} --test-time=${TEST_TIME} --protocol=memcache_text --ratio=${SET_RATIO}:${GET_RATIO} --key-maximum=${KEY_MAX} --data-size=${DATA_SIZE} --out-file=log-${client}-${mw_thread}.txt"
            CMD1="/home/maki/memtier_benchmark-master/memtier_benchmark ${PARAMS1}"

            # RUN MWs
            RUN_JAR_CMD="java -jar ${MW_JAR_PATH} -l 0.0.0.0 -p ${MW_PORT} -t ${mw_thread} -s ${SHARDED} -m ${SERVERS}"
            ssh ${MW1} "${RUN_JAR_CMD} &" &

            echo "waiting ${MW_WAITING_TIME}s"
            sleep ${MW_WAITING_TIME}s
            echo "done waiting ${MW_WAITING_TIME}s"

            # RUN CLients 1
            echo "executing 1: $CMD1"
            echo "run=$experiment; client=$client"
            ssh ${CLIENT1} "${CDDIR1};${CMD1}" &
            ssh ${CLIENT2} "${CDDIR1};${CMD1}" &
            ssh ${CLIENT3} "${CDDIR1};${CMD1}" &

            ssh ${MW1} "${MW_DSTAT_CMD}" &
            ssh ${SERVER1} "${DSTAT_CMD_SERVER}" &

            echo "Sleep before Killing java"
            sleep ${TOTAL_TIME}s
#                wait
            echo "Killing java"
            ssh ${MW1} "pkill java"

            wait
            sleep 2s

            DST_DIR1="./MW1/MW_${TYPE}_${mw_thread}_${experiment}_${client}"
            mkdir -p ${DST_DIR1}
            scp -r ${MW1}:*.txt ./${DST_DIR1}
            scp -r ${MW1}:*.csv ./${DST_DIR1}
            sleep 2s
            ssh ${MW1} "rm *.txt"
            ssh ${MW1} "rm *.csv"

            echo "DONE $client, $experiment, $mw_thread"
            echo
    done
done

#################### set ####################
TYPE='set'
SET_RATIO=1
GET_RATIO=0


# prepare directories
for experiment in "${run[@]}"
do
        MKDIR1="mkdir -p ${CLIENT_DIR1}/${TYPE}/${experiment}"

        RMFILES1="rm ${CLIENT_DIR1}/${TYPE}/${experiment}/*"

        ssh ${CLIENT1} "${MKDIR1};${RMFILES1}"

        ssh ${CLIENT2} "${MKDIR1};${RMFILES1}"

        ssh ${CLIENT3} "${MKDIR1};${RMFILES1}"
done

for experiment in "${run[@]}"
do
    for mw_thread in "${MW_THREADS[@]}"
    do
            DSTAT_CMD_SERVER="dstat --output server_${mw_thread}_${TYPE}_${experiment}_${client}.csv 1 ${DSTAT_TIME}"

            CDDIR1="cd ${CLIENT_DIR1}/${TYPE}/${experiment}"
            PARAMS1="-s ${MW1_PRIVATE} --port=${MW_PORT} --clients=${client} --threads=${THREADS} --test-time=${TEST_TIME} --protocol=memcache_text --ratio=${SET_RATIO}:${GET_RATIO} --key-maximum=${KEY_MAX} --data-size=${DATA_SIZE} --out-file=log-${client}-${mw_thread}.txt"
            CMD1="/home/maki/memtier_benchmark-master/memtier_benchmark ${PARAMS1}"

            # RUN MWs
            RUN_JAR_CMD="java -jar ${MW_JAR_PATH} -l 0.0.0.0 -p ${MW_PORT} -t ${mw_thread} -s ${SHARDED} -m ${SERVERS}"
            ssh ${MW1} "${RUN_JAR_CMD} &" &

            echo "waiting ${MW_WAITING_TIME}s"
            sleep ${MW_WAITING_TIME}s
            echo "done waiting ${MW_WAITING_TIME}s"

            # RUN CLients 1
            echo "executing 1: $CMD1"
            echo "run=$experiment; client=$client"
            ssh ${CLIENT1} "${CDDIR1};${CMD1}" &
            ssh ${CLIENT2} "${CDDIR1};${CMD1}" &
            ssh ${CLIENT3} "${CDDIR1};${CMD1}" &

            ssh ${MW1} "${MW_DSTAT_CMD}" &
            ssh ${SERVER1} "${DSTAT_CMD_SERVER}" &

            echo "Sleep before Killing java"
            sleep ${TOTAL_TIME}s
#                wait
            echo "Killing java"
            ssh ${MW1} "pkill java"

            wait
            sleep 2s

            DST_DIR1="./MW1/MW_${TYPE}_${mw_thread}_${experiment}_${client}"
            mkdir -p ${DST_DIR1}
            scp -r ${MW1}:*.txt ./${DST_DIR1}
            scp -r ${MW1}:*.csv ./${DST_DIR1}
            sleep 2s
            ssh ${MW1} "rm *.txt"
            ssh ${MW1} "rm *.csv"

            echo "DONE $client, $experiment, $mw_thread"
            echo
    done
done



########### CPY files ################
mkdir -p server1

scp -r ${SERVER1}:*.csv ./server1/

c_dirs=( ${CLIENT_DIR1} )
for c_dir in "${c_dirs[@]}"
do
    mkdir -p client1/${c_dir}
    scp -r ${CLIENT1}:${c_dir} client1/${c_dir}

    mkdir -p client2/${c_dir}
    scp -r ${CLIENT2}:${c_dir} client2/${c_dir}

    mkdir -p client3/${c_dir}
    scp -r ${CLIENT3}:${c_dir} client3/${c_dir}
done
