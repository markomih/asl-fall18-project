2         Threads
32        Connections per thread
70        Seconds


ALL STATS
========================================================================
Type        Ops/sec     Hits/sec   Misses/sec      Latency       KB/sec 
------------------------------------------------------------------------
Sets        3409.05          ---          ---     18.76900     13759.01 
Gets           0.00         0.00         0.00      0.00000         0.00 
Waits          0.00          ---          ---      0.00000          --- 
Totals      3409.05         0.00         0.00     18.76900     13759.01 


Request Latency Distribution
Type     <= msec         Percent
------------------------------------------------------------------------
SET       2.400         0.00
SET       3.200         0.00
SET       3.600         0.00
SET       4.300         0.00
SET       4.400         0.00
SET       4.600         0.00
SET       4.900         0.00
SET       5.000         0.00
SET       5.100         0.00
SET       5.200         0.00
SET       5.400         0.01
SET       5.600         0.01
SET       5.700         0.01
SET       5.800         0.01
SET       5.900         0.01
SET       6.000         0.01
SET       6.100         0.01
SET       6.200         0.01
SET       6.300         0.01
SET       6.400         0.01
SET       6.500         0.01
SET       6.700         0.02
SET       6.800         0.02
SET       6.900         0.02
SET       7.000         0.02
SET       7.100         0.02
SET       7.200         0.02
SET       7.400         0.03
SET       7.500         0.03
SET       7.600         0.03
SET       7.700         0.03
SET       7.800         0.04
SET       7.900         0.04
SET       8.000         0.04
SET       8.100         0.05
SET       8.200         0.05
SET       8.300         0.06
SET       8.400         0.07
SET       8.500         0.08
SET       8.600         0.09
SET       8.700         0.10
SET       8.800         0.12
SET       8.900         0.13
SET       9.000         0.15
SET       9.100         0.17
SET       9.200         0.20
SET       9.300         0.23
SET       9.400         0.26
SET       9.500         0.29
SET       9.600         0.33
SET       9.700         0.37
SET       9.800         0.42
SET       9.900         0.46
SET      10.000         0.77
SET      11.000         1.47
SET      12.000         2.49
SET      13.000         4.11
SET      14.000         8.93
SET      15.000        20.52
SET      16.000        34.74
SET      17.000        45.67
SET      18.000        52.31
SET      19.000        57.56
SET      20.000        65.38
SET      21.000        75.85
SET      22.000        85.60
SET      23.000        91.87
SET      24.000        95.22
SET      25.000        96.95
SET      26.000        97.87
SET      27.000        98.37
SET      28.000        98.69
SET      29.000        98.94
SET      30.000        99.18
SET      31.000        99.37
SET      32.000        99.49
SET      33.000        99.58
SET      34.000        99.65
SET      35.000        99.69
SET      36.000        99.73
SET      37.000        99.76
SET      38.000        99.79
SET      39.000        99.84
SET      40.000        99.87
SET      41.000        99.89
SET      42.000        99.91
SET      43.000        99.93
SET      44.000        99.94
SET      45.000        99.94
SET      46.000        99.94
SET      47.000        99.95
SET      48.000        99.96
SET      49.000        99.96
SET      50.000        99.96
SET      51.000        99.96
SET      52.000        99.97
SET      53.000        99.97
SET      54.000        99.97
SET      55.000        99.97
SET      56.000        99.97
SET      57.000        99.97
SET      58.000        99.97
SET      59.000        99.97
SET      60.000        99.97
SET      61.000        99.97
SET      62.000        99.97
SET      63.000        99.97
SET      69.000        99.97
SET      71.000        99.97
SET      81.000        99.97
SET      88.000        99.97
SET     130.000        99.97
SET     140.000        99.97
SET     170.000        99.98
SET     180.000        99.98
SET     190.000        99.98
SET     200.000        99.98
SET     210.000        99.98
SET     230.000        99.98
SET     240.000        99.98
SET     250.000        99.99
SET     260.000        99.99
SET     280.000        99.99
SET     290.000        99.99
SET     300.000        99.99
SET     330.000        99.99
SET     340.000        99.99
SET     360.000       100.00
SET     380.000       100.00
SET     400.000       100.00
SET     420.000       100.00
SET     430.000       100.00
SET     440.000       100.00
SET     450.000       100.00
---
---
