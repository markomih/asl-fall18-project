1         Threads
32        Connections per thread
70        Seconds


ALL STATS
========================================================================
Type        Ops/sec     Hits/sec   Misses/sec      Latency       KB/sec 
------------------------------------------------------------------------
Sets           0.00          ---          ---      0.00000         0.00 
Gets        1390.16      1390.16         0.00     23.00800      5630.91 
Waits          0.00          ---          ---      0.00000          --- 
Totals      1390.16      1390.16         0.00     23.00800      5630.91 


Request Latency Distribution
Type     <= msec         Percent
------------------------------------------------------------------------
---
GET       2.600         0.00
GET       3.800         0.00
GET       3.900         0.00
GET       4.100         0.01
GET       4.300         0.01
GET       4.400         0.01
GET       4.500         0.02
GET       4.600         0.03
GET       4.700         0.03
GET       4.800         0.04
GET       4.900         0.04
GET       5.000         0.05
GET       5.100         0.05
GET       5.200         0.07
GET       5.300         0.08
GET       5.400         0.08
GET       5.500         0.09
GET       5.600         0.10
GET       5.700         0.12
GET       5.800         0.13
GET       5.900         0.14
GET       6.000         0.15
GET       6.100         0.16
GET       6.200         0.18
GET       6.300         0.20
GET       6.400         0.22
GET       6.500         0.24
GET       6.600         0.26
GET       6.700         0.28
GET       6.800         0.31
GET       6.900         0.32
GET       7.000         0.35
GET       7.100         0.37
GET       7.200         0.39
GET       7.300         0.42
GET       7.400         0.44
GET       7.500         0.46
GET       7.600         0.49
GET       7.700         0.50
GET       7.800         0.53
GET       7.900         0.56
GET       8.000         0.59
GET       8.100         0.61
GET       8.200         0.63
GET       8.300         0.65
GET       8.400         0.68
GET       8.500         0.71
GET       8.600         0.74
GET       8.700         0.76
GET       8.800         0.79
GET       8.900         0.82
GET       9.000         0.84
GET       9.100         0.87
GET       9.200         0.88
GET       9.300         0.91
GET       9.400         0.93
GET       9.500         0.96
GET       9.600         0.98
GET       9.700         1.01
GET       9.800         1.04
GET       9.900         1.07
GET      10.000         1.22
GET      11.000         1.54
GET      12.000         2.05
GET      13.000         2.89
GET      14.000         4.46
GET      15.000         8.18
GET      16.000        22.38
GET      17.000        41.44
GET      18.000        50.30
GET      19.000        54.63
GET      20.000        56.60
GET      21.000        58.17
GET      22.000        59.44
GET      23.000        60.64
GET      24.000        61.99
GET      25.000        63.84
GET      26.000        66.51
GET      27.000        70.04
GET      28.000        74.82
GET      29.000        79.01
GET      30.000        83.02
GET      31.000        89.63
GET      32.000        92.93
GET      33.000        93.95
GET      34.000        94.73
GET      35.000        95.13
GET      36.000        95.33
GET      37.000        95.45
GET      38.000        95.53
GET      39.000        95.64
GET      40.000        95.76
GET      41.000        95.90
GET      42.000        96.12
GET      43.000        96.38
GET      44.000        96.80
GET      45.000        97.30
GET      46.000        97.92
GET      47.000        98.60
GET      48.000        98.73
GET      49.000        98.82
GET      50.000        98.89
GET      51.000        98.92
GET      52.000        98.94
GET      53.000        98.96
GET      54.000        98.97
GET      55.000        98.99
GET      56.000        99.02
GET      57.000        99.06
GET      58.000        99.10
GET      59.000        99.18
GET      60.000        99.26
GET      61.000        99.37
GET      62.000        99.49
GET      63.000        99.57
GET      64.000        99.58
GET      65.000        99.60
GET      66.000        99.61
GET      67.000        99.61
GET      68.000        99.62
GET      69.000        99.63
GET      70.000        99.63
GET      71.000        99.64
GET      72.000        99.65
GET      73.000        99.65
GET      74.000        99.67
GET      75.000        99.69
GET      76.000        99.71
GET      77.000        99.73
GET      78.000        99.76
GET      79.000        99.77
GET      80.000        99.77
GET      81.000        99.78
GET      82.000        99.78
GET      84.000        99.78
GET      85.000        99.78
GET      87.000        99.79
GET      88.000        99.79
GET      89.000        99.79
GET      90.000        99.80
GET      91.000        99.80
GET      92.000        99.81
GET      93.000        99.82
GET      94.000        99.83
GET      95.000        99.83
GET      96.000        99.83
GET      97.000        99.84
GET      98.000        99.84
GET      99.000        99.84
GET     100.000        99.86
GET     110.000        99.87
GET     120.000        99.90
GET     130.000        99.90
GET     140.000        99.91
GET     150.000        99.91
GET     170.000        99.91
GET     190.000        99.92
GET     200.000        99.92
GET     210.000        99.92
GET     220.000        99.95
GET     230.000        99.95
GET     290.000        99.95
GET     300.000        99.96
GET     320.000        99.96
GET     390.000        99.96
GET     400.000        99.96
GET     480.000        99.97
GET     490.000        99.97
GET     500.000        99.97
GET     570.000        99.98
GET     580.000        99.98
GET     590.000        99.99
GET     600.000       100.00
GET     670.000       100.00
GET     680.000       100.00
GET     690.000       100.00
---
