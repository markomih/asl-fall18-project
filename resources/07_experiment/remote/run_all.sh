#!/usr/bin/env bash

echo "starting server3 MW2"
cd /home/maki/projects/asl-fall18-project/asl-scripts/07_experiment/remote/server3/MW2
./run_all.sh
cd /home/maki/projects/asl-fall18-project/asl-scripts/07_experiment/remote

echo "starting server3 MW1"
cd /home/maki/projects/asl-fall18-project/asl-scripts/07_experiment/remote/server3/MW1
./run_all.sh
cd /home/maki/projects/asl-fall18-project/asl-scripts/07_experiment/remote

echo "starting server1 MW2"
cd /home/maki/projects/asl-fall18-project/asl-scripts/07_experiment/remote/server1/MW2
./run_all.sh
cd /home/maki/projects/asl-fall18-project/asl-scripts/07_experiment/remote

echo "starting server1 MW1"
cd /home/maki/projects/asl-fall18-project/asl-scripts/07_experiment/remote/server1/MW1
./run_all.sh
cd /home/maki/projects/asl-fall18-project/asl-scripts/07_experiment/remote
