#!/usr/bin/env bash

#!/usr/bin/env bash


CLIENT1=storexg5xzpqildpbosshpublicip1.westeurope.cloudapp.azure.com
CLIENT2=storexg5xzpqildpbosshpublicip2.westeurope.cloudapp.azure.com
CLIENT3=storexg5xzpqildpbosshpublicip3.westeurope.cloudapp.azure.com

SERVER1=storexg5xzpqildpbosshpublicip6.westeurope.cloudapp.azure.com
SERVER2=storexg5xzpqildpbosshpublicip7.westeurope.cloudapp.azure.com
SERVER3=storexg5xzpqildpbosshpublicip8.westeurope.cloudapp.azure.com

MW1=storexg5xzpqildpbosshpublicip4.westeurope.cloudapp.azure.com
MW2=storexg5xzpqildpbosshpublicip5.westeurope.cloudapp.azure.com

SERVER1_PRIVATE=10.0.0.7
SERVER2_PRIVATE=10.0.0.10
SERVER3_PRIVATE=10.0.0.8

MW1_PRIVATE=10.0.0.6
MW2_PRIVATE=10.0.0.11

SERVER_PORT=11211
MW_PORT=45231



CLIENT_DIR1='05_1_experiment'
CLIENT_DIR2='05_2_experiment'

#specifying MW
#MW_THREADS=( 16 )
MW_THREADS=( 64 )
MW_JAR_PATH="./mw.jar"
SHARDED="false"

run=( 1 )
#run=( 1 )

THREADS=1
DATA_SIZE=4096
KEY_MAX=10000

TEST_TIME=70
#TEST_TIME=10
MW_WAITING_TIME=10

DSTAT_TIME=${TEST_TIME}
TOTAL_TIME=$((TEST_TIME + MW_WAITING_TIME + 5))

DATA_SIZE=4096


#################### SET ####################
clients=( 22 )
TYPE='set'
SET_RATIO=1
GET_RATIO=0

MW_DSTAT_CMD="dstat --output dstat.csv 1 ${DSTAT_TIME}"

ssh ${SERVER1} "rm *.csv"
ssh ${SERVER2} "rm *.csv"
ssh ${SERVER3} "rm *.csv"
# prepare directories
for experiment in "${run[@]}"
do
        MKDIR1="mkdir -p ${CLIENT_DIR1}/${TYPE}/${experiment}"
        MKDIR2="mkdir -p ${CLIENT_DIR2}/${TYPE}/${experiment}"

        RMFILES1="rm ${CLIENT_DIR1}/${TYPE}/${experiment}/*"
        RMFILES2="rm ${CLIENT_DIR2}/${TYPE}/${experiment}/*"

        ssh ${CLIENT1} "${MKDIR1};${RMFILES1}"
        ssh ${CLIENT1} "${MKDIR2};${RMFILES2}"

        ssh ${CLIENT2} "${MKDIR1};${RMFILES1}"
        ssh ${CLIENT2} "${MKDIR2};${RMFILES2}"

        ssh ${CLIENT3} "${MKDIR1};${RMFILES1}"
        ssh ${CLIENT3} "${MKDIR2};${RMFILES2}"
done

for mw_thread in "${MW_THREADS[@]}"
do
    for experiment in "${run[@]}"
    do
        for client in "${clients[@]}"
        do
                DSTAT_CMD_SERVER="dstat --output server_${mw_thread}_${TYPE}_${experiment}_${client}.csv 1 ${DSTAT_TIME}"

                CDDIR1="cd ${CLIENT_DIR1}/${TYPE}/${experiment}"
                PARAMS1="-s ${MW1_PRIVATE} --port=${MW_PORT} --clients=${client} --threads=${THREADS} --test-time=${TEST_TIME} --protocol=memcache_text --ratio=${SET_RATIO}:${GET_RATIO} --key-maximum=10000 --hide-histogram --data-size=${DATA_SIZE} --out-file=log-${client}-${mw_thread}.txt"
                CMD1="/home/maki/memtier_benchmark-master/memtier_benchmark ${PARAMS1}"

                CDDIR2="cd ${CLIENT_DIR2}/${TYPE}/${experiment}"
                PARAMS2="-s ${MW2_PRIVATE} --port=${MW_PORT} --clients=${client} --threads=${THREADS} --test-time=${TEST_TIME} --protocol=memcache_text --ratio=${SET_RATIO}:${GET_RATIO} --key-maximum=10000 --hide-histogram --data-size=${DATA_SIZE} --out-file=log-${client}-${mw_thread}.txt"
                CMD2="/home/maki/memtier_benchmark-master/memtier_benchmark ${PARAMS2}"

                # RUN MW1 here
                RUN_JAR_CMD1="java -jar ${MW_JAR_PATH} -l 0.0.0.0 -p ${MW_PORT} -t ${mw_thread} -s ${SHARDED} -m ${SERVER1_PRIVATE}:${SERVER_PORT} ${SERVER2_PRIVATE}:${SERVER_PORT} ${SERVER3_PRIVATE}:${SERVER_PORT}"
                ssh ${MW1} "${RUN_JAR_CMD1} &" &
                MW_PID1=$!

                # RUN MW2 here
                RUN_JAR_CMD2="java -jar ${MW_JAR_PATH} -l 0.0.0.0 -p ${MW_PORT} -t ${mw_thread} -s ${SHARDED} -m ${SERVER1_PRIVATE}:${SERVER_PORT} ${SERVER2_PRIVATE}:${SERVER_PORT} ${SERVER3_PRIVATE}:${SERVER_PORT}"
                ssh ${MW2} "${RUN_JAR_CMD2} &" &
                MW_PID2=$!

                echo "waiting ${MW_WAITING_TIME}s"
                sleep ${MW_WAITING_TIME}s
                echo "done waiting ${MW_WAITING_TIME}s"

                # RUN CLients 1
                echo "executing 1: $CMD1"
                echo "run=$experiment; client=$client"
                ssh ${CLIENT1} "${CDDIR1};${CMD1}" &
                ssh ${CLIENT2} "${CDDIR1};${CMD1}" &
                ssh ${CLIENT3} "${CDDIR1};${CMD1}" &

                # RUN CLients 2
                echo "executing 2: $CMD2"
                echo "run=$experiment; client=$client"
                ssh ${CLIENT1} "${CDDIR2};${CMD2}" &
                ssh ${CLIENT2} "${CDDIR2};${CMD2}" &
                ssh ${CLIENT3} "${CDDIR2};${CMD2}" &

                ssh ${MW1} "${MW_DSTAT_CMD}" &
                ssh ${MW2} "${MW_DSTAT_CMD}" &
                ssh ${SERVER1} "${DSTAT_CMD_SERVER}" &
                ssh ${SERVER2} "${DSTAT_CMD_SERVER}" &
                ssh ${SERVER3} "${DSTAT_CMD_SERVER}" &

                echo "Sleep before Killing java"
                sleep ${TOTAL_TIME}s
#                wait
                echo "Killing java"
                ssh ${MW1} "pkill java"
                ssh ${MW2} "pkill java"

                wait
                sleep 2s

                DST_DIR1="./MW1/MW_${TYPE}_${mw_thread}_${experiment}_${client}"
                mkdir -p ${DST_DIR1}
                scp -r ${MW1}:*.txt ./${DST_DIR1}
                scp -r ${MW1}:*.csv ./${DST_DIR1}
                sleep 2s
                ssh ${MW1} "rm *.txt"
                ssh ${MW1} "rm *.csv"

                # copy MW2 files and delete rest
                DST_DIR2="./MW2/MW_${TYPE}_${mw_thread}_${experiment}_${client}"
                mkdir -p ${DST_DIR2}
                scp -r ${MW2}:*.txt ./${DST_DIR2}
                scp -r ${MW2}:*.csv ./${DST_DIR2}
                ssh ${MW2} "rm *.txt"
                ssh ${MW2} "rm *.csv"

                echo "DONE $client, $experiment, $mw_thread"
                echo
        done
    done
done

########### CPY files ################
mkdir -p server1
mkdir -p server2
mkdir -p server3

scp -r ${SERVER1}:*.csv ./server1/
scp -r ${SERVER2}:*.csv ./server2/
scp -r ${SERVER3}:*.csv ./server3/

c_dirs=( ${CLIENT_DIR1} ${CLIENT_DIR2} )
for c_dir in "${c_dirs[@]}"
do
    mkdir -p client1/${c_dir}
    scp -r ${CLIENT1}:${c_dir} client1/${c_dir}

    mkdir -p client2/${c_dir}
    scp -r ${CLIENT2}:${c_dir} client2/${c_dir}

    mkdir -p client3/${c_dir}
    scp -r ${CLIENT3}:${c_dir} client3/${c_dir}
done


########## STOPPING ################
#az vm stop --resource-group asl-group --name Server1
#az vm stop --resource-group asl-group --name Server2
#az vm stop --resource-group asl-group --name Server3
#az vm stop --resource-group asl-group --name Middleware1
#az vm stop --resource-group asl-group --name Middleware2
#az vm stop --resource-group asl-group --name Client1
#az vm stop --resource-group asl-group --name Client2
#az vm stop --resource-group asl-group --name Client3
#
#az vm deallocate --resource-group asl-group --name Server1
#az vm deallocate --resource-group asl-group --name Server2
#az vm deallocate --resource-group asl-group --name Server3
#az vm deallocate --resource-group asl-group --name Middleware1
#az vm deallocate --resource-group asl-group --name Middleware2
#az vm deallocate --resource-group asl-group --name Client1
#az vm deallocate --resource-group asl-group --name Client2
#az vm deallocate --resource-group asl-group --name Client3


#connect: connection failed: Connection refused

#connection dropped.
#read error: Connection reset by peer
