from io import StringIO
import re
import numpy as np

import os
import pandas as pd

REMOTE_DIR = 'remote'
EXPERIMENT_DIR = '07_experiment'


def parse_file(file_path, request_type='th', misses=False):
    if request_type == 'set':
        matching = 'Sets'
        position = 4
    elif request_type == 'get' or misses:
        matching = 'Gets'
        position = 3 if misses else 4
    elif request_type == 'th':
        matching = 'Totals'
        position = 1
    else:
        raise Exception('incorrect request type')

    with open(file_path) as f:
        for line in f:
            if line.startswith(matching):
                data = [x for x in line.split(" ") if x != ""][position]
                return float(data)


def _get_client_files(dir_paths, number_of_clients, mw_thread):
    file_paths = []
    for dir_path in dir_paths:
        for root, _, files in os.walk(dir_path):
            for file in files:
                if re.match(r'log-%s-%s.txt' % (number_of_clients, mw_thread), file):
                    file_paths.append(os.path.join(root, file))
    return file_paths


def get_client_files(number_of_clients, mw_thread, request_type, run, server, mw):
    client_files = ['client1', 'client2', 'client3']
    root_dir = os.path.join('..', EXPERIMENT_DIR, REMOTE_DIR, 'server%s' % server, 'MW%s' % mw)

    dir_paths1 = [
        os.path.join(root_dir, c, '07_1_experiment', '07_1_experiment', request_type, run)
        for c in client_files]
    file_paths1 = _get_client_files(dir_paths1, number_of_clients, mw_thread)

    if mw == 1:
        return file_paths1, None

    dir_paths2 = [
        os.path.join(root_dir, c, '07_2_experiment', '07_2_experiment', request_type, run)
        for c in client_files]

    file_paths2 = _get_client_files(dir_paths2, number_of_clients, mw_thread)
    return file_paths1, file_paths2


def get_client_th(number_of_clients, mw_thread, request_type, run, server, mw):
    files1, files2 = get_client_files(number_of_clients, mw_thread, request_type, run, server, mw)

    t1 = np.sum([parse_file(file, 'th') for file in files1])
    if mw == 1:
        return t1
    t2 = np.sum([parse_file(file, 'th') for file in files2])
    return t1 + t2


def get_client_rt(number_of_clients, mw_thread, request_type, run, server, mw):
    files1, files2 = get_client_files(number_of_clients, mw_thread, request_type, run, server, mw)
    t1 = np.mean([parse_file(file, request_type) for file in files1])
    if mw == 1:
        return t1
    t2 = np.mean([parse_file(file, request_type) for file in files2])
    return (t1 + t2) / 2.
