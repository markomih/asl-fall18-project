import pandas as pd

# from to_repeat import to_repeat
from utils import get_client_rt, get_client_th


def main(request_type='set'):
    # to_exclude = to_repeat(CLIENTS, MW_THREADS)
    tmp_dict = {
        'server': [],
        'mw_thread': [],
        'run': [],
        'c_rt': [],
        'c_th': [],
        'mw': [],
    }

    for mw_thread in MW_THREADS:
        for server in SERVERS:
            for mw in MWS:
                for run in RUN:
                    tmp_dict['server'].append(server)
                    tmp_dict['mw'].append(mw)
                    tmp_dict['mw_thread'].append(mw_thread)
                    tmp_dict['run'].append(run)

                    tmp_dict['c_rt'].append(get_client_rt(CLIENT, mw_thread, request_type, run, server, mw))
                    tmp_dict['c_th'].append(get_client_th(CLIENT, mw_thread, request_type, run, server, mw))

    df = pd.DataFrame.from_dict(tmp_dict)
    df.to_csv('stats_%s.csv'%request_type)
    print(df)
    print(df.describe())


if __name__ == '__main__':
    CLIENT=32
    MWS=[1,2]
    MW_THREADS=[8,32]
    RUN=['1','2','3']
    SERVERS=[1,3]
    main('set')
    main('get')
