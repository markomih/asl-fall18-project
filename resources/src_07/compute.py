from itertools import product
from scipy import stats

import pandas as pd
import numpy as np
import warnings

warnings.filterwarnings("ignore")


def encode_df(df):
    encoded_df = pd.DataFrame()
    encoded_df['I'] = [1] * df.shape[0]
    encoded_df['x_A'] = [SERVERS[x] for x in df['server'].values.tolist()]
    encoded_df['x_B'] = [MW[x] for x in df['mw'].values.tolist()]
    encoded_df['x_C'] = [MW_THREADS[x] for x in df['mw_thread'].values.tolist()]

    encoded_df['x_Ax_B'] = encoded_df['x_A'] * encoded_df['x_B']
    encoded_df['x_Ax_C'] = encoded_df['x_A'] * encoded_df['x_C']
    encoded_df['x_Bx_C'] = encoded_df['x_B'] * encoded_df['x_C']
    encoded_df['x_Ax_Bx_C'] = encoded_df['x_A'] * encoded_df['x_B'] * encoded_df['x_C']

    encoded_df['y'] = Y_TRANSFORM(df['y'].values)

    encoded_df['y_mean'] = [-1] * df.shape[0]
    encoded_df['e'] = [None] * df.shape[0]
    encoded_df['y_'] = [None] * df.shape[0]

    for xa, xb, xc in product([-1, 1], [-1, 1], [-1, 1]):
        indexes = (encoded_df['x_A'] == xa) & (encoded_df['x_B'] == xb) & (encoded_df['x_C'] == xc)
        tmp = encoded_df[indexes]

        y_mean = tmp['y'].mean()
        ys = tuple(tmp['y'].values.tolist())
        ers = tuple(tmp['y'].values - y_mean)

        encoded_df.loc[indexes, 'y_mean'] = y_mean

        for i in np.argwhere(indexes).flatten().tolist():
            encoded_df.set_value(i, 'e', ers)
            encoded_df.set_value(i, 'y_', ys)

    encoded_df.drop(columns=['y'], inplace=True)
    encoded_df.rename(columns={'y_': 'y'}, inplace=True)

    tmp = encoded_df.drop_duplicates(subset=['x_A', 'x_B', 'x_C'])
    return tmp


def compute_ss(df):
    errors = [x for t in (df['e'].values.flatten()).tolist() for x in t]
    sse = np.sum(np.array(errors) ** 2)

    keys = [('I', 'I'),
            ('x_A', 'A'),
            ('x_B', 'B'),
            ('x_C', 'C'),
            ('x_Ax_B', 'AB'),
            ('x_Ax_C', 'AC'),
            ('x_Bx_C', 'BC'),
            ('x_Ax_Bx_C', 'ABC')]

    tmp = pd.DataFrame()
    for key in keys:
        q = np.mean(df[key[0]].values * df['y_mean'].values)
        tmp[key[1]] = [q, (2 * 2 ** 3) * q * q if key[1] != 'I' else DEL_NUM]

    df_final = pd.DataFrame()
    sst = tmp.iloc[1].sum() - tmp.iloc[1]['I'] + sse
    for key in keys:
        col = tmp[key[1]].values.tolist()
        col.append(100 * col[-1] / sst if key[1] != 'I' else DEL_NUM)
        df_final[key[1]] = col

    df_final['y'] = [DEL_NUM, sst, DEL_NUM]
    df_final['e'] = [DEL_NUM, sse, DEL_NUM]

    return df_final


def print_latex_computed(df):
    # df.rename(index={0: 'Effect (q)', 1: "Sum of Squares", 2: 'Variation (%)'}, inplace=True)
    df.rename(index={0: 'q', 1: "SS", 2: 'V [%]'}, inplace=True)
    df = df.round(PRECISION)
    ess = df['e'][1]
    estd = np.round(np.sqrt(ess / ((3 - 1) * (2 ** 3))), PRECISION)

    df.drop(columns=['e'],inplace=True)
    df['e']=['', '%s (std %s)' %(ess,estd), '']
    tex = df.to_latex()
    tex = tex.replace('%s.00' % DEL_NUM, '')
    tex = tex.replace('8.888888e+06', '')
    tex = tex.replace('%s.0' % DEL_NUM, '')
    # tex = tex.replace(r'\%', '%')
    tex = tex.replace(r'.0', '')

    print(tex)


def print_latex(df):
    tmp = df.copy().round(PRECISION)
    tmp.rename(columns={
        'x_A': r'$x_A$',
        'x_B': r'$x_B$',
        'x_C': r'$x_C$',
        'x_Ax_B': r'$x_Ax_B$',
        'x_Ax_C': r'$x_Ax_C$',
        'x_Bx_C': r'$x_Bx_C$',
        'x_Ax_Bx_C': r'$x_Ax_Bx_C$',
    },
        inplace=True)

    # tmp.index.name='i'
    for i, row in tmp.iterrows():
        tmp.set_value(i, 'e', tuple([round(x, PRECISION) for x in tmp.loc[i, 'e']]))
        tmp.set_value(i, 'y', tuple([round(x, PRECISION) for x in tmp.loc[i, 'y']]))

    tex = tmp.to_latex()
    tex = tex.replace(r'\$', '$')
    tex = tex.replace(r'\_', '_')
    tex = tex.replace(r'{}', 'i')
    tex = tex.replace(r'y_mean', '$\hat{y}$')
    # tex = tex.replace(r'.0', '')
    print(tex)


def check_normality(values):
    return stats.shapiro(values)


def check_model(df):
    errors = [x for t in (df['e'].values.flatten()).tolist() for x in t]

    print(check_normality(errors)[1])


def main(request_type, label='rt'):
    df = pd.read_csv('stats_%s.csv' % request_type)
    df.drop(columns=['Unnamed: 0'], inplace=True)
    df['y'] = df['c_%s' % label]

    df = encode_df(df)
    print_latex(df)

    # check_model(df)
    df = compute_ss(df)
    print_latex_computed(df)


if __name__ == '__main__':
    ROUDN = int
    DEL_NUM = 8888888
    np.set_printoptions(suppress=True)
    SERVERS = {1: -1, 3: 1}
    MW = {1: -1, 2: 1}
    MW_THREADS = {8: -1, 32: 1}

    # Y_TRANSFORM = np.log
    Y_TRANSFORM = lambda x: x

    LABEL = 'rt'
    PRECISION = 1
    main('set', 'rt')
    print('###########################################')
    LABEL = 'TH'
    PRECISION = 1
    main('set', 'th')
    print('###########################################')
    LABEL = 'rt'
    PRECISION = 1
    main('get', 'rt')
    print('###########################################')
    LABEL = 'TH'
    PRECISION = 1
    main('get', 'th')
