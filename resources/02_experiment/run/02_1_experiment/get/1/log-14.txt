1         Threads
14        Connections per thread
70        Seconds


ALL STATS
========================================================================
Type        Ops/sec     Hits/sec   Misses/sec      Latency       KB/sec 
------------------------------------------------------------------------
Sets           0.00          ---          ---      0.00000         0.00 
Gets        2978.77      2978.77         0.00      4.76200     12065.70 
Waits          0.00          ---          ---      0.00000          --- 
Totals      2978.77      2978.77         0.00      4.76200     12065.70 


Request Latency Distribution
Type     <= msec         Percent
------------------------------------------------------------------------
---
GET       0.190         0.00
GET       0.210         0.00
GET       0.220         0.00
GET       0.230         0.00
GET       0.240         0.00
GET       0.250         0.01
GET       0.260         0.01
GET       0.270         0.01
GET       0.280         0.02
GET       0.290         0.03
GET       0.300         0.04
GET       0.310         0.05
GET       0.320         0.07
GET       0.330         0.10
GET       0.340         0.13
GET       0.350         0.16
GET       0.360         0.21
GET       0.370         0.27
GET       0.380         0.35
GET       0.390         0.45
GET       0.400         0.58
GET       0.410         0.71
GET       0.420         0.89
GET       0.430         1.09
GET       0.440         1.33
GET       0.450         1.63
GET       0.460         1.94
GET       0.470         2.34
GET       0.480         2.77
GET       0.490         3.24
GET       0.500         3.75
GET       0.510         4.32
GET       0.520         4.93
GET       0.530         5.59
GET       0.540         6.22
GET       0.550         6.93
GET       0.560         7.65
GET       0.570         8.45
GET       0.580         9.31
GET       0.590        10.16
GET       0.600        11.02
GET       0.610        11.89
GET       0.620        12.80
GET       0.630        13.73
GET       0.640        14.67
GET       0.650        15.64
GET       0.660        16.65
GET       0.670        17.63
GET       0.680        18.65
GET       0.690        19.68
GET       0.700        20.68
GET       0.710        21.76
GET       0.720        22.85
GET       0.730        23.87
GET       0.740        24.92
GET       0.750        26.02
GET       0.760        27.09
GET       0.770        28.15
GET       0.780        29.21
GET       0.790        30.32
GET       0.800        31.39
GET       0.810        32.49
GET       0.820        33.55
GET       0.830        34.64
GET       0.840        35.72
GET       0.850        36.75
GET       0.860        37.75
GET       0.870        38.77
GET       0.880        39.77
GET       0.890        40.74
GET       0.900        41.70
GET       0.910        42.66
GET       0.920        43.53
GET       0.930        44.35
GET       0.940        45.15
GET       0.950        45.90
GET       0.960        46.60
GET       0.970        47.32
GET       0.980        48.00
GET       0.990        48.72
GET       1.000        52.04
GET       1.100        56.14
GET       1.200        58.61
GET       1.300        60.18
GET       1.400        61.34
GET       1.500        62.19
GET       1.600        62.86
GET       1.700        63.39
GET       1.800        63.81
GET       1.900        64.18
GET       2.000        64.50
GET       2.100        64.86
GET       2.200        65.15
GET       2.300        65.44
GET       2.400        65.69
GET       2.500        65.93
GET       2.600        66.17
GET       2.700        66.39
GET       2.800        66.57
GET       2.900        66.76
GET       3.000        66.96
GET       3.100        67.12
GET       3.200        67.27
GET       3.300        67.42
GET       3.400        67.57
GET       3.500        67.70
GET       3.600        67.82
GET       3.700        67.94
GET       3.800        68.03
GET       3.900        68.13
GET       4.000        68.21
GET       4.100        68.30
GET       4.200        68.41
GET       4.300        68.53
GET       4.400        68.64
GET       4.500        68.72
GET       4.600        68.81
GET       4.700        68.91
GET       4.800        69.01
GET       4.900        69.10
GET       5.000        69.17
GET       5.100        69.23
GET       5.200        69.29
GET       5.300        69.35
GET       5.400        69.40
GET       5.500        69.46
GET       5.600        69.50
GET       5.700        69.57
GET       5.800        69.62
GET       5.900        69.67
GET       6.000        69.72
GET       6.100        69.77
GET       6.200        69.82
GET       6.300        69.85
GET       6.400        69.90
GET       6.500        69.93
GET       6.600        69.99
GET       6.700        70.04
GET       6.800        70.10
GET       6.900        70.17
GET       7.000        70.22
GET       7.100        70.28
GET       7.200        70.35
GET       7.300        70.40
GET       7.400        70.44
GET       7.500        70.48
GET       7.600        70.53
GET       7.700        70.58
GET       7.800        70.65
GET       7.900        70.72
GET       8.000        70.77
GET       8.100        70.83
GET       8.200        70.90
GET       8.300        70.97
GET       8.400        71.04
GET       8.500        71.10
GET       8.600        71.15
GET       8.700        71.21
GET       8.800        71.28
GET       8.900        71.35
GET       9.000        71.40
GET       9.100        71.46
GET       9.200        71.52
GET       9.300        71.59
GET       9.400        71.66
GET       9.500        71.71
GET       9.600        71.77
GET       9.700        71.86
GET       9.800        71.92
GET       9.900        71.99
GET      10.000        72.51
GET      11.000        73.78
GET      12.000        76.34
GET      13.000        82.83
GET      14.000        97.76
GET      15.000        98.74
GET      16.000        99.03
GET      17.000        99.19
GET      18.000        99.31
GET      19.000        99.38
GET      20.000        99.42
GET      21.000        99.46
GET      22.000        99.52
GET      23.000        99.57
GET      24.000        99.62
GET      25.000        99.68
GET      26.000        99.74
GET      27.000        99.80
GET      28.000        99.92
GET      29.000        99.97
GET      30.000        99.98
GET      31.000        99.98
GET      32.000        99.99
GET      33.000        99.99
GET      43.000       100.00
GET      44.000       100.00
GET      49.000       100.00
GET      50.000       100.00
---
