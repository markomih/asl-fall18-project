#!/usr/bin/env bash


function populate_db {
    PARAMS1="-s ${SERVER1_PRIVATE} --port=${SERVER1_PORT} --protocol=memcache_text --key-maximum=${KEY_MAX} --data-size=${DATA_SIZE} --clients=100 --threads=${THREADS} --test-time=200 --ratio=1:0"
    CMD1="/home/maki/memtier_benchmark-master/memtier_benchmark ${PARAMS1}"

    PARAMS2="-s ${SERVER2_PRIVATE} --port=${SERVER2_PORT} --protocol=memcache_text --key-maximum=${KEY_MAX} --data-size=${DATA_SIZE} --clients=100 --threads=${THREADS} --test-time=200 --ratio=1:0"
    CMD2="/home/maki/memtier_benchmark-master/memtier_benchmark ${PARAMS2}"

    echo ${CLIENT1}

    $(ssh ${CLIENT1} "${CMD1}") &
    $(ssh ${CLIENT1} "${CMD2}") &
    wait
    echo "DB populated"
}

# GETs

CLIENT1=storeml4kjdlgl3y3csshpublicip1.westeurope.cloudapp.azure.com

SERVER1=storeml4kjdlgl3y3csshpublicip6.westeurope.cloudapp.azure.com
SERVER1_PORT=11211
SERVER2=storeml4kjdlgl3y3csshpublicip7.westeurope.cloudapp.azure.com
SERVER2_PORT=11211

SERVER1_PRIVATE=10.0.0.11
SERVER2_PRIVATE=10.0.0.6


SERVER_DIR1="02_1_experiment"
SERVER_DIR2="02_2_experiment"

TYPE="set"
THREADS=1
TEST_TIME=70
DSTAT_TIME=60
DATA_SIZE=4096
KEY_MAX=10000

SET_RATIO=1
GET_RATIO=0
clients=( 1 2 4 6 8 10 14 22 32 )
run=( 1 2 3 )

populate_db


# prepare directories
ssh ${SERVER1} "rm *.csv"
ssh ${SERVER2} "rm *.csv"
for experiment in "${run[@]}"
do
        MKDIR="mkdir -p ${SERVER_DIR1}/${TYPE}/${experiment}"
        RMFILES="rm ${SERVER_DIR1}/${TYPE}/${experiment}/*"
        ssh ${CLIENT1} "${MKDIR};${RMFILES}"

        MKDIR="mkdir -p ${SERVER_DIR2}/${TYPE}/${experiment}"
        RMFILES="rm ${SERVER_DIR2}/${TYPE}/${experiment}/*"
        ssh ${CLIENT1} "${MKDIR};${RMFILES}"
done


for experiment in "${run[@]}"
do
    for client in "${clients[@]}"
    do
        CDDIR1="cd ${SERVER_DIR1}/${TYPE}/${experiment}"
        CDDIR2="cd ${SERVER_DIR2}/${TYPE}/${experiment}"

        DSTAT_CMD_SERVER1="dstat --output server1_${TYPE}_${experiment}_${client}.csv 1 ${DSTAT_TIME}"
        DSTAT_CMD_SERVER2="dstat --output server2_${TYPE}_${experiment}_${client}.csv 1 ${DSTAT_TIME}"

        PARAMS="--clients=${client} --threads=${THREADS} --test-time=${TEST_TIME} --data-size=${DATA_SIZE} --ratio=${SET_RATIO}:${GET_RATIO} --key-maximum=${KEY_MAX} --out-file=log-${client}.txt --protocol=memcache_text "
        PARAMS1="-s ${SERVER1_PRIVATE} --port=${SERVER1_PORT} ${PARAMS}"
        PARAMS2="-s ${SERVER2_PRIVATE} --port=${SERVER2_PORT} ${PARAMS}"

        CMD1="/home/maki/memtier_benchmark-master/memtier_benchmark ${PARAMS1}"
        CMD2="/home/maki/memtier_benchmark-master/memtier_benchmark ${PARAMS2}"

        echo "executing:"
        echo ${CMD1}
        echo ${CMD2}
        echo "run=$experiment; client=$client"

        $(ssh ${CLIENT1} "${CDDIR1};${CMD1};") &
        $(ssh ${CLIENT1} "${CDDIR2};${CMD2};") &

        # RUN Dstat Servers 1
        $(ssh ${SERVER1} "${DSTAT_CMD_SERVER1}") &
        $(ssh ${SERVER2} "${DSTAT_CMD_SERVER2}") &

        echo "WAITING . . ."
        wait
        echo "DONE"
        echo
    done
done


# GETs
TYPE="get"
SET_RATIO=0
GET_RATIO=1

for experiment in "${run[@]}"
do
        MKDIR="mkdir -p ${SERVER_DIR1}/${TYPE}/${experiment}"
        RMFILES="rm ${SERVER_DIR1}/${TYPE}/${experiment}/*"
        ssh ${CLIENT1} "${MKDIR};${RMFILES}"

        MKDIR="mkdir -p ${SERVER_DIR2}/${TYPE}/${experiment}"
        RMFILES="rm ${SERVER_DIR2}/${TYPE}/${experiment}/*"
        ssh ${CLIENT1} "${MKDIR};${RMFILES}"
done


for experiment in "${run[@]}"
do
    for client in "${clients[@]}"
    do
        CDDIR1="cd ${SERVER_DIR1}/${TYPE}/${experiment}"
        CDDIR2="cd ${SERVER_DIR2}/${TYPE}/${experiment}"

        DSTAT_CMD_SERVER1="dstat --output server1_${TYPE}_${experiment}_${client}.csv 1 ${DSTAT_TIME}"
        DSTAT_CMD_SERVER2="dstat --output server2_${TYPE}_${experiment}_${client}.csv 1 ${DSTAT_TIME}"

        PARAMS="--clients=${client} --threads=${THREADS} --test-time=${TEST_TIME} --data-size=${DATA_SIZE} --ratio=${SET_RATIO}:${GET_RATIO} --key-maximum=${KEY_MAX} --out-file=log-${client}.txt --protocol=memcache_text "
        PARAMS1="-s ${SERVER1_PRIVATE} --port=${SERVER1_PORT} ${PARAMS}"
        PARAMS2="-s ${SERVER2_PRIVATE} --port=${SERVER2_PORT} ${PARAMS}"

        CMD1="/home/maki/memtier_benchmark-master/memtier_benchmark ${PARAMS1}"
        CMD2="/home/maki/memtier_benchmark-master/memtier_benchmark ${PARAMS2}"

        echo "executing:"
        echo ${CMD1}
        echo ${CMD2}
        echo "run=$experiment; client=$client"

        $(ssh ${CLIENT1} "${CDDIR1};${CMD1};") &
        $(ssh ${CLIENT1} "${CDDIR2};${CMD2};") &

        # RUN Dstat Servers 1
        $(ssh ${SERVER1} "${DSTAT_CMD_SERVER1}") &
        $(ssh ${SERVER2} "${DSTAT_CMD_SERVER2}") &

        echo "WAITING . . ."
        wait
        echo "DONE"
        echo
    done
done


# copy files
mkdir -p server1
mkdir -p server2

scp -r ${SERVER1}:*.csv ./server1/
scp -r ${SERVER2}:*.csv ./server2/

scp -r ${CLIENT1}:02_1_experiment 02_1_experiment
scp -r ${CLIENT1}:02_2_experiment 02_2_experiment

# STOP VMs

az vm stop --resource-group asl-group --name Client1
az vm stop --resource-group asl-group --name Server1
az vm stop --resource-group asl-group --name Server2

az vm deallocate --resource-group asl-group --name Client1
az vm deallocate --resource-group asl-group --name Server1
az vm deallocate --resource-group asl-group --name Server2