import re
import subprocess

from utils import get_client_rt_per_mw, get_client_th_per_mw, get_mw_rt_per_mw, get_mw_system_requests_per_mw

import numpy as np
import pandas as pd


def flatten(l):
    return sum(l, [])


def get_mw_rt(number_of_clients, request_type, mw_thread):
    mw1_rt, mw1_ret, mw1_s = [], [], []
    mw2_rt, mw2_ret, mw2_s = [], [], []
    for run in ['1', '2', '3']:
        tmp = get_mw_rt_per_mw(number_of_clients, request_type, run, mw_thread)

        mw1_rt.append(tmp['mw1_responseTimer'])
        mw2_rt.append(tmp['mw2_responseTimer'])

        mw1_ret.append(tmp['mw1_returningResponseTimer'])
        mw2_ret.append(tmp['mw2_returningResponseTimer'])

        # worker S
        mw1_s.append(tmp['mw1_processingTimer'] + tmp['mw1_internalQueueTimer'] + tmp['mw1_serviceTimer'] + tmp[
            'mw1_waitingServiceTimer'])
        mw2_s.append(tmp['mw2_processingTimer'] + tmp['mw2_internalQueueTimer'] + tmp['mw2_serviceTimer'] + tmp[
            'mw2_waitingServiceTimer'])

    print(mw1_rt)

    mw1_rt, mw1_s, mw1_ret = np.mean(mw1_rt), np.mean(mw1_s), np.mean(mw1_ret)
    mw2_rt, mw2_s, mw2_ret = np.mean(mw2_rt), np.mean(mw2_s), np.mean(mw2_ret)

    return (mw1_rt, mw1_s, mw1_ret), (mw2_rt, mw2_s, mw2_ret)


def get_client_mwrt(number_of_clients, mw_thread, request_type):
    c1_rt, c2_rt, c1_th, c2_th = [], [], [], []
    for run in ['1', '2', '3']:
        c1rt, c2rt = get_client_rt_per_mw(number_of_clients, mw_thread, request_type, run)
        c1_rt.append(c1rt)
        c2_rt.append(c2rt)

        c1th, c2th = get_client_th_per_mw(number_of_clients, mw_thread, request_type, run)
        c1_th.append(c1th)
        c2_th.append(c2th)

    print(c1_rt)
    c1_rt, c2_rt = np.mean(c1_rt), np.mean(c2_rt)
    c1_th, c2_th = np.mean(c1_th), np.mean(c2_th)

    return (c1_rt, c1_th), (c2_rt, c2_th)


def get_s(c1_rt, mw1_rt, c2_rt, mw2_rt, mw1_s, mw2_s, mw1_ret, mw2_ret):
    s = np.zeros(8)

    s[0] = c1_rt - mw1_rt
    s[1] = c2_rt - mw2_rt

    s[2] = 0.001
    s[3] = 0.001

    s[4] = mw1_s
    s[5] = mw2_s

    s[6] = mw1_ret
    s[7] = mw2_ret
    s_str = '[%f,%f,%f,%f,%f,%f,%f,%f]' % (s[0], s[1], s[2], s[3], s[4], s[5], s[6], s[7])
    return s, s_str


def remove_at(i, s):
    return s[:i] + s[i + 1:]


def get_values(s_str, workers=64, clients=1):
    clients = clients * 6
    cmd = 'octave --eval "cd /home/maki/octave/queueing-1.2.6; [RT, TH, Q, U]=net2(%s,%s,%s)"' % (
        workers, clients, s_str)
    # ret = os.system(cmd)
    ret = subprocess.run(cmd, shell=True, stdout=subprocess.PIPE, universal_newlines=True).stdout
    while ret.find('Columns') != -1 and ret.find(':') != -1:
        i2 = ret.find(':')
        i1 = ret.find('Columns')
        for i in range(i2, i1 - 1, -1):
            ret = remove_at(i, ret)

    ret = re.sub(' +', ' ', ret)
    ret = ret.split('=')
    rt = float(ret[1].replace('\nTH', ''))
    th = float(ret[2].replace('\nQ', ''))

    q = [float(x) for x in ret[3].replace('\n', '').replace('U', '').strip().split(' ') if x != '']
    u = [float(x) for x in ret[4].replace('\n', '').replace('U', '').strip().split(' ') if x != '']

    return rt, th, q, u


def get_qm(clients, mw_thread, request_type):
    mw1_q, mw2_q = [], []

    for run in ['1', '2', '3']:
        m1, m2 = get_mw_system_requests_per_mw(clients, request_type, run, mw_thread)
        mw1_q.append(m1)
        mw2_q.append(m2)

    mw1_q, mw2_q = np.mean(mw1_q), np.mean(mw2_q)

    return mw1_q, mw2_q


def main():
    tmp = {
        'Workload': [],
        'Workers': [],
        'Clients': [],
        'RT': [],
        'TH': [],
        'RT_M': [],
        'TH_M': [],
    }

    full_tmp = {}
    for i in range(DEVICES):
        full_tmp['D_%s' % (i + 1)] = []

    for i in range(len(LOAD)):
        tmp['Workload'].append(LOAD[i])
        tmp['Workers'].append(THREAD[i])
        tmp['Clients'].append(CLIENT[i] * 6)

        (c1_rt, c1_th), (c2_rt, c2_th) = get_client_mwrt(CLIENT[i], THREAD[i], REQUEST_TYPE[i])
        (mw1_rt, mw1_s, mw1_ret), (mw2_rt, mw2_s, mw2_ret) = get_mw_rt(CLIENT[i], REQUEST_TYPE[i], THREAD[i])

        print((c1_rt, c1_th), (c2_rt, c2_th))
        print((mw1_rt, mw1_s, mw1_ret), (mw2_rt, mw2_s, mw2_ret))

        s, s_str = get_s(c1_rt, mw1_rt, c2_rt, mw2_rt, mw1_s, mw2_s, mw1_ret, mw2_ret)
        rt, th, q, u = get_values(s_str, THREAD[i], CLIENT[i])

        tmp['RT'].append(rt * 1000)
        tmp['TH'].append(th)

        tmp['RT_M'].append((c1_rt + c2_rt) / 2)
        tmp['TH_M'].append(c1_th + c2_th)

        mw1_qm, mw2_qm = get_qm(CLIENT[i], THREAD[i], REQUEST_TYPE[i])
        for j in range(DEVICES):
            full_tmp['D_%s' % (j + 1)].append(s[j])
            full_tmp['D_%s' % (j + 1)].append(u[j])
            full_tmp['D_%s' % (j + 1)].append(q[j])

            if j + 1 == 5:
                val = mw1_qm
            elif j + 1 == 6:
                val = mw2_qm
            else:
                val = DEL_NUM

            full_tmp['D_%s' % (j + 1)].append(val)

    df = pd.DataFrame(tmp)
    print(df.round(2))
    print(df.round(2).to_latex(escape=False))

    indexes = flatten([['S_%s' % i, 'U_%s' % i, 'Q_%s' % i, 'Q_{m%s}' % i] for i in range(1, 5)])
    df_full = pd.DataFrame(full_tmp, index=pd.Index(indexes))

    df_full = df_full.round(3)
    print(df_full)
    latex = df_full.to_latex(escape=False)
    print(latex.replace('%.3f' % DEL_NUM, ''))


if __name__ == '__main__':
    LOAD = ('Write', 'Write', 'Read', 'Read')
    REQUEST_TYPE = ('set', 'set', 'get', 'get')
    THREAD = (8, 64, 8, 64)
    CLIENT = (2, 14, 1, 1)
    DEL_NUM = 8888
    DEVICES=8
    main()
