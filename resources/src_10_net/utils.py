from io import StringIO
import re
import numpy as np

import os
import pandas as pd


# REMOTE_DIR='remote_tmp'
def remote_dir(request_type):
    if request_type == 'get':
        return 'remote_b'
    return 'remote'


REMOVE_LIMIT = (5, -5)


def parse_file(file_path, request_type='th', misses=False):
    # src_type: None for th,'set', or 'get'
    if request_type == 'set':
        matching = 'Sets'
        position = 4
    elif request_type == 'get' or misses:
        matching = 'Gets'
        position = 3 if misses else 4
    elif request_type == 'th':
        matching = 'Totals'
        position = 1
    else:
        raise Exception('incorrect request type')

    with open(file_path) as f:
        for line in f:
            if line.startswith(matching):
                data = [x for x in line.split(" ") if x != ""][position]
                return float(data)


def get_mw_dir(client, request_type, run, mw_thread):
    return os.path.join('..', '04_experiment', remote_dir(request_type), 'MW1',
                        'MW_%s_%s_%s_%s' % (request_type, mw_thread, run, client)), \
           os.path.join('..', '04_experiment', remote_dir(request_type), 'MW2',
                        'MW_%s_%s_%s_%s' % (request_type, mw_thread, run, client))


def _get_client_files(dir_paths, number_of_clients, mw_thread):
    file_paths = []
    for dir_path in dir_paths:
        for root, _, files in os.walk(dir_path):
            for file in files:
                if re.match(r'log-%s-%s.txt' % (number_of_clients, mw_thread), file):
                    file_paths.append(os.path.join(root, file))
    return file_paths


def get_server_file(client, request_type, run, mw_thread):
    return os.path.join('..', '04_experiment', remote_dir(request_type), 'server1',
                        'server1_%s_%s_%s_%s.csv' % (mw_thread, request_type, run, client))


def get_client_files(number_of_clients, mw_thread, request_type, run):
    client_files = ['client1', 'client2', 'client3']
    dir_paths1 = [
        os.path.join('..', '04_experiment', remote_dir(request_type), c, '04_1_experiment', '04_1_experiment',
                     request_type, run)
        for c in client_files]
    dir_paths2 = [
        os.path.join('..', '04_experiment', remote_dir(request_type), c, '04_2_experiment', '04_2_experiment',
                     request_type, run)
        for c in client_files]
    # dir_paths = [os.path.join('..', '03_experiment', remote_dir(request_type), c, request_type, run) for c in client_files]

    file_paths1 = _get_client_files(dir_paths1, number_of_clients, mw_thread)
    file_paths2 = _get_client_files(dir_paths2, number_of_clients, mw_thread)
    return file_paths1, file_paths2


def get_client_th(number_of_clients, mw_thread, request_type, run):
    files1, files2 = get_client_files(number_of_clients, mw_thread, request_type, run)

    t1 = np.sum([parse_file(file, 'th') for file in files1])
    t2 = np.sum([parse_file(file, 'th') for file in files2])
    return t1 + t2


def get_client_th_per_mw(number_of_clients, mw_thread, request_type, run):
    files1, files2 = get_client_files(number_of_clients, mw_thread, request_type, run)

    t1 = np.sum([parse_file(file, 'th') for file in files1])
    t2 = np.sum([parse_file(file, 'th') for file in files2])
    return t1, t2


def get_client_rt(number_of_clients, mw_thread, request_type, run):
    files1, files2 = get_client_files(number_of_clients, mw_thread, request_type, run)
    t1 = np.mean([parse_file(file, request_type) for file in files1])
    t2 = np.mean([parse_file(file, request_type) for file in files2])
    return (t1 + t2) / 2.


def get_client_rt_per_mw(number_of_clients, mw_thread, request_type, run):
    files1, files2 = get_client_files(number_of_clients, mw_thread, request_type, run)
    t1 = np.mean([parse_file(file, request_type) for file in files1])
    t2 = np.mean([parse_file(file, request_type) for file in files2])
    return t1, t2


def get_client_misses(number_of_clients, mw_thread, request_type, run):
    if request_type == 'set':
        return 0
    files1, files2 = get_client_files(number_of_clients, mw_thread, request_type, run)
    # files = get_client_files(number_of_clients,mw_thread, 'set', run)
    t1 = np.round(np.sum([parse_file(file, request_type, misses=True) for file in files1]))
    t2 = np.round(np.sum([parse_file(file, request_type, misses=True) for file in files2]))

    return np.round(t1 + t2)


def get_mw_th(client, request_type, run, mw_thread, seconds=70):
    dir_path1, dir_path2 = get_mw_dir(client, request_type, run, mw_thread)
    file1 = os.path.join(dir_path1, 'counter_log_%s_false.txt' % mw_thread)
    file2 = os.path.join(dir_path2, 'counter_log_%s_false.txt' % mw_thread)
    df1, df2 = pd.read_csv(file1), pd.read_csv(file2)

    to_ret = df1[request_type].max() + df2[request_type].max()

    if request_type == 'get':
        to_ret -= df1['cachemisses'].max()
        to_ret -= df2['cachemisses'].max()

    return to_ret / seconds


def get_mw_rt(number_of_clients, request_type, run, mw_thread, seconds=70):
    dir_path1, dir_path2 = get_mw_dir(number_of_clients, request_type, run, mw_thread)
    file1 = os.path.join(dir_path1, 'request_log_%s_false.csv' % mw_thread)
    file2 = os.path.join(dir_path2, 'request_log_%s_false.csv' % mw_thread)

    df1 = pd.read_csv(file1)
    df2 = pd.read_csv(file2)

    df1 = df1.loc[df1['requestType'] == request_type.upper()]
    df2 = df2.loc[df2['requestType'] == request_type.upper()]

    df1 = df1[df1['second'] < seconds]
    df2 = df2[df2['second'] < seconds]

    # df1 = df1.iloc[REMOVE_LIMIT[0]:REMOVE_LIMIT[1]]
    # df2 = df2.iloc[REMOVE_LIMIT[0]:REMOVE_LIMIT[1]]

    rt_by_sec1 = df1.groupby(['second']).mean()
    rt_by_sec2 = df2.groupby(['second']).mean()

    return abs(rt_by_sec1['responseTimer'].mean() + rt_by_sec2['responseTimer'].mean()) / 2, \
           abs(rt_by_sec1['internalQueueTimer'].mean() + rt_by_sec2['internalQueueTimer'].mean()) / 2, \
           abs(rt_by_sec1['processingTimer'].mean() + rt_by_sec2['processingTimer'].mean()) / 2, \
           abs(rt_by_sec1['serviceTimer'].mean() + rt_by_sec2['serviceTimer'].mean()) / 2, \
           abs(rt_by_sec1['waitingServiceTimer'].mean() + rt_by_sec2['waitingServiceTimer'].mean()) / 2, \
           abs(rt_by_sec1['returningResponseTimer'].mean() + rt_by_sec2['returningResponseTimer'].mean()) / 2


def get_mw_rt_per_mw(number_of_clients, request_type, run, mw_thread, seconds=70):
    dir_path1, dir_path2 = get_mw_dir(number_of_clients, request_type, run, mw_thread)
    file1 = os.path.join(dir_path1, 'request_log_%s_false.csv' % mw_thread)
    file2 = os.path.join(dir_path2, 'request_log_%s_false.csv' % mw_thread)

    df1 = pd.read_csv(file1)
    df2 = pd.read_csv(file2)

    df1 = df1.loc[df1['requestType'] == request_type.upper()]
    df2 = df2.loc[df2['requestType'] == request_type.upper()]

    df1 = df1[df1['second'] < seconds]
    df2 = df2[df2['second'] < seconds]

    # df1 = df1.iloc[REMOVE_LIMIT[0]:REMOVE_LIMIT[1]]
    # df2 = df2.iloc[REMOVE_LIMIT[0]:REMOVE_LIMIT[1]]

    rt_by_sec1 = df1.groupby(['second']).mean()
    rt_by_sec1 = rt_by_sec1[rt_by_sec1['responseTimer'] < 100]
    # rt_by_sec1= rt_by_sec1[rt_by_sec1.responseTimer < np.percentile(rt_by_sec1.responseTimer, 99.9)]
    rt_by_sec2 = df2.groupby(['second']).mean()
    rt_by_sec2 = rt_by_sec2[rt_by_sec2['responseTimer'] < 100]
    # rt_by_sec2= rt_by_sec2[rt_by_sec2.responseTimer < np.percentile(rt_by_sec2.responseTimer, 99.9)]

    return {
        'mw1_responseTimer': abs(rt_by_sec1['responseTimer'].mean()),
        'mw2_responseTimer': abs(rt_by_sec2['responseTimer'].mean()),

        'mw1_internalQueueTimer': abs(rt_by_sec1['internalQueueTimer'].mean()),
        'mw2_internalQueueTimer': abs(rt_by_sec2['internalQueueTimer'].mean()),

        'mw1_processingTimer': abs(rt_by_sec1['processingTimer'].mean()),
        'mw2_processingTimer': abs(rt_by_sec2['processingTimer'].mean()),

        'mw1_serviceTimer': abs(rt_by_sec1['serviceTimer'].mean()),
        'mw2_serviceTimer': abs(rt_by_sec2['serviceTimer'].mean()),

        'mw1_waitingServiceTimer': abs(rt_by_sec1['waitingServiceTimer'].mean()),
        'mw2_waitingServiceTimer': abs(rt_by_sec2['waitingServiceTimer'].mean()),

        'mw1_returningResponseTimer': abs(rt_by_sec1['returningResponseTimer'].mean()),
        'mw2_returningResponseTimer': abs(rt_by_sec2['returningResponseTimer'].mean()),
    }


def get_mw_misses(number_of_clients, request_type, run, mw_thread):
    if request_type == 'set':
        return 0

    dir_path1, dir_path2 = get_mw_dir(number_of_clients, request_type, run, mw_thread)
    file1 = os.path.join(dir_path1, 'counter_log_%s_false.txt' % mw_thread)
    file2 = os.path.join(dir_path2, 'counter_log_%s_false.txt' % mw_thread)

    df1 = pd.read_csv(file1)
    df2 = pd.read_csv(file2)
    t1 = df1['cachemisses'].max() / df1['second'].max()
    t2 = df2['cachemisses'].max() / df2['second'].max()
    return np.round(t1 + t2)


def get_mw_queue_length(number_of_clients, request_type, run, mw_thread):
    dir_path1, dir_path2 = get_mw_dir(number_of_clients, request_type, run, mw_thread)

    file1 = os.path.join(dir_path1, 'sys_log_%s_false.csv' % mw_thread)
    file2 = os.path.join(dir_path2, 'sys_log_%s_false.csv' % mw_thread)

    df1, df2 = pd.read_csv(file1), pd.read_csv(file2)
    t1 = df1['queueSize'].sum() / df1['second'].max()
    t2 = df2['queueSize'].sum() / df2['second'].max()
    return np.round((t1 + t2) / 2.)


def get_mw_system_requests_per_mw(number_of_clients, request_type, run, mw_thread, seconds=70):
    dir_path1, dir_path2 = get_mw_dir(number_of_clients, request_type, run, mw_thread)

    file1 = os.path.join(dir_path1, 'sys_log_%s_false.csv' % mw_thread)
    file2 = os.path.join(dir_path2, 'sys_log_%s_false.csv' % mw_thread)

    df1, df2 = pd.read_csv(file1), pd.read_csv(file2)
    df1=df1[df1['second']<seconds]
    df2=df2[df2['second']<seconds]

    t1 = df1['systemRequests'].mean()
    t2 = df2['systemRequests'].mean()

    return t1, t2


def get_mw_arrival_rate(number_of_clients, request_type, run, mw_thread):
    dir_path1, dir_path2 = get_mw_dir(number_of_clients, request_type, run, mw_thread)

    file1 = os.path.join(dir_path1, 'sys_log_%s_false.csv' % mw_thread)
    file2 = os.path.join(dir_path2, 'sys_log_%s_false.csv' % mw_thread)

    df1, df2 = pd.read_csv(file1), pd.read_csv(file2)

    t1 = df1['expectedArrivalRate'].sum() / df1['second'].max()
    t2 = df2['expectedArrivalRate'].sum() / df2['second'].max()
    return np.round((t1 + t2) / 2.)


def _parse_dstat(file):
    content = []
    with open(file, 'r') as f:
        for i, line in enumerate(f):
            if line.startswith(r'"usr"'):
                content = []
            content.append(line)
    content = '\n'.join(content)
    return pd.read_csv(StringIO(content))


def get_dstat(number_of_clients, request_type, run, mw_thread):
    dir_path1, dir_path2 = get_mw_dir(number_of_clients, request_type, run, mw_thread)
    file1 = os.path.join(dir_path1, 'dstat.csv')
    file2 = os.path.join(dir_path2, 'dstat.csv')

    df1 = _parse_dstat(file1)
    df2 = _parse_dstat(file2)

    df1 = df1.iloc[REMOVE_LIMIT[0]:REMOVE_LIMIT[1]]
    df2 = df2.iloc[REMOVE_LIMIT[0]:REMOVE_LIMIT[1]]

    return (df1['recv'].mean() + df2['recv'].mean()) / 2., \
           (df1['send'].mean() + df2['send'].mean()) / 2., \
           (df1['idl'].mean() + df2['idl'].mean()) / 2.,


def get_server_dstat(number_of_clients, request_type, run, mw_thread):
    server_path = get_server_file(number_of_clients, request_type, run, mw_thread)

    df = _parse_dstat(server_path)

    df = df.iloc[REMOVE_LIMIT[0]:REMOVE_LIMIT[1]]
    return df['recv'].mean(), \
           df['send'].mean(), \
           df['idl'].mean()
