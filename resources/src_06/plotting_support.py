import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt


# def plot_queue_size(df, dst,threads, x_label='Number of clients', y_label='Internal Queue size', legend='threads'):
#     tmp_df=df[df['mw_thread']==threads]
#     data = pd.DataFrame()
#     data[x_label] = tmp_df['client'] * CLIENT_FACTOR
#     data[y_label] = tmp_df['mw_queueLength']
#     data[legend] = tmp_df['mw_thread']
#
#     fig = plt.figure()
#     ax = fig.add_subplot(1, 1, 1)
#
#     if threads==8:
#         color=0
#     elif threads==64:
#         color=3
#     sns.lineplot(x=x_label, y=y_label, hue=legend, data=data, legend="full", ci=CI, marker=MARKER,palette=[PALETTE[color]])
#
#     x_thicks = [x for x in sorted(plt.xticks()[0].tolist() + [data[x_label].min()]) if x >= data[x_label].min()]
#     ax.set_xticks(x_thicks)
#
#     ax.set_ylim(bottom=0, top=180)
#     ax.set_xlim(left=0, right=data[x_label].max() + 1)
#
#     ax.grid(True)
#     fig.show()
#     fig.savefig(dst)
def plot_queue_size(df, dst, x_label='Key size', y_label='Internal Queue size'):
    data = pd.DataFrame()
    data[x_label] = df['key_size']
    data[y_label] = df['mw_queueLength']

    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)
    sns.lineplot(x=x_label, y=y_label, data=data, legend="full", ci=CI, marker=MARKER, palette=PALETTE)

    ax.set_xticks(KEY_SIZE)
    ax.set_ylim(bottom=0)

    ax.grid(True)
    fig.show()
    fig.savefig(dst)


def plot_system_requests(df, dst, x_label='Key size', y_label='Requests in system'):
    data = pd.DataFrame()
    data[x_label] = df['key_size']
    data[y_label] = df['mw_systemRequests']

    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)
    sns.lineplot(x=x_label, y=y_label, data=data, legend="full", ci=CI, marker=MARKER, palette=PALETTE)

    ax.set_xticks(KEY_SIZE)
    ax.set_ylim(bottom=0, top=8)

    ax.grid(True)
    fig.show()
    fig.savefig(dst)


def plot_cpu_idle(df, dst, x_label='Key size', y_label='CPU idle percentage'):
    data = pd.DataFrame()
    data[x_label] = df['key_size']
    data[y_label] = df['mw_idl']

    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)
    sns.lineplot(x=x_label, y=y_label, data=data, legend="full", ci=CI, marker=MARKER, palette=PALETTE)

    ax.set_ylim(bottom=0, top=100)
    ax.set_xticks(KEY_SIZE)

    ax.grid(True)
    fig.show()
    fig.savefig(dst)


# def plot_memory(df, dst, x_label='Number of clients', y_label='Memory consumed by middleware [MBs]', legend='threads'):
#     data = pd.DataFrame()
#     data[x_label] = df['client'] * CLIENT_FACTOR
#     data[y_label] = df['mw_usedMemory'] * (1./2**20)
#     data[legend] = df['mw_thread']
#
#     fig = plt.figure()
#     ax = fig.add_subplot(1, 1, 1)
#     sns.lineplot(x=x_label, y=y_label, hue=legend, data=data, legend="full", ci=CI, marker=MARKER, palette=PALETTE)
#
#     x_thicks = [x for x in sorted(plt.xticks()[0].tolist() + [data[x_label].min()]) if x >= data[x_label].min()]
#     ax.set_xticks(x_thicks)
#
#     ax.set_ylim(bottom=0)
#     ax.set_xlim(left=0, right=data[x_label].max() + 1)
#
#     ax.grid(True)
#     fig.show()
#     fig.savefig(dst)


def plot_client_send(df, dst, x_label='Key size', y_label='MBs received from clients'):
    data = pd.DataFrame()
    data[x_label] = df['key_size']
    data[y_label] = df['client_send']

    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)
    sns.lineplot(x=x_label, y=y_label, data=data, legend="full", ci=CI, marker=MARKER, palette=PALETTE)

    ax.set_ylim(bottom=0)
    ax.set_xticks(KEY_SIZE)

    ax.grid(True)
    fig.show()
    fig.savefig(dst)


def plot_server_to_mw(df, dst, x_label='Key size', y_label='MBs sent from servers to middlewares'):
    data = pd.DataFrame()
    data[x_label] = df['key_size']
    data[y_label] = df['server_send']

    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)
    sns.lineplot(x=x_label, y=y_label, data=data, legend="full", ci=CI, marker=MARKER, palette=PALETTE)

    ax.set_ylim(bottom=0, top=13)
    ax.set_xlim(left=0)
    ax.set_xticks(KEY_SIZE)

    plt.hlines(y=12.5, xmin=0, xmax=max(KEY_SIZE) + 1, color='g', linestyle='--', label='bandwidth')
    plt.legend()

    ax.grid(True)
    fig.show()
    fig.savefig(dst)


def plot_time_decomposition(df, dst, x_label='Key size', y_label='Time request spend in MW [ms]'):
    data = df.copy()
    data[x_label] = df['key_size']
    # data[y_label] = df['client_send']
    palette = sns.color_palette("hls", 5)

    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)
    ax = sns.lineplot(x=x_label, y='mw_rt', data=data, legend=None, ci=CI, marker=MARKER, palette=palette,
                      label='Total time')
    ax = sns.lineplot(x=x_label, y='r_internalQueueTimer', data=data, legend=None, ci=CI, marker=MARKER,
                      palette=palette, label='Internal queue timer')
    ax = sns.lineplot(x=x_label, y='r_waitingServiceTimer', data=data, legend=None, ci=CI, marker=MARKER,
                      palette=palette, label='Waiting service timer')
    ax = sns.lineplot(x=x_label, y='r_processingTimer', data=data, legend=None, ci=CI, marker=MARKER,
                      palette=palette, label='Processing timer')
    ax = sns.lineplot(x=x_label, y='r_returningResponseTimer', data=data, legend=None, ci=CI, marker=MARKER,
                      palette=palette, label='Returning response timer')

    # plt.axvline(x=84, color='k', linestyle='--', label='Saturation')

    plt.ylabel(y_label)
    plt.legend(loc=2)

    ax.set_xticks(KEY_SIZE)
    ax.set_ylim(bottom=0)

    ax.grid(True)
    fig.show()
    fig.savefig(dst)


def main(request_type='multi_get'):
    df = pd.read_csv('stats_%s.csv' % request_type)

    # threads = 8
    # plot_queue_size(set_df, '05_experiment_QueueSize_set.jpg')
    # plot_time_decomposition(set_df, '05_experiment_rtDecomp%s_set.jpg' % threads, threads)
    # threads = 64
    # plot_time_decomposition(set_df, '05_experiment_rtDecomp%s_set.jpg' % threads, threads)

    # client send
    scl = 1. / 2 ** 20  # in MBs
    df['server_recv'] = df['server_recv'] * scl
    df['server_send'] = df['server_send'] * scl
    df['mw_recv'] = df['mw_recv'] * scl
    df['mw_send'] = df['mw_send'] * scl

    df['client_send'] = df['mw_recv'] - df['server_send']
    df['client_recv'] = df['mw_send'] - df['server_recv']

    plot_client_send(df, '06_experiment_ClientSend_%s.jpg' % request_type)

    # plot_queue_size(df, '06_experiment_QueueSize_%s.jpg' % request_type)

    plot_server_to_mw(df, '06_experiment_ServerToMW_%s.jpg' % request_type)
    # plot_time_decomposition(df, '06_experiment_rtDecomp_%s.jpg' % request_type)
    # plot_cpu_idle(df, '06_experiment_CPUidle_%s.jpg' % request_type)
    # plot_system_requests(df, '06_experiment_RequestsInSystem_%s.jpg' % request_type)
    # plot_memory(set_df, '05_experiment_memory_set.jpg')


if __name__ == '__main__':
    PALETTE = sns.color_palette("hls", 4)

    MARKER = "o"
    CI = 'sd'

    KEY_SIZE = [1, 3, 6, 9]

    # NUMBER_OF_CLIENTS = 3
    # NUMBER_OF_INSTANCES = 2
    # NUMBER_OF_THREADS = 1

    # CLIENT_FACTOR = NUMBER_OF_CLIENTS * NUMBER_OF_INSTANCES * NUMBER_OF_THREADS
    main(request_type='multi_get')
    main(request_type='multi_get_sharded')
