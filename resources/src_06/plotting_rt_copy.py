import re

import pandas as pd
import matplotlib.pyplot as plt

import seaborn as sns
import numpy as np
from io import StringIO

from utils import get_client_files
from scipy.stats import skew

sns.set(style="whitegrid")


def parse_client_cdf(file):
    global TYPE

    content = list()
    with open(file) as f:
        for line in f:
            if line.startswith(TYPE):
                content.append(re.sub(' +', ' ', line))
    content = '\n'.join([x.replace(' ', ',') for x in content])
    content = 'type,time,percent\n' + content.strip()
    return pd.read_csv(StringIO(content))


def get_percentil(df, percentile):
    tmp = df['percent'].copy()
    tmp[tmp > percentile] = np.inf

    tmp = abs(tmp - percentile).values
    ind = np.argmin(tmp)
    return df.loc[ind, 'time']


def generate_client_hists(request_type, number_of_clients=2, mw_thread=8, key_size=6):
    dfs = []
    for run in RUN:
        files1, files2 = get_client_files(number_of_clients, mw_thread, request_type, run, key_size)
        for file in files1:
            dfs.append(parse_client_cdf(file))
        for file in files2:
            dfs.append(parse_client_cdf(file))

    N = len(dfs)
    df = dfs[0]
    for x in dfs[1:]:
        df['percent'] = df['percent'] + x['percent']
        df['time'] = df['time'] + x['time']

    df['percent'] = df['percent'] / N
    df['time'] = df['time'] / N
    df.dropna(inplace=True)
    return df


def generate_percentiles(request_type):
    tmp = {'request_type': [], 'key_size': [], 'percentil': [], 'time': []}

    for request_type in REQUEST_TYPES:
        tmp['request_type'] += [request_type] * (len(PERCENTILES) * len(KEY_SIZE))
        for key_size in KEY_SIZE:
            tmp['key_size'] += [key_size] * len(PERCENTILES)
            df = generate_client_hists(request_type, key_size=key_size)

            for percentile in PERCENTILES:
                tmp['percentil'].append(percentile)
                tmp['time'].append(get_percentil(df, percentile))

            print(request_type, key_size, skew(df[df['time'] < 11]['time']) * 100)
    return pd.DataFrame(tmp)


def plot_rt(df, dst, x_label='Key size', y_label='Response Time [ms]', legend='percentile', plot_col='mode'):
    data = pd.DataFrame()
    data[x_label] = df['key_size']
    data[y_label] = df['time']
    data[legend] = df['percentil']
    data[plot_col] = df['request_type'].apply(lambda x: 'non-sharded' if x == 'multi_get' else 'sharded')

    # g = sns.FacetGrid(data, col=plot_col, hue=legend, palette=PALETTE, col_order=["sharded", "non-sharded"])
    # g = (g.map(sns.stripplot, x_label, y_label, jitter=False, marker=MARKER, linewidth=1))
    # plt.legend(loc='upper right')
    sns.set(style='whitegrid', font_scale=1)

    ax = sns.catplot(x=x_label, y=y_label, hue=legend, legend_out=False, col=plot_col, kind='point', jitter=False,
                     data=data, palette=PALETTE, col_order=["sharded", "non-sharded"])
    ax.set(ylim=[0, 20.0])

    plt.savefig(dst)
    plt.show()


def main(request_type):
    # df = generate_client_hists(request_type)
    # print(df)
    df_get = generate_percentiles(request_type)
    global TYPE
    TYPE = "SET"
    df_set = generate_percentiles(request_type)

    df_get['time']=(df_set['time']+df_get['time']*df_get['key_size'])/(df_get['key_size']+1)


    plot_rt(df_get, '06_experiment_RT.jpg')


if __name__ == '__main__':
    TYPE = "GET"
    PALETTE = sns.color_palette("hls", 5)
    MARKER = "o"
    CI = 'sd'
    RUN = ['1', '2', '3']

    KEY_SIZE = [1, 3, 6, 9]
    PERCENTILES = [25, 50, 75, 90, 99]
    REQUEST_TYPES = ['multi_get', 'multi_get_sharded']
    # NUMBER_OF_CLIENTS = 3
    # NUMBER_OF_INSTANCES = 1
    # NUMBER_OF_THREADS = 2

    main('multi_get')
    # main('multi_get_sharded')
