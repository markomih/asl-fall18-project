import pandas as pd
import matplotlib.pyplot as plt

import seaborn as sns
import numpy as np

sns.set(style="whitegrid")


def plot_th(df, dst, x_label='Key size', y_label='Throughput 1000 Ops/sec', legend='percentile', TH_SCALE=1000,th_label='mw_th'):
    tmp = {
        x_label: [],
        y_label: [],
        legend: [],
    }
    for key_size in KEY_SIZE:
        mw_ths = df[df['key_size'] == key_size][th_label].values/TH_SCALE
        for percentile in [25, 50, 75, 90, 99]:
            tmp[x_label].append(key_size)
            _tmp_per = np.percentile(mw_ths, percentile)
            print(_tmp_per)
            tmp[y_label].append(_tmp_per)
            tmp[legend].append(str(percentile))

    data = pd.DataFrame(tmp)

    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)
    sns.lineplot(x=x_label, y=y_label, hue=legend, data=data, legend="full", ci=CI, marker=MARKER, palette=PALETTE)

    ax.set_xticks(KEY_SIZE)
    ax.set_ylim(ymin=0, ymax=18.0)
    ax.grid(True)
    fig.show()
    fig.savefig(dst)



def plot_rt(df, dst, x_label='Key size', y_label='Response Time [ms]', legend='percentile',
            resp_time_label='mw_rt'):
    tmp = {
        x_label: [],
        y_label: [],
        legend: [],
    }
    for key_size in KEY_SIZE:
        mw_rts = df[df['key_size'] == key_size][resp_time_label].values
        for percentile in [25, 50, 75, 90, 99]:
            tmp[x_label].append(key_size)
            _tmp_per = np.percentile(mw_rts, percentile)
            print(_tmp_per)
            tmp[y_label].append(_tmp_per)
            tmp[legend].append(str(percentile))

    data = pd.DataFrame(tmp)

    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)
    sns.lineplot(x=x_label, y=y_label, hue=legend, data=data, legend="full", ci=CI, marker=MARKER, palette=PALETTE)

    ax.set_xticks(KEY_SIZE)
    ax.set_ylim(ymin=0, ymax=10)
    ax.grid(True)
    fig.show()
    fig.savefig(dst)


def make_th_plt(request_type='set'):
    df = pd.read_csv('stats_%s.csv' % request_type)

    plot_th(df, '06_experiment_TH_%s.jpg' % request_type, th_label='mw_th')
    plot_th(df, '06_experiment_client_TH_%s.jpg' % request_type, th_label='mw_th')


def make_rt_plt(request_type):
    df = pd.read_csv('stats_%s.csv' % request_type)

    plot_rt(df, '06_experiment_RT_%s.jpg' % request_type, resp_time_label='mw_rt')
    plot_rt(df, '06_experiment_client_RT_%s.jpg' % request_type, resp_time_label='c_rt')


if __name__ == '__main__':
    PALETTE = sns.color_palette("hls", 5)
    MARKER = "o"
    CI = 'sd'

    KEY_SIZE = [1, 3, 6, 9]

    # NUMBER_OF_CLIENTS = 3
    # NUMBER_OF_INSTANCES = 1
    # NUMBER_OF_THREADS = 2

    make_rt_plt('multi_get')
    make_rt_plt('multi_get_sharded')

    make_th_plt('multi_get')
    make_th_plt('multi_get_sharded')
