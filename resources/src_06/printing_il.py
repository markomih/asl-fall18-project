from utils import get_client_th_il, get_mw_request_files

import pandas as pd
import numpy as np


def il(N, X, R, Z):
    return (N / X) / (R+Z)


def workload(request_type):
    if request_type == 'multi_get':
        return 'Non-sharded'
    return 'Sharded'


def get_mw_th_il(number_of_clients, request_type, mw_thread, key_size):
    th_list = list()
    for run in ['1', '2', '3']:
        th = 0
        file1, file2 = get_mw_request_files(number_of_clients, request_type, run, mw_thread, key_size)
        for file in [file1, file2]:
            for request_pattern in ['GET', 'SET']:
                df = pd.read_csv(file)
                df = df.loc[df['requestType'].str.contains(request_pattern)]
                th += df.shape[0]
        th_list.append(th)
    return np.mean(th_list) / 70


def print_interactive_law():
    tmp = {'Mode': [], 'Workers': [], 'Clients': []}
    for key_size in KEY_SIZE:
        tmp['key size %s' % key_size] = []

    for request_type in ['multi_get_sharded', 'multi_get']:
        df = pd.read_csv('stats_%s.csv' % request_type)
        threads = sorted(set(df['mw_thread'].values.tolist()))

        for thread in threads:
            N = CLIENTS * CLIENT_FACTOR
            tmp['Mode'].append(workload(request_type))
            tmp['Workers'].append(thread)
            tmp['Clients'].append(N)

            for key_size in KEY_SIZE:
                tmp_df = df[(df['key_size'] == key_size) & (df['mw_thread'] == thread)]
                # X = get_client_th_il(CLIENTS, thread, request_type, key_size) / 1000
                # X = tmp_df['c_th'].mean() / 1000
                X = get_mw_th_il(CLIENTS, request_type, thread, key_size)/1000
                R = tmp_df['mw_rt'].mean()
                # Rc = tmp_df['c_rt'].mean()
                # Z = abs(R - Rc)
                Z = 1.7
                print(thread, N, '\t%.2f' % il(N, X, R, Z))

                tmp['key size %s' % key_size].append(il(N, X, R, Z))

    print(pd.DataFrame(tmp).round(2).to_latex(index=False))


def main():
    print_interactive_law()


if __name__ == '__main__':
    KEY_SIZE = [1, 3, 6, 9]
    CLIENTS = 2

    NUMBER_OF_CLIENTS = 3
    NUMBER_OF_INSTANCES = 2
    NUMBER_OF_THREADS = 1

    CLIENT_FACTOR = NUMBER_OF_CLIENTS * NUMBER_OF_INSTANCES * NUMBER_OF_THREADS

    main()
