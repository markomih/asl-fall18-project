from io import StringIO
import re
import numpy as np

import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from matplotlib.text import Text

from matplotlib.axis import Axis

from utils import parse_file, get_client_files, get_mw_request_files


def parse_client_cdf(file):
    content = list()
    with open(file) as f:
        for line in f:
            if line.startswith('GET'):
                content.append(re.sub(' +', ' ', line))
    content = '\n'.join([x.replace(' ', ',') for x in content])
    content = 'type,time,percent\n' + content.strip()
    return pd.read_csv(StringIO(content))


def fill_mw_bucket(file, key_size, bucket, bucket_values, seconds=70):
    for request_pattern in ['GET', 'SET']:
        df = pd.read_csv(file)
        df = df.loc[df['requestType'].str.contains(request_pattern)]
        df['responseTimer'] = df['responseTimer'] / 1e6
        df = df.loc[df['responseTimer'] < bucket_values[-1] + BUCKET_STEP]
        for i in range(len(bucket) - 1, - 1, -1):
            indexes = df['responseTimer'] > bucket_values[i]
            bucket[i] += df['responseTimer'][indexes].count()  # / seconds
            df = df.loc[~indexes]


def fill_client_bucket(file, key_size, bucket, bucket_values):
    get_th = parse_file(file, 'get_th')  # * key_size
    set_th = parse_file(file, 'set_th')  # * key_size
    client_th=get_th+set_th
    # print("CLIENT_GET_TH=", parse_file(file, 'get_th'))

    df = parse_client_cdf(file)
    df.drop(columns=['type'], inplace=True)

    to_eval = pd.DataFrame()
    to_eval['time'] = df['time'].values
    to_eval['percent'] = np.append([0], np.diff(df['percent']))
    to_eval['th'] = to_eval['percent'] * client_th / 100

    rt_bins = to_eval.time.values.round(2)
    hists = to_eval.th.values.round(2)

    for i in range(bucket_values.size):
        count = hists[rt_bins == bucket_values[i]]
        if len(count) == 0: continue

        if len(count) != 1:
            raise Exception("len(count)=%s" % len(count))
        bucket[i] += count[0]


def plot_bucet(df, dst, x_label="Response Time [ms]", y_label='Requests per bucket'):
    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)

    data = df.groupby('bucket_values', as_index=False).aggregate('sum')
    data[x_label] = data['bucket_values']
    data[y_label] = data['bucket']
    # data['run']=df['run']
    # plt.bar(data[x_label], data[y_label])
    ax = sns.barplot(x=x_label, y=y_label, data=data, color=PALETTE[0])
    ax.set(xlabel=x_label, ylabel=y_label)
    ax.set_ylim(bottom=0, top=60000)
    ticks = list(range(int(data.bucket_values.min()), int(data.bucket_values.max()) + 1))
    ind = np.array([np.argwhere(data.bucket_values == x) for x in ticks]).flatten()

    plt.xticks(ind, ticks)
    fig.show()
    fig.savefig(dst)


def generate_client_hists(request_type, number_of_clients=2, mw_thread=8, key_size=6, seconds=70):
    tmp = {'bucket': [], 'bucket_values': []}

    # run = '1'
    bucket_values = np.arange(LIMITS[0], LIMITS[1], BUCKET_STEP).round(2)
    bucket = np.zeros(shape=bucket_values.shape)

    for run in RUN:
        files1, files2 = get_client_files(number_of_clients, mw_thread, request_type, run, key_size)
        for file in files1:
            fill_client_bucket(file, key_size, bucket, bucket_values)

        for file in files2:
            fill_client_bucket(file, key_size, bucket, bucket_values)

    # handling outliers
    bucket = bucket * seconds

    ind = int(len(bucket) / 2)
    outliers = bucket[ind:].mean()
    for i in range(ind, len(bucket)):
        bucket[i] = outliers

        ##############################
    # tmp['run'] = np.append(tmp['run'], np.ravel([run] * len(bucket)))
    tmp['bucket'] = np.append(tmp['bucket'], np.ravel(bucket))
    tmp['bucket_values'] = np.append(tmp['bucket_values'], np.ravel(bucket_values))
    print('\tbucket=', bucket.sum())

    return pd.DataFrame(tmp)


def generate_mw_hists(request_type, number_of_clients=2, mw_thread=8, key_size=6):
    tmp = {'bucket': [], 'bucket_values': []}
    bucket_values = np.arange(LIMITS[0], LIMITS[1], BUCKET_STEP).round(2)
    bucket = np.zeros(shape=bucket_values.shape)

    for run in RUN:
        file1, file2 = get_mw_request_files(number_of_clients, request_type, run, mw_thread, key_size)

        fill_mw_bucket(file1, key_size, bucket, bucket_values)
        fill_mw_bucket(file2, key_size, bucket, bucket_values)

    # tmp['run'] = np.append(tmp['run'], np.ravel([run] * len(bucket)))
    tmp['bucket'] = np.append(tmp['bucket'], np.ravel(bucket))
    tmp['bucket_values'] = np.append(tmp['bucket_values'], np.ravel(bucket_values))

    print('bucket=', bucket.sum())

    # tmp['bucket'] = tmp['bucket']
    return pd.DataFrame(tmp)


def main(key_size=6):
    request_types = ['multi_get', 'multi_get_sharded']

    for request_type in request_types:
        df1 = generate_client_hists(request_type, key_size=key_size)
        plot_bucet(df1, dst='06_experiment_client_hist_%s.jpg' % request_type)
        df2 = generate_mw_hists(request_type, key_size=key_size)
        plot_bucet(df2, dst='06_experiment_mw_hist_%s.jpg' % request_type)


if __name__ == '__main__':
    PALETTE = sns.color_palette("tab10", 4)
    MARKER = "o"
    CI = 'sd'
    RUN = ['1', '2', '3']

    KEY_SIZE = [1, 3, 6, 9]
    LIMITS = (0, 20)
    BUCKET_STEP = .1

    main()
