import pandas as pd

# from to_repeat import to_repeat
from utils import get_client_rt, get_client_th, get_mw_rt, get_mw_th, get_client_misses, get_mw_misses, \
    get_mw_queue_length, get_mw_arrival_rate, get_dstat, get_server_dstat, get_mw_used_memory, get_mw_requests_in_system


def main(request_type='set'):
    # to_exclude = to_repeat(CLIENTS, MW_THREADS)
    tmp_dict = {
        'client': [],
        'mw_thread': [],
        'run': [],
        'c_rt': [],
        'c_th': [],
        'mw_rt': [],
        'mw_th': [],
        'c_misses': [],
        'key_size': [],

        'mw_misses': [],
        'mw_queueLength': [],
        'mw_arrivalRate': [],
        'mw_usedMemory': [],
        'mw_systemRequests': [],

        'mw_recv': [],
        'mw_send': [],
        'mw_idl': [],

        'server_recv': [],
        'server_send': [],
        'server_idl': [],

        'r_internalQueueTimer': [],
        'r_processingTimer': [],
        'r_serviceTimer': [],
        'r_waitingServiceTimer': [],
        'r_returningResponseTimer': [],
    }

    for client in CLIENTS:
        for mw_thread in MW_THREADS:
            for key_size in KEY_SIZE:
                for run in RUN:
                    # tmp=(request_type, client, mw_thread, run)
                    # if tmp in to_exclude:
                    #     continue

                    print(client, mw_thread, run)
                    tmp_dict['client'].append(client)
                    tmp_dict['mw_thread'].append(mw_thread)
                    tmp_dict['run'].append(run)
                    tmp_dict['key_size'].append(key_size)

                    tmp_dict['c_rt'].append(get_client_rt(client, mw_thread, request_type, run,key_size))
                    tmp_dict['c_th'].append(get_client_th(client, mw_thread, request_type, run,key_size))
                    tmp_dict['c_misses'].append(get_client_misses(client, mw_thread, request_type, run,key_size))

                    mw_request = get_mw_rt(client, request_type, run, mw_thread,key_size,SHARDED)

                    mw_th = get_mw_th(client, request_type, run, mw_thread,key_size,SHARDED)
                    mw_misses = get_mw_misses(client, request_type, run, mw_thread,key_size,SHARDED)
                    mw_queueLength = get_mw_queue_length(client, request_type, run, mw_thread,key_size,SHARDED)
                    mw_arrivalRate = get_mw_arrival_rate(client, request_type, run, mw_thread,key_size,SHARDED)
                    mw_used_memory = get_mw_used_memory(client, request_type, run, mw_thread,key_size,SHARDED)
                    mw_systemRequests = get_mw_requests_in_system(client, request_type, run, mw_thread,key_size,SHARDED)

                    net = get_dstat(client, request_type, run, mw_thread,key_size)
                    server_net = get_server_dstat(client, request_type, run, mw_thread,key_size)

                    tmp_dict['mw_th'].append(mw_th)
                    tmp_dict['mw_misses'].append(mw_misses)
                    tmp_dict['mw_queueLength'].append(mw_queueLength)
                    tmp_dict['mw_arrivalRate'].append(mw_arrivalRate)
                    tmp_dict['mw_usedMemory'].append(mw_used_memory)
                    tmp_dict['mw_systemRequests'].append(mw_systemRequests)

                    # tmp_dict['c_misses'].append(get_client_misses(client, 'get', run))
                    # tmp_dict['mw_misses'].append(get_mw_misses(client, 'get', run, mw_thread))

                    tmp_dict['mw_recv'].append(net[0])
                    tmp_dict['mw_send'].append(net[1])
                    tmp_dict['mw_idl'].append(net[2])

                    tmp_dict['server_recv'].append(server_net[0])
                    tmp_dict['server_send'].append(server_net[1])
                    tmp_dict['server_idl'].append(server_net[2])

                    tmp_dict['mw_rt'].append(mw_request[0])
                    tmp_dict['r_internalQueueTimer'].append(mw_request[1])
                    tmp_dict['r_processingTimer'].append(mw_request[2])
                    tmp_dict['r_serviceTimer'].append(mw_request[3])
                    tmp_dict['r_waitingServiceTimer'].append(mw_request[4])
                    tmp_dict['r_returningResponseTimer'].append(mw_request[5])

                    # tmp_dict['mw_rt'].append(mw_request[0]/1000000)
                    # tmp_dict['r_internalQueueTimer'].append(mw_request[1]/1000000)
                    # tmp_dict['r_processingTimer'].append(mw_request[2]/1000000)
                    # tmp_dict['r_serviceTimer'].append(mw_request[3]/1000000)
                    # tmp_dict['r_waitingServiceTimer'].append(mw_request[4]/1000000)
                    # tmp_dict['r_returningResponseTimer'].append(mw_request[5]/1000000)
                    #
    df = pd.DataFrame.from_dict(tmp_dict)
    df.to_csv('stats_%s.csv' % request_type)
    print(df)
    print(df.describe())


if __name__ == '__main__':
    CLIENTS = [2]
    # CLIENTS= [14, 22, 64 ]

    MW_THREADS = [8]
    KEY_SIZE = [1, 3, 6, 9]
    # MW_THREADS = [ 16 ]

    RUN = ['1', '2', '3']
    # RUN=['1']
    SHARDED='true'
    main('multi_get_sharded')

    SHARDED='false'
    main('multi_get')
