import re

import pandas as pd
import matplotlib.pyplot as plt

import seaborn as sns
import numpy as np
from io import StringIO

from utils import get_client_files

sns.set(style="whitegrid")


def plot_rt(df, dst, x_label='Key size', y_label='Response Time [ms]', legend='percentile', plot_col='mode'):
    data = pd.DataFrame()
    data[x_label] = df['key_size']
    data[y_label] = df['time']
    data[legend] = df['percentil']
    data[plot_col] = df['request_type'].apply(lambda x: 'non-sharded' if x == 'multi_get' else 'sharded')

    ax = sns.catplot(x=x_label, y=y_label, hue=legend, col=plot_col, kind='point', jitter=False, data=data,
                     palette=PALETTE)

    plt.savefig(dst)
    plt.show()


def plot_th(df, dst, x_label='Key size', y_label='Throughput 1000 Ops/sec', TH_SCALE=1000):
    # df['client'] =
    data = pd.DataFrame()
    data[x_label] = df['key_size'].values
    data[y_label] = df['c_th'] / TH_SCALE
    data['mode'] = df['mode']

    # data[legend] = df['mw_thread']

    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)
    sns.set(style='whitegrid', font_scale=1)
    g = sns.FacetGrid(data, col="mode", palette=PALETTE[:1], col_order=["sharded", "non-sharded"])
    g = (g.map(sns.lineplot, x_label, y_label, ci=CI, marker=MARKER))
    g.set(ylim=(0, 18))
    g.set(xticks=KEY_SIZE)
    g.set(fontsize=12)
    # g.set(/)
    ax.set_ylim(ymin=0)

    plt.savefig(dst)
    plt.show()


def aggregate_modes():
    request_type = 'multi_get'
    df1 = pd.read_csv('stats_%s.csv' % request_type)

    request_type = 'multi_get_sharded'
    df2 = pd.read_csv('stats_%s.csv' % request_type)

    df1['mode'] = 'non-sharded'
    df2['mode'] = 'sharded'

    df = df1.append(df2, ignore_index=True)

    return df


def main():
    df = aggregate_modes()
    plot_th(df, '06_experiment_TH.jpg')


if __name__ == '__main__':
    PALETTE = sns.color_palette("hls", 5)
    MARKER = "o"
    CI = 'sd'
    RUN = ['1', '2', '3']

    KEY_SIZE = [1, 3, 6, 9]
    PERCENTILES = [25, 50, 75, 90, 99]
    REQUEST_TYPES = ['multi_get', 'multi_get_sharded']
    # NUMBER_OF_CLIENTS = 3
    # NUMBER_OF_INSTANCES = 1
    # NUMBER_OF_THREADS = 2

    main()
    # main('multi_get_sharded')
