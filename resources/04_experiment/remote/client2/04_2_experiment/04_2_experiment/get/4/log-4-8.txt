1         Threads
4         Connections per thread
70        Seconds


ALL STATS
========================================================================
Type        Ops/sec     Hits/sec   Misses/sec      Latency       KB/sec 
------------------------------------------------------------------------
Sets           0.00          ---          ---      0.00000         0.00 
Gets         469.54       469.54         0.00      8.50500      1901.91 
Waits          0.00          ---          ---      0.00000          --- 
Totals       469.54       469.54         0.00      8.50500      1901.91 


Request Latency Distribution
Type     <= msec         Percent
------------------------------------------------------------------------
---
GET       1.100         0.01
GET       1.200         0.05
GET       1.300         0.30
GET       1.400         0.72
GET       1.500         1.60
GET       1.600         2.93
GET       1.700         4.78
GET       1.800         7.12
GET       1.900        10.00
GET       2.000        13.28
GET       2.100        16.45
GET       2.200        19.63
GET       2.300        22.90
GET       2.400        25.76
GET       2.500        28.21
GET       2.600        30.47
GET       2.700        32.40
GET       2.800        34.01
GET       2.900        35.34
GET       3.000        36.52
GET       3.100        37.59
GET       3.200        38.48
GET       3.300        39.13
GET       3.400        39.75
GET       3.500        40.33
GET       3.600        40.75
GET       3.700        41.22
GET       3.800        41.62
GET       3.900        42.01
GET       4.000        42.34
GET       4.100        42.66
GET       4.200        42.89
GET       4.300        43.08
GET       4.400        43.28
GET       4.500        43.45
GET       4.600        43.60
GET       4.700        43.73
GET       4.800        43.91
GET       4.900        44.02
GET       5.000        44.14
GET       5.100        44.24
GET       5.200        44.34
GET       5.300        44.43
GET       5.400        44.48
GET       5.500        44.59
GET       5.600        44.70
GET       5.700        44.79
GET       5.800        44.84
GET       5.900        44.91
GET       6.000        44.96
GET       6.100        45.00
GET       6.200        45.08
GET       6.300        45.15
GET       6.400        45.17
GET       6.500        45.25
GET       6.600        45.31
GET       6.700        45.37
GET       6.800        45.42
GET       6.900        45.49
GET       7.000        45.51
GET       7.100        45.57
GET       7.200        45.62
GET       7.300        45.68
GET       7.400        45.72
GET       7.500        45.79
GET       7.600        45.84
GET       7.700        45.88
GET       7.800        45.93
GET       7.900        45.98
GET       8.000        46.04
GET       8.100        46.08
GET       8.200        46.15
GET       8.300        46.21
GET       8.400        46.25
GET       8.500        46.30
GET       8.600        46.34
GET       8.700        46.39
GET       8.800        46.45
GET       8.900        46.50
GET       9.000        46.55
GET       9.100        46.64
GET       9.200        46.73
GET       9.300        46.78
GET       9.400        46.86
GET       9.500        46.91
GET       9.600        46.98
GET       9.700        47.03
GET       9.800        47.09
GET       9.900        47.20
GET      10.000        47.73
GET      11.000        49.44
GET      12.000        54.29
GET      13.000        73.00
GET      14.000        92.43
GET      15.000        97.99
GET      16.000        99.01
GET      17.000        99.37
GET      18.000        99.55
GET      19.000        99.66
GET      20.000        99.70
GET      21.000        99.74
GET      22.000        99.75
GET      23.000        99.77
GET      24.000        99.80
GET      25.000        99.81
GET      26.000        99.84
GET      27.000        99.90
GET      28.000        99.94
GET      29.000        99.97
GET      30.000        99.97
GET      31.000        99.98
GET      32.000        99.99
GET     170.000       100.00
GET     180.000       100.00
---
