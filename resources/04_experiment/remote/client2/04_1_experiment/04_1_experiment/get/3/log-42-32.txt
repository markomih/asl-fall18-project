1         Threads
42        Connections per thread
70        Seconds


ALL STATS
========================================================================
Type        Ops/sec     Hits/sec   Misses/sec      Latency       KB/sec 
------------------------------------------------------------------------
Sets           0.00          ---          ---      0.00000         0.00 
Gets         489.22       489.22         0.00     84.62500      1981.64 
Waits          0.00          ---          ---      0.00000          --- 
Totals       489.22       489.22         0.00     84.62500      1981.64 


Request Latency Distribution
Type     <= msec         Percent
------------------------------------------------------------------------
---
GET      13.000         0.00
GET      14.000         0.01
GET      15.000         0.04
GET      16.000         0.07
GET      18.000         0.08
GET      22.000         0.08
GET      23.000         0.09
GET      24.000         0.09
GET      25.000         0.09
GET      26.000         0.10
GET      27.000         0.12
GET      28.000         0.12
GET      29.000         0.13
GET      30.000         0.15
GET      31.000         0.22
GET      32.000         0.26
GET      33.000         0.26
GET      34.000         0.26
GET      36.000         0.27
GET      38.000         0.27
GET      39.000         0.28
GET      40.000         0.29
GET      41.000         0.29
GET      42.000         0.30
GET      43.000         0.31
GET      45.000         0.31
GET      46.000         0.35
GET      47.000         0.44
GET      48.000         0.47
GET      49.000         0.49
GET      50.000         0.50
GET      51.000         0.50
GET      52.000         0.51
GET      53.000         0.52
GET      55.000         0.52
GET      56.000         0.54
GET      57.000         0.55
GET      58.000         0.58
GET      59.000         0.58
GET      60.000         0.61
GET      61.000         0.77
GET      62.000         2.23
GET      63.000         5.93
GET      64.000         6.95
GET      65.000         7.10
GET      66.000         7.16
GET      67.000         7.20
GET      68.000         7.25
GET      69.000         7.29
GET      70.000         7.31
GET      71.000         7.34
GET      72.000         7.40
GET      73.000         7.50
GET      74.000         7.69
GET      75.000         8.03
GET      76.000         9.00
GET      77.000        16.44
GET      78.000        46.30
GET      79.000        61.17
GET      80.000        63.23
GET      81.000        63.84
GET      82.000        64.05
GET      83.000        64.22
GET      84.000        64.31
GET      85.000        64.36
GET      86.000        64.38
GET      87.000        64.42
GET      88.000        64.51
GET      89.000        64.64
GET      90.000        64.79
GET      91.000        65.18
GET      92.000        67.09
GET      93.000        77.07
GET      94.000        88.06
GET      95.000        90.48
GET      96.000        90.84
GET      97.000        90.94
GET      98.000        91.04
GET      99.000        91.09
GET     100.000        91.18
GET     110.000        96.88
GET     120.000        97.99
GET     130.000        98.64
GET     140.000        99.39
GET     150.000        99.53
GET     160.000        99.77
GET     170.000        99.92
GET     190.000        99.97
GET     200.000        99.99
GET     220.000       100.00
---
