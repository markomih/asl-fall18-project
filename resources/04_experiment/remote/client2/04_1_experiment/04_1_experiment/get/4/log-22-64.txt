1         Threads
22        Connections per thread
70        Seconds


ALL STATS
========================================================================
Type        Ops/sec     Hits/sec   Misses/sec      Latency       KB/sec 
------------------------------------------------------------------------
Sets           0.00          ---          ---      0.00000         0.00 
Gets         474.96       474.96         0.00     46.30200      1923.87 
Waits          0.00          ---          ---      0.00000          --- 
Totals       474.96       474.96         0.00     46.30200      1923.87 


Request Latency Distribution
Type     <= msec         Percent
------------------------------------------------------------------------
---
GET       1.800         0.00
GET       2.700         0.01
GET       2.900         0.01
GET       3.000         0.02
GET       3.100         0.03
GET       5.000         0.04
GET       6.000         0.04
GET       6.100         0.04
GET       6.300         0.05
GET       6.400         0.06
GET       7.000         0.06
GET       7.200         0.07
GET       7.700         0.08
GET       7.900         0.08
GET       8.700         0.08
GET       8.900         0.09
GET       9.000         0.10
GET       9.100         0.11
GET       9.200         0.11
GET       9.300         0.12
GET       9.400         0.13
GET       9.500         0.13
GET       9.700         0.14
GET       9.800         0.14
GET       9.900         0.15
GET      10.000         0.23
GET      11.000         0.30
GET      12.000         0.47
GET      13.000         0.74
GET      14.000         2.40
GET      15.000        14.00
GET      16.000        30.56
GET      17.000        33.68
GET      18.000        34.07
GET      19.000        34.37
GET      20.000        34.51
GET      21.000        34.67
GET      22.000        34.77
GET      23.000        34.80
GET      24.000        34.82
GET      25.000        34.90
GET      26.000        35.01
GET      27.000        35.14
GET      28.000        35.41
GET      29.000        36.12
GET      30.000        40.11
GET      31.000        49.50
GET      32.000        54.62
GET      33.000        55.41
GET      34.000        55.67
GET      35.000        55.85
GET      36.000        55.94
GET      37.000        55.97
GET      38.000        56.02
GET      39.000        56.04
GET      40.000        56.07
GET      41.000        56.13
GET      42.000        56.18
GET      43.000        56.27
GET      44.000        56.41
GET      45.000        57.01
GET      46.000        60.30
GET      47.000        67.32
GET      48.000        69.95
GET      49.000        70.39
GET      50.000        70.54
GET      51.000        70.59
GET      52.000        70.59
GET      53.000        70.61
GET      54.000        70.62
GET      55.000        70.63
GET      57.000        70.64
GET      58.000        70.67
GET      59.000        70.75
GET      60.000        70.88
GET      61.000        71.56
GET      62.000        75.76
GET      63.000        80.03
GET      64.000        80.95
GET      65.000        81.11
GET      66.000        81.15
GET      67.000        81.17
GET      68.000        81.22
GET      69.000        81.24
GET      70.000        81.27
GET      71.000        81.29
GET      72.000        81.33
GET      73.000        81.36
GET      74.000        81.40
GET      75.000        81.50
GET      76.000        81.67
GET      77.000        82.58
GET      78.000        85.92
GET      79.000        87.78
GET      80.000        88.02
GET      81.000        88.06
GET      82.000        88.09
GET      83.000        88.12
GET      84.000        88.15
GET      85.000        88.15
GET      86.000        88.16
GET      87.000        88.17
GET      88.000        88.19
GET      89.000        88.21
GET      90.000        88.21
GET      91.000        88.26
GET      92.000        88.44
GET      93.000        89.56
GET      94.000        91.40
GET      95.000        92.06
GET      96.000        92.16
GET      97.000        92.20
GET      98.000        92.22
GET     100.000        92.25
GET     110.000        94.94
GET     120.000        95.57
GET     130.000        96.24
GET     140.000        97.01
GET     150.000        97.07
GET     160.000        97.87
GET     170.000        98.44
GET     180.000        98.46
GET     190.000        98.83
GET     200.000        99.11
GET     210.000        99.15
GET     220.000        99.39
GET     230.000        99.56
GET     240.000        99.59
GET     250.000        99.71
GET     260.000        99.72
GET     270.000        99.81
GET     280.000        99.88
GET     300.000        99.90
GET     310.000        99.91
GET     320.000        99.92
GET     330.000        99.92
GET     340.000        99.94
GET     360.000        99.96
GET     370.000        99.97
GET     380.000        99.97
GET     390.000        99.98
GET     410.000        99.98
GET     420.000        99.99
GET     440.000        99.99
GET     470.000       100.00
GET     480.000       100.00
---
