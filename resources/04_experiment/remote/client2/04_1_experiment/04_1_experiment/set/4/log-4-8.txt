1         Threads
4         Connections per thread
70        Seconds


ALL STATS
========================================================================
Type        Ops/sec     Hits/sec   Misses/sec      Latency       KB/sec 
------------------------------------------------------------------------
Sets        1430.73          ---          ---      2.78800      5774.46 
Gets           0.00         0.00         0.00      0.00000         0.00 
Waits          0.00          ---          ---      0.00000          --- 
Totals      1430.73         0.00         0.00      2.78800      5774.46 


Request Latency Distribution
Type     <= msec         Percent
------------------------------------------------------------------------
SET       0.810         0.00
SET       0.830         0.00
SET       0.840         0.00
SET       0.850         0.00
SET       0.860         0.01
SET       0.870         0.01
SET       0.880         0.01
SET       0.890         0.02
SET       0.900         0.02
SET       0.910         0.02
SET       0.920         0.03
SET       0.930         0.04
SET       0.940         0.05
SET       0.950         0.07
SET       0.960         0.08
SET       0.970         0.10
SET       0.980         0.12
SET       0.990         0.15
SET       1.000         0.37
SET       1.100         1.40
SET       1.200         3.50
SET       1.300         7.06
SET       1.400        11.80
SET       1.500        17.70
SET       1.600        23.93
SET       1.700        30.41
SET       1.800        36.81
SET       1.900        42.93
SET       2.000        48.55
SET       2.100        53.61
SET       2.200        58.22
SET       2.300        62.16
SET       2.400        65.68
SET       2.500        68.61
SET       2.600        71.13
SET       2.700        73.43
SET       2.800        75.24
SET       2.900        76.79
SET       3.000        78.26
SET       3.100        79.52
SET       3.200        80.59
SET       3.300        81.56
SET       3.400        82.34
SET       3.500        83.03
SET       3.600        83.75
SET       3.700        84.31
SET       3.800        84.81
SET       3.900        85.27
SET       4.000        85.66
SET       4.100        86.05
SET       4.200        86.41
SET       4.300        86.73
SET       4.400        86.99
SET       4.500        87.26
SET       4.600        87.54
SET       4.700        87.79
SET       4.800        88.01
SET       4.900        88.24
SET       5.000        88.46
SET       5.100        88.69
SET       5.200        88.89
SET       5.300        89.09
SET       5.400        89.26
SET       5.500        89.40
SET       5.600        89.55
SET       5.700        89.71
SET       5.800        89.85
SET       5.900        89.99
SET       6.000        90.15
SET       6.100        90.31
SET       6.200        90.48
SET       6.300        90.68
SET       6.400        90.90
SET       6.500        91.18
SET       6.600        91.46
SET       6.700        91.81
SET       6.800        92.21
SET       6.900        92.65
SET       7.000        93.13
SET       7.100        93.57
SET       7.200        94.10
SET       7.300        94.58
SET       7.400        95.07
SET       7.500        95.50
SET       7.600        95.93
SET       7.700        96.31
SET       7.800        96.68
SET       7.900        97.00
SET       8.000        97.28
SET       8.100        97.56
SET       8.200        97.77
SET       8.300        97.95
SET       8.400        98.12
SET       8.500        98.29
SET       8.600        98.43
SET       8.700        98.55
SET       8.800        98.64
SET       8.900        98.73
SET       9.000        98.80
SET       9.100        98.88
SET       9.200        98.97
SET       9.300        99.04
SET       9.400        99.09
SET       9.500        99.15
SET       9.600        99.21
SET       9.700        99.25
SET       9.800        99.30
SET       9.900        99.34
SET      10.000        99.53
SET      11.000        99.73
SET      12.000        99.81
SET      13.000        99.86
SET      14.000        99.89
SET      15.000        99.90
SET      16.000        99.92
SET      17.000        99.94
SET      18.000        99.95
SET      19.000        99.96
SET      20.000        99.97
SET      21.000        99.97
SET      22.000        99.97
SET      24.000        99.97
SET      28.000        99.98
SET      29.000        99.98
SET      30.000        99.98
SET      31.000        99.98
SET      52.000        99.98
SET      53.000        99.99
SET      55.000        99.99
SET      56.000        99.99
SET      58.000        99.99
SET      73.000        99.99
SET     100.000        99.99
SET     120.000        99.99
SET     140.000        99.99
SET     280.000       100.00
SET     290.000       100.00
SET     320.000       100.00
SET     330.000       100.00
SET     340.000       100.00
---
---
