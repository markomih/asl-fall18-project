1         Threads
22        Connections per thread
70        Seconds


ALL STATS
========================================================================
Type        Ops/sec     Hits/sec   Misses/sec      Latency       KB/sec 
------------------------------------------------------------------------
Sets        1871.21          ---          ---     11.74900      7552.26 
Gets           0.00         0.00         0.00      0.00000         0.00 
Waits          0.00          ---          ---      0.00000          --- 
Totals      1871.21         0.00         0.00     11.74900      7552.26 


Request Latency Distribution
Type     <= msec         Percent
------------------------------------------------------------------------
SET       2.500         0.00
SET       2.600         0.00
SET       2.700         0.01
SET       2.800         0.01
SET       2.900         0.01
SET       3.000         0.01
SET       3.100         0.01
SET       3.200         0.02
SET       3.300         0.02
SET       3.400         0.03
SET       3.500         0.04
SET       3.600         0.05
SET       3.700         0.06
SET       3.800         0.07
SET       3.900         0.10
SET       4.000         0.13
SET       4.100         0.17
SET       4.200         0.22
SET       4.300         0.27
SET       4.400         0.34
SET       4.500         0.43
SET       4.600         0.53
SET       4.700         0.64
SET       4.800         0.78
SET       4.900         0.91
SET       5.000         1.08
SET       5.100         1.28
SET       5.200         1.49
SET       5.300         1.75
SET       5.400         2.04
SET       5.500         2.36
SET       5.600         2.70
SET       5.700         3.02
SET       5.800         3.40
SET       5.900         3.82
SET       6.000         4.24
SET       6.100         4.69
SET       6.200         5.12
SET       6.300         5.57
SET       6.400         6.02
SET       6.500         6.50
SET       6.600         6.98
SET       6.700         7.46
SET       6.800         7.94
SET       6.900         8.44
SET       7.000         8.95
SET       7.100         9.42
SET       7.200         9.87
SET       7.300        10.33
SET       7.400        10.75
SET       7.500        11.22
SET       7.600        11.66
SET       7.700        12.09
SET       7.800        12.52
SET       7.900        12.99
SET       8.000        13.40
SET       8.100        13.81
SET       8.200        14.20
SET       8.300        14.59
SET       8.400        14.93
SET       8.500        15.31
SET       8.600        15.64
SET       8.700        15.98
SET       8.800        16.31
SET       8.900        16.65
SET       9.000        17.00
SET       9.100        17.37
SET       9.200        17.73
SET       9.300        18.10
SET       9.400        18.49
SET       9.500        18.88
SET       9.600        19.29
SET       9.700        19.73
SET       9.800        20.21
SET       9.900        20.79
SET      10.000        24.93
SET      11.000        40.27
SET      12.000        61.66
SET      13.000        79.09
SET      14.000        89.02
SET      15.000        93.48
SET      16.000        95.49
SET      17.000        96.67
SET      18.000        97.54
SET      19.000        98.32
SET      20.000        98.84
SET      21.000        99.18
SET      22.000        99.46
SET      23.000        99.58
SET      24.000        99.65
SET      25.000        99.71
SET      26.000        99.76
SET      27.000        99.79
SET      28.000        99.84
SET      29.000        99.87
SET      30.000        99.90
SET      31.000        99.92
SET      32.000        99.94
SET      33.000        99.95
SET      34.000        99.95
SET      35.000        99.96
SET      36.000        99.97
SET      37.000        99.98
SET      38.000        99.98
SET      39.000        99.99
SET      41.000        99.99
SET      42.000        99.99
SET      43.000        99.99
SET      45.000        99.99
SET      49.000        99.99
SET      96.000       100.00
SET      97.000       100.00
SET     120.000       100.00
---
---
