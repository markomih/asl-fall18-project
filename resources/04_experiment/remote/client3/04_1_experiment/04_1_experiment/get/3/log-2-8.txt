1         Threads
2         Connections per thread
70        Seconds


ALL STATS
========================================================================
Type        Ops/sec     Hits/sec   Misses/sec      Latency       KB/sec 
------------------------------------------------------------------------
Sets           0.00          ---          ---      0.00000         0.00 
Gets         502.72       502.72         0.00      3.96800      2036.31 
Waits          0.00          ---          ---      0.00000          --- 
Totals       502.72       502.72         0.00      3.96800      2036.31 


Request Latency Distribution
Type     <= msec         Percent
------------------------------------------------------------------------
---
GET       0.900         0.00
GET       0.910         0.01
GET       0.920         0.01
GET       0.930         0.01
GET       0.940         0.02
GET       0.970         0.02
GET       0.980         0.03
GET       0.990         0.03
GET       1.000         0.06
GET       1.100         0.26
GET       1.200         0.97
GET       1.300         2.59
GET       1.400         5.46
GET       1.500        10.03
GET       1.600        16.83
GET       1.700        24.50
GET       1.800        32.81
GET       1.900        40.25
GET       2.000        46.48
GET       2.100        51.73
GET       2.200        55.86
GET       2.300        59.08
GET       2.400        61.58
GET       2.500        63.49
GET       2.600        65.02
GET       2.700        66.43
GET       2.800        67.48
GET       2.900        68.37
GET       3.000        69.03
GET       3.100        69.75
GET       3.200        70.38
GET       3.300        70.88
GET       3.400        71.27
GET       3.500        71.70
GET       3.600        72.10
GET       3.700        72.42
GET       3.800        72.60
GET       3.900        72.77
GET       4.000        73.01
GET       4.100        73.20
GET       4.200        73.36
GET       4.300        73.49
GET       4.400        73.66
GET       4.500        73.81
GET       4.600        73.92
GET       4.700        74.03
GET       4.800        74.17
GET       4.900        74.28
GET       5.000        74.35
GET       5.100        74.45
GET       5.200        74.55
GET       5.300        74.62
GET       5.400        74.74
GET       5.500        74.82
GET       5.600        74.91
GET       5.700        75.00
GET       5.800        75.09
GET       5.900        75.19
GET       6.000        75.24
GET       6.100        75.33
GET       6.200        75.41
GET       6.300        75.51
GET       6.400        75.60
GET       6.500        75.73
GET       6.600        75.82
GET       6.700        75.94
GET       6.800        76.08
GET       6.900        76.21
GET       7.000        76.36
GET       7.100        76.55
GET       7.200        76.73
GET       7.300        76.88
GET       7.400        77.00
GET       7.500        77.19
GET       7.600        77.39
GET       7.700        77.63
GET       7.800        77.89
GET       7.900        78.15
GET       8.000        78.40
GET       8.100        78.67
GET       8.200        79.01
GET       8.300        79.36
GET       8.400        79.79
GET       8.500        80.21
GET       8.600        80.65
GET       8.700        81.06
GET       8.800        81.65
GET       8.900        82.23
GET       9.000        82.78
GET       9.100        83.31
GET       9.200        83.99
GET       9.300        84.75
GET       9.400        85.53
GET       9.500        86.38
GET       9.600        87.24
GET       9.700        88.20
GET       9.800        89.30
GET       9.900        90.18
GET      10.000        95.18
GET      11.000        98.87
GET      12.000        99.57
GET      13.000        99.68
GET      14.000        99.77
GET      15.000        99.79
GET      16.000        99.80
GET      17.000        99.81
GET      18.000        99.83
GET      19.000        99.85
GET      20.000        99.87
GET      21.000        99.88
GET      22.000        99.90
GET      23.000        99.94
GET      24.000        99.97
GET      25.000        99.98
GET      26.000        99.99
GET      28.000        99.99
GET     200.000       100.00
---
