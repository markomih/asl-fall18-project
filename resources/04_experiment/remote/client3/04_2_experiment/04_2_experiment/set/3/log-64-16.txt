1         Threads
64        Connections per thread
70        Seconds


ALL STATS
========================================================================
Type        Ops/sec     Hits/sec   Misses/sec      Latency       KB/sec 
------------------------------------------------------------------------
Sets        1777.34          ---          ---     36.00300      7173.38 
Gets           0.00         0.00         0.00      0.00000         0.00 
Waits          0.00          ---          ---      0.00000          --- 
Totals      1777.34         0.00         0.00     36.00300      7173.38 


Request Latency Distribution
Type     <= msec         Percent
------------------------------------------------------------------------
SET       7.300         0.00
SET       7.700         0.00
SET       8.100         0.00
SET       8.200         0.00
SET       8.500         0.00
SET       8.900         0.01
SET       9.000         0.01
SET       9.200         0.01
SET       9.500         0.01
SET       9.600         0.01
SET       9.900         0.01
SET      10.000         0.01
SET      11.000         0.02
SET      12.000         0.03
SET      13.000         0.03
SET      14.000         0.04
SET      15.000         0.05
SET      16.000         0.06
SET      17.000         0.07
SET      18.000         0.08
SET      19.000         0.10
SET      20.000         0.13
SET      21.000         0.17
SET      22.000         0.24
SET      23.000         0.35
SET      24.000         0.58
SET      25.000         1.01
SET      26.000         1.82
SET      27.000         2.97
SET      28.000         4.62
SET      29.000         7.17
SET      30.000        10.81
SET      31.000        15.71
SET      32.000        21.83
SET      33.000        28.56
SET      34.000        35.89
SET      35.000        44.21
SET      36.000        54.17
SET      37.000        64.49
SET      38.000        74.51
SET      39.000        82.55
SET      40.000        88.32
SET      41.000        91.86
SET      42.000        94.08
SET      43.000        95.48
SET      44.000        96.52
SET      45.000        97.20
SET      46.000        97.73
SET      47.000        98.13
SET      48.000        98.44
SET      49.000        98.67
SET      50.000        98.90
SET      51.000        99.03
SET      52.000        99.16
SET      53.000        99.30
SET      54.000        99.42
SET      55.000        99.55
SET      56.000        99.64
SET      57.000        99.71
SET      58.000        99.77
SET      59.000        99.82
SET      60.000        99.84
SET      61.000        99.87
SET      62.000        99.88
SET      63.000        99.90
SET      64.000        99.93
SET      65.000        99.95
SET      66.000        99.96
SET      67.000        99.96
SET      69.000        99.96
SET      76.000        99.96
SET      80.000        99.97
SET      84.000        99.97
SET      94.000        99.97
SET      96.000        99.97
SET     100.000        99.97
SET     110.000        99.98
SET     120.000        99.98
SET     140.000        99.98
SET     160.000        99.98
SET     170.000        99.98
SET     180.000        99.98
SET     190.000        99.99
SET     200.000        99.99
SET     210.000       100.00
---
---
