import numpy as np

from scipy import stats
from plotting_th import get_th_all
from plotting_rt import get_rt_all
import matplotlib.pyplot as plt

def normal_dist_test(data):
    # pts = 1000
    # np.random.seed(28041990)
    # a = np.random.normal(0, 1, size=pts)
    # b = np.random.normal(2, 1, size=pts)
    # x = np.concatenate((a, b))
    k2, p = stats.normaltest(data)
    alpha = 1e-3
    print("p = {:g}".format(p))

    if p < alpha:  # null hypothesis: x comes from a normal distribution
        print("The null hypothesis can be rejected")
    else:
        print("The null hypothesis cannot be rejected")


def main():
    all_RTs_full = get_rt_all('get', clients)
    # all_THs_full = get_th_all('set', set_clients)

    for all_RTs in all_RTs_full:
        t = [sum(x) for x in all_RTs]
        # t=sum(all_RTs, [])
        plt.hist(t)
        plt.show()
        # normal_dist_test(t)

    # all_THs_full = get_th_all(request_type, clients)


if __name__ == '__main__':
    # get_clients = [1, 2, 4, 6, 8, 10, 14, 22, 42]
    # set_clients = [1, 2, 4, 6, 8, 10, 14, 22, 32, 42]
    clients=[1, 2, 4, 6, 8, 10, 14, 22, 32, 42, 64]
    main()