import os
import re
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

from io import StringIO


def extract_rt(number_of_clients, request_type='set'):
    dir_path = os.path.join('..', '01_experiment')
    MRTs = []
    MRTs_full = []
    for root, _, files in os.walk(dir_path):
        for file in files:
            if re.match(r'log-%s.client-%s-[0-9].[0-9].[0-9]+.csv' % (request_type, number_of_clients), file):
                content = []
                with open(os.path.join(root, file), 'r') as f:
                    next(f)  # skip first line
                    for line in f:
                        if 'Full-Test GET Latency' in line:
                            break
                        content.append(line)
                content = '\n'.join(content)
                df = pd.read_csv(StringIO(content))
                # number_of_requests = df[request_type.upper()+' Requests'].sum()
                # number_of_seconds = df['Second'].max()-1
                # print(file, '\tRT = ', number_of_requests/number_of_seconds)
                MRT = df['SET Average Latency'].mean()
                MRTs_full += df['SET Average Latency'].values.tolist()
                print(file, '\tMRT = %sms' % MRT)
                MRTs.append(MRT)

    return MRTs, MRTs_full


def plot_image(all_MRTs_full, clients):
    y_points, x_points = [], []
    for x, client in zip(all_MRTs_full, clients):
        y_points += np.mean(x),np.min(x),np.max(x)
        x_points += [client]*3

    df = pd.DataFrame()
    df['Response Time [ms]'] = y_points
    df['Virtual Clients'] = x_points

    print('PRINTING')
    sns.lineplot(x="Virtual Clients", y="Response Time [ms]", data=df)
    plt.grid(True)
    plt.show()
    print('PRINTING')


def main():
    clients = list(range(2, 32 + 1, 2))
    all_MRTs_full = list()

    for client in clients:
        MRTs, MRTs_full = extract_rt(client, 'set')
        MRTs_full = np.array(MRTs_full) * 1000
        all_MRTs_full.append(MRTs_full)

        print('final MRT is %s ms' % np.mean(MRTs_full))

    plot_image(all_MRTs_full, clients)
    # sns.boxplot(x="day", y="total_bill", data=MRTs_full)
    # plt.boxplot(all_MRTs_full, positions=clients, notch=True, showbox=True, autorange=True, showfliers=False)
    # plt.xlabel(clients)
    # plt.show()


if __name__ == '__main__':
    main()
