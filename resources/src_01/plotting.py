import os
import re
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

from io import StringIO


def reject_outliers(sr, iq_range=0.95):  # keep 60% data
    tmp = pd.Series(sr)

    pcnt = (1 - iq_range) / 2
    qlow, median, qhigh = tmp.dropna().quantile([pcnt, 0.50, 1 - pcnt])
    iqr = qhigh - qlow

    tmp = tmp[(tmp - median).abs() <= iqr]

    return tmp.values


def get_files(number_of_clients, request_type):
    tmp = [
        os.path.join('..', '01_experiment', 'run', 'client1', request_type),
        os.path.join('..', '01_experiment', 'run', 'client2', request_type),
        os.path.join('..', '01_experiment', 'run', 'client3', request_type)
    ]
    dir_paths = []
    for t in tmp:
        dir_paths.append(os.path.join(t, '1'))
        dir_paths.append(os.path.join(t, '2'))
        dir_paths.append(os.path.join(t, '3'))

    file_paths = []
    for dir_path in dir_paths:
        for root, _, files in os.walk(dir_path):
            for file in files:
                if re.match(r'log.client-%s-[0-9].[0-9].[0-9]+.csv' % number_of_clients, file):
                    file_paths.append(os.path.join(root, file))

    return file_paths


def get_RT(number_of_clients, request_type='set'):
    files = get_files(number_of_clients, request_type)
    MRTs_full = []

    for i, file in enumerate(files):
        content = []
        with open(file, 'r') as f:
            next(f)  # skip first line
            for line in f:
                if 'Full-Test GET Latency' in line:
                    break
                content.append(line)
        content = '\n'.join(content)
        df = pd.read_csv(StringIO(content))
        MRTs_full += df['%s Average Latency' % request_type.upper()].values.tolist()

        if i % 10 == 0:
            print("%s\tPROCESSED\t%s/%s" % (number_of_clients, i, len(files)))

    return MRTs_full


def plot_image(all_MRTs_full, clients, file_path):
    clients=np.array(clients)*NUMBER_OF_CLIENTS*NUMBER_OF_INSTANCES*NUMBER_OF_THREADS

    y_points, x_points,y_points_mean = [], [], []
    for x, client in zip(all_MRTs_full, clients):
        y_points += x.tolist()
        x_points += [client] * x.size
        y_points_mean.append(np.mean(x))

    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)

    df = pd.DataFrame()
    df[Y_LABEL] = y_points
    df[X_LABEL] = x_points
    sns.lineplot(x=X_LABEL, y=Y_LABEL, data=df, ci="sd", markers=True)

    dfp = pd.DataFrame()
    dfp[X_LABEL] = clients
    dfp[Y_LABEL] = y_points_mean
    sns.scatterplot(x=X_LABEL, y=Y_LABEL, data=dfp)

    x_thicks = [x for x in sorted(plt.xticks()[0].tolist()+[min(clients)]) if x >= min(clients)]
    ax.set_xticks(x_thicks)
    ax.set_ylim(ymin=0)

    ax.grid(which='both')
    fig.show()
    fig.savefig(file_path)


def generate_plot(request_type, file_path):
    all_MRTs_full = list()

    for client in clients:
        MRTs_full = get_RT(client, request_type)
        MRTs_full = reject_outliers(np.array(MRTs_full) * 1000)
        all_MRTs_full.append(MRTs_full)

        print('final MRT is %s ms' % np.mean(MRTs_full))

    plot_image(all_MRTs_full, clients, file_path)


def main():
    generate_plot('set', '_01_experiment_RT_set.jpg')
    generate_plot('get', '_01_experiment_RT_get.jpg')


if __name__ == '__main__':
    NUMBER_OF_CLIENTS=3
    NUMBER_OF_THREADS=2
    NUMBER_OF_INSTANCES=1
    clients = [2, 4, 6, 8, 10, 14, 22, 32, 42]

    Y_LABEL = 'Response Time [ms]'
    X_LABEL = 'Number of clients'
    main()
