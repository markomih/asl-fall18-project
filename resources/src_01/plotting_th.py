import os
import re
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns


def get_files(number_of_clients, request_type, run='1'):
    tmp = [
        os.path.join('..', '01_experiment', 'run', 'client1', request_type),
        os.path.join('..', '01_experiment', 'run', 'client2', request_type),
        os.path.join('..', '01_experiment', 'run', 'client3', request_type)
    ]
    dir_paths = []
    for t in tmp:
        dir_paths.append(os.path.join(t, run))

    file_paths = []
    for dir_path in dir_paths:
        for root, _, files in os.walk(dir_path):
            for file in files:
                if re.match(r'log-%s.txt' % number_of_clients, file):
                    # tmp = os.path.join(root, file)
                    # shutil.copy(tmp, os.path.join('/home/maki/projects/asl-fall18-project/asl-scripts/01_experiment/run/tmp',
                    #                               "%s_%s_%s" %(run, request_type, file)))
                    file_paths.append(os.path.join(root, file))

    return file_paths


def parse_file(file_path):
    with open(file_path) as f:
        for line in f:
            if line.startswith('Totals'):
                data = [x for x in line.split(" ") if x != ""][1]
                return float(data)


def get_TH(number_of_clients, request_type='set'):
    th_list = []
    for run in ['1', '2', '3']:
        files = get_files(number_of_clients, request_type, run)
        data = [parse_file(file) for file in files]
        th_list.append(np.sum(data))
    return th_list


def plot_image(all_THs_full, clients, file_path, request_type):
    clients = np.array(clients) * NUMBER_OF_CLIENTS * NUMBER_OF_INSTANCES * NUMBER_OF_THREADS

    y_points, x_points, y_means, y_points_mean = [], [], [], []
    for x, client in zip(all_THs_full, clients):
        y_points += x.tolist()
        x_points += [client] * x.size
        y_points_mean.append(np.mean(x))

    y_points = np.array(y_points) / 1000
    y_points_mean = np.array(y_points_mean) / 1000

    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)

    df = pd.DataFrame()
    df[Y_LABEL] = y_points
    df[X_LABEL] = x_points
    sns.lineplot(x=X_LABEL, y=Y_LABEL, data=df, markers=True, ci="sd", markersize=12)

    dfp = pd.DataFrame()
    dfp[X_LABEL] = clients
    dfp[Y_LABEL] = y_points_mean
    sns.scatterplot(x=X_LABEL, y=Y_LABEL, data=dfp)

    x_thicks = [x for x in sorted(plt.xticks()[0].tolist() + [min(clients)]) if x >= min(clients)]
    # ax.set_xticks(clients.tolist())
    ax.set_xticks(x_thicks)
    # ax.set_yticks(ymin=0)

    ax.set_ylim(ymin=0, ymax=18.0)
    ax.set_xlim(left=0, right=clients.max() + 1)

    if request_type == 'get':
        plt.axvline(x=6, color='g', linestyle='--', label='saturation')
    else:
        plt.axvline(x=48, color='g', linestyle='--', label='saturation')
        plt.axvline(x=192, color='r', linestyle='--', label='over saturation')
        # plt.axhline(y=v_line, color='r', linestyle='--', label='saturation')

    plt.legend()

    ax.grid(which='both')
    fig.show()
    fig.savefig(file_path)


def get_th_all(request_type, clients):
    all_THs_full = list()
    for client in clients:
        THs_full = get_TH(client, request_type)
        # THs_full = reject_outliers(np.array(THs_full) * 1000)
        all_THs_full.append(np.array(THs_full))
        print('final MRT is %s ms' % np.mean(THs_full))
    return all_THs_full


def generate_plot(request_type, file_path, clients):
    all_THs_full = get_th_all(request_type, clients)

    plot_image(all_THs_full, clients, file_path, request_type)


if __name__ == '__main__':
    NUMBER_OF_CLIENTS = 3
    NUMBER_OF_THREADS = 2
    NUMBER_OF_INSTANCES = 1
    # clients = [ 1, 2, 4, 6, 8, 10, 14, 22, 42 ]
    # clients = [2, 4, 6, 8, 10, 14, 22, 32, 42]

    Y_LABEL = "Throughput 1000 Ops/sec"
    X_LABEL = 'Number of clients'

    clients=[1, 2, 4, 6, 8, 10, 14, 22, 32, 42, 64]
    # clients = [1, 2, 4, 6, 8, 10, 14, 22, 32, 42]
    generate_plot('set', '01_experiment_TH_set.jpg', clients)
    # clients = [1, 2, 4, 6, 8, 10, 14, 22, 42]
    generate_plot('get', '01_experiment_TH_get.jpg', clients)
