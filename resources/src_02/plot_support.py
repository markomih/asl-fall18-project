import os
import re
from io import StringIO

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

from plotting_th import get_th_all
from plotting_rt import get_rt_all


def plot_image_rt(all_MRTs_full, clients, file_path):
    clients = np.array(clients) * NUMBER_OF_CLIENTS * NUMBER_OF_INSTANCES * NUMBER_OF_THREADS
    data = {X_LABEL: clients, Y_LABEL_RT: all_MRTs_full}

    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)

    df = pd.DataFrame(data)
    sns.lineplot(x=X_LABEL, y=Y_LABEL_RT, data=df)
    sns.scatterplot(x=clients, y=all_MRTs_full)

    # x_thicks = [x for x in sorted(plt.xticks()[0].tolist() + [min(clients)]) if x >= min(clients)]
    x_thicks = [x for x in sorted(plt.xticks()[0].tolist() + [min(clients)]) if x >= min(clients)]
    ax.set_xticks(x_thicks)
    ax.set_ylim(bottom=0)
    ax.set_xlim(left=clients.min(), right=clients.max())
    ax.grid(which='both')
    fig.show()
    fig.savefig(file_path)


def plot_image_th(all_THs_full, clients, file_path):
    clients = np.array(clients) * NUMBER_OF_CLIENTS * NUMBER_OF_INSTANCES * NUMBER_OF_THREADS
    data = {X_LABEL: clients, Y_LABEL_TH: all_THs_full}

    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)

    df = pd.DataFrame(data)
    sns.lineplot(x=X_LABEL, y=Y_LABEL_TH, data=df)
    sns.scatterplot(x=clients, y=all_THs_full)

    # x_thicks = [x for x in sorted(plt.xticks()[0].tolist() + [min(clients)]) if x >= min(clients)]
    x_thicks = [x for x in sorted(plt.xticks()[0].tolist() + [min(clients)]) if x >= min(clients)]
    ax.set_xticks(x_thicks)
    ax.set_ylim(bottom=0)
    ax.set_xlim(left=clients.min(), right=clients.max())
    ax.grid(which='both')
    fig.show()
    fig.savefig(file_path)


# def interactive_law_rt(request_type, clients, dst_path):
#     all_THs_full = get_th_all(request_type, clients)
#     # clients = np.array(clients) * NUMBER_OF_CLIENTS * NUMBER_OF_INSTANCES * NUMBER_OF_THREADS
#     # clients = clients.tolist()
#     all_THs = [x.sum() for x in all_THs_full]
#     all_RTs = [(client * NUMBER_OF_INSTANCES * NUMBER_OF_THREADS) / TH
#                for client, TH in zip(clients, all_THs)]
#     all_RTs = [x * 1000 for x in all_RTs]
#     plot_image_rt(all_RTs, clients, dst_path)
#
#
# def interactive_law_th(request_type, clients, dst_path):
#     all_RTs_full = get_rt_all(request_type, clients)
#     all_RTs = [np.mean(x) * 1000. for x in all_RTs_full]
#     all_THs = [client / RT for client, RT in zip(clients, all_RTs)]
#     plot_image_th(all_THs, clients, dst_path)

def print_interactive_law(request_type, clients):
    all_THs_full = get_th_all(request_type, clients)
    all_THs = [x.mean() / 1000 for x in all_THs_full]

    all_RTs_full = get_rt_all(request_type, clients)
    all_RTs = [np.mean(x) for x in all_RTs_full]

    clients = np.array(clients) * NUMBER_OF_CLIENTS * NUMBER_OF_INSTANCES * NUMBER_OF_THREADS
    alpha = [(N / X / R) for N, X, R in zip(clients, all_THs, all_RTs)]

    table = '\n'.join([
        '&\t'.join(['clients'] + [str(c) for c in clients.tolist()]) + r'\\',
        '&\t'.join([r'TH [1000 ops/s]'] + ['%.2f' % x for x in all_THs]) + r'\\',
        '&\t'.join(['RT [ms]'] + ['%.2f' % r for r in all_RTs]) + r'\\',
        '&\t'.join([r'$\frac{celint/TH}{RT}$'] + ['%.2f' % a for a in alpha]) + r'\\',
    ])
    print(table)


def get_dstat(number_of_clients, request_type, run):
    dfs = []
    for server in ['server1', 'server2']:
        file = os.path.join('..', '02_experiment', 'run', server,
                            '%s_%s_%s_%s.csv' % (server, request_type, run, number_of_clients))

        content = []
        with open(file, 'r') as f:
            for i, line in enumerate(f):
                if line.startswith(r'"usr"'):
                    content = []
                content.append(line)
        content = '\n'.join(content)
        df = pd.read_csv(StringIO(content))
        dfs.append(df.iloc[5:-5])

    df = pd.concat(dfs)
    return df

    # return df['recv'].sum() / 30, df['send'].sum() / 30


def generate_server_summary(request_type, clients):
    data = {
        'clients': [],
        'send': [],
        'recv': [],
        'idle': [],
    }

    for client in clients:
        data['clients'].append(client)

        snd, rcv, idle = [], [], []
        for run in ['1', '2', '3']:
            df = get_dstat(client, request_type, run)
            snd.append(df['send'].mean())
            rcv.append(df['recv'].mean())
            idle.append(df['idl'].mean())
        data['send'].append(np.mean(snd))
        data['recv'].append(np.mean(rcv))
        data['idle'].append(np.mean(idle))
    df = pd.DataFrame(data)
    df.to_csv('%s_dstat.csv' % request_type, index=False)


def generate_server_summary_full(request_type, clients):
    data = {
        'clients': [],
        'send': [],
        'recv': [],
        'idle': [],
        'run': [],
    }

    for client in clients:
        for run in ['1', '2', '3']:
            df = get_dstat(client, request_type, run)
            data['send'].append(df['send'].mean())
            data['recv'].append(df['recv'].mean())
            data['idle'].append(df['idl'].mean())
            data['clients'].append(client)
            data['run'].append(run)
    df = pd.DataFrame(data)
    df.to_csv('%s_dstat.csv' % request_type, index=False)


def plot_net(request_type, file_path, src_label, x_label, y_label, scaling=0.000001):
    df = pd.read_csv('%s_dstat.csv' % request_type)
    df[y_label] = df[src_label] * scaling
    df[x_label] = df['clients'] * NUMBER_OF_CLIENTS * NUMBER_OF_INSTANCES * NUMBER_OF_THREADS

    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)

    sns.lineplot(x=x_label, y=y_label, data=df)
    x_points = list(sorted(set(df[x_label].values.tolist())))
    y_points = df.groupby('clients').mean()[y_label]
    sns.scatterplot(x=x_points, y=y_points)

    x_thicks = [x for x in sorted(plt.xticks()[0].tolist() + [df[x_label].min()]) if x >= df[x_label].min()]
    ax.set_xticks(x_thicks)
    ax.set_ylim(bottom=0, top=13)
    if src_label == 'idle':
        ax.set_ylim(top=100)

    ax.set_xlim(left=0, right=df[x_label].max() + 1)
    ax.grid(which='both')
    if src_label == 'recv':
        plt.hlines(y=12.5, xmin=0, xmax=df[x_label].max() + 1, color='g', linestyle='--', label='bandwidth')
    elif src_label == 'send':
        plt.hlines(y=12.5, xmin=0, xmax=df[x_label].max() + 1, color='g', linestyle='--', label='bandwidth')
    fig.show()
    fig.savefig(file_path)


if __name__ == '__main__':
    NUMBER_OF_CLIENTS = 1
    NUMBER_OF_THREADS = 1
    NUMBER_OF_INSTANCES = 2

    Y_LABEL_RT = 'Response Time [ms]'
    Y_LABEL_TH = "Throughput 1000 Ops/sec"
    Y_LABEL_IDL = "Idle CPU percentage"
    X_LABEL = 'Number of clients'

    SND_Y_LABEL = 'MB/s sent to clients'
    RCV_Y_LABEL = 'MB/s received from clients'

    clients = [1, 2, 4, 6, 8, 10, 14, 22, 32]

    # print_interactive_law('set', clients)
    # print_interactive_law('get', clients)
    # interactive_law_rt('set', clients, '01_experiment_IL_RT_set.jpg')
    # interactive_law_rt('get', clients, '01_experiment_IL_RT_get.jpg')

    # interactive_law_th('set', clients, '01_experiment_IL_TH_set.jpg')
    # interactive_law_th('get', clients, '01_experiment_IL_TH_get.jpg')
    #
    #
    generate_server_summary_full('get', clients)
    generate_server_summary_full('set', clients)
    #
    scl = 1. / 2 ** 20
    #
    plot_net('get', '02_experiment_SND_get.jpg', 'send', X_LABEL, SND_Y_LABEL, scl)
    # plot_net('get', '02_experiment_RCV_get.jpg', 'recv', X_LABEL, RCV_Y_LABEL, scl)
    #
    # plot_net('set', '02_experiment_SND_set.jpg', 'send', X_LABEL, SND_Y_LABEL, scl)
    plot_net('set', '02_experiment_RCV_set.jpg', 'recv', X_LABEL, RCV_Y_LABEL, scl)
    #
    # plot_net('get', '02_experiment_IDL_get.jpg', 'idle', X_LABEL, Y_LABEL_IDL, 1)
    # plot_net('set', '02_experiment_IDL_set.jpg', 'idle', X_LABEL, Y_LABEL_IDL, 1)
