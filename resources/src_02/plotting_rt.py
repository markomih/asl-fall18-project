import os
import re
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

from io import StringIO


# def reject_outliers(sr, iq_range=0.95):  # keep 60% data
#     tmp = pd.Series(sr)
#
#     pcnt = (1 - iq_range) / 2
#     qlow, median, qhigh = tmp.dropna().quantile([pcnt, 0.50, 1 - pcnt])
#     iqr = qhigh - qlow
#
#     tmp = tmp[(tmp - median).abs() <= iqr]
#
#     return tmp.values

def parse_file(file_path, request_type='th', misses=False):
    # src_type: None for th,'set', or 'get'
    if request_type == 'set':
        matching = 'Sets'
        position = 4
    elif request_type == 'get' or misses:
        matching = 'Gets'
        position = 3 if misses else 4
    elif request_type == 'th':
        matching = 'Totals'
        position = 1
    else:
        raise Exception('incorrect request type')

    with open(file_path) as f:
        for line in f:
            if line.startswith(matching):
                data = [x for x in line.split(" ") if x != ""][position]
                return float(data)


def get_files(number_of_clients, request_type, run):
    tmp = [
        os.path.join('..', '02_experiment', 'run', '02_1_experiment', request_type),
        os.path.join('..', '02_experiment', 'run', '02_2_experiment', request_type),
    ]
    dir_paths = []
    for t in tmp:
        dir_paths.append(os.path.join(t, run))

    file_paths = []
    for dir_path in dir_paths:
        for root, _, files in os.walk(dir_path):
            for file in files:
                if re.match(r'log-%s.txt' % number_of_clients, file):
                    file_paths.append(os.path.join(root, file))

    return file_paths


def get_RT(number_of_clients, request_type='set'):
    th_list = []
    full_data = []
    for run in ['1', '2', '3']:
        files = get_files(number_of_clients, request_type, run)
        data = [parse_file(file, request_type=request_type) for file in files]
        full_data.append([data])
        th_list.append(data)
        # th_list.append(np.mean(data))
    print(full_data)
    return th_list


def plot_image(request_type, all_MRTs_full, clients, file_path):
    data = {X_LABEL: [], Y_LABEL: []}
    clients = np.array(clients) * NUMBER_OF_CLIENTS * NUMBER_OF_INSTANCES * NUMBER_OF_THREADS

    for MRTs_full, client in zip(all_MRTs_full, clients):
        for MRT in MRTs_full:
            data[X_LABEL] += [client] * len(MRT)
            data[Y_LABEL] += MRT

    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)

    df = pd.DataFrame(data)
    sns.lineplot(x=X_LABEL, y=Y_LABEL, data=df, ci="sd")
    sns.scatterplot(x=clients, y=df.groupby([X_LABEL]).agg('mean')[Y_LABEL].values)

    x_thicks = [x for x in sorted(plt.xticks()[0].tolist() + [min(clients)]) if x >= min(clients)]
    ax.set_xticks(x_thicks)

    ax.set_ylim(ymin=0, ymax=140)
    ax.set_xlim(left=0, right=clients.max() + 1)

    if request_type=='set':
        plt.axvline(x=4, color='g', linestyle='--', label='saturation')
        # plt.axvline(x=4, color='r', linestyle='--', label='maximum throughput')
    elif request_type=='get':
        plt.axvline(x=4, color='g', linestyle='--', label='saturation')

    plt.legend()

    ax.grid(which='both')
    fig.show()
    fig.savefig(file_path)


def get_rt_all(request_type, clients):
    all_MRTs_full = list()

    for client in clients:
        MRTs_full = get_RT(client, request_type)
        all_MRTs_full.append(MRTs_full)

        print('final MRT is %s ms' % np.mean(MRTs_full))
    return all_MRTs_full


def generate_plot(request_type, file_path, clients):
    all_MRTs_full = get_rt_all(request_type, clients)

    plot_image(request_type, all_MRTs_full, clients, file_path)


# def main():
#     generate_plot('set', '02_experiment_RT_set.jpg')
#     generate_plot('get', '02_experiment_RT_get.jpg')


if __name__ == '__main__':
    NUMBER_OF_CLIENTS = 1
    NUMBER_OF_THREADS = 1
    NUMBER_OF_INSTANCES = 2
    clients = [1, 2, 4, 6, 8, 10, 14, 22, 32]

    Y_LABEL = 'Response Time [ms]'
    X_LABEL = 'Number of clients'
    generate_plot('set', '02_experiment_RT_set.jpg', clients)
    generate_plot('get', '02_experiment_RT_get.jpg', clients)

