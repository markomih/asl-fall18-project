#!/usr/bin/env bash

function populate_db {
    PARAMS1="-s ${SERVER1_PRIVATE} --port=${SERVER1_PORT} --protocol=memcache_text --key-maximum=10000 --data-size=4096 --clients=100 --threads=2 --test-time=200 --ratio=1:0"
    CMD1="/home/maki/memtier_benchmark-master/memtier_benchmark ${PARAMS1}"

    echo ${CLIENT1}
    ssh ${CLIENT1} "${CMD1}"
    echo "DB populated"
}

function ping_all {
    CMD1="ping -c 10 ${MW1_PRIVATE}"
    CMD2="ping -c 10 ${MW2_PRIVATE}"

    ssh ${CLIENT1} ${CMD1} > "client1_mw1.txt"
    ssh ${CLIENT2} ${CMD1} > "client2_mw1.txt"
    ssh ${CLIENT3} ${CMD1} > "client3_mw1.txt"

    ssh ${CLIENT1} ${CMD2} > "client1_mw2.txt"
    ssh ${CLIENT2} ${CMD2} > "client2_mw2.txt"
    ssh ${CLIENT3} ${CMD2} > "client3_mw2.txt"

    CMD_MW="ping -c 10 ${SERVER1_PRIVATE}"

    ssh ${MW1} ${CMD_MW} > "MW1_server1.txt"
    ssh ${MW2} ${CMD_MW} > "MW2_server1.txt"
}

CLIENT1=storeml4kjdlgl3y3csshpublicip1.westeurope.cloudapp.azure.com
CLIENT2=storeml4kjdlgl3y3csshpublicip2.westeurope.cloudapp.azure.com
CLIENT3=storeml4kjdlgl3y3csshpublicip3.westeurope.cloudapp.azure.com

SERVER1=storeml4kjdlgl3y3csshpublicip6.westeurope.cloudapp.azure.com
SERVER1_PRIVATE=10.0.0.11
SERVER1_PORT=11211

MW1=storeml4kjdlgl3y3csshpublicip4.westeurope.cloudapp.azure.com
MW2=storeml4kjdlgl3y3csshpublicip5.westeurope.cloudapp.azure.com

MW1_PRIVATE=10.0.0.7
MW2_PRIVATE=10.0.0.8
MW_PORT=45231

ping_all

#populate_db

cd /home/maki/projects/asl-fall18-project/asl-scripts/04_experiment/remote
./run_all.sh

echo "experiment 4 DONE"
#
#cd /home/maki/projects/asl-fall18-project/asl-scripts/03_experiment/remote
#./run_all.sh
