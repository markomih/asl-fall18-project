import os
from itertools import product

import pandas as pd
import numpy as np

from utils import get_mw_dir


def get_max_th(df_count):
    return np.diff(df_count['set'].values).max()


def get_service_rate(mw_thread):  # df for one th
    max_th_list = []
    for run in RUN:
        tmp_th1, tmp_th2, = [], []
        for client in CLIENTS:
            dir_path1, dir_path2 = get_mw_dir(client, 'set', run, mw_thread)
            file1 = os.path.join(dir_path1, 'counter_log_%s_false.txt' % mw_thread)
            file2 = os.path.join(dir_path2, 'counter_log_%s_false.txt' % mw_thread)
            df1, df2 = pd.read_csv(file1), pd.read_csv(file2)
            tmp_th1.append(get_max_th(df1))
            tmp_th2.append(get_max_th(df2))
        max_th_list.append(np.mean([max(tmp_th1), max(tmp_th2)]))

    return int(np.mean(max_th_list)) * 2


# def get_service_rate(mw_thread):  # df for one th
#     max_th_list = []
#     for run in RUN:
#         tmp_th = []
#         for client in CLIENTS:
#             dir_path1, dir_path2 = get_mw_dir(client, 'set', run, mw_thread)
#             file1 = os.path.join(dir_path1, 'counter_log_%s_false.txt' % mw_thread)
#             file2 = os.path.join(dir_path2, 'counter_log_%s_false.txt' % mw_thread)
#             df1, df2 = pd.read_csv(file1), pd.read_csv(file2)
#             tmp_th.append(get_max_th(df1))
#             tmp_th.append(get_max_th(df2))
#         max_th_list.append(max(tmp_th))
#
#     return int(np.mean(max_th_list)) * 2


def compute(data):
    df_dict = {'client': [], 'mw_thread': [], 'lam': [], 'mu': []}

    num_client = len(CLIENTS)

    for mw_thread in MW_THREAD:
        tmp = data[data['mw_thread'] == mw_thread]
        tmp = tmp.groupby(['client'], as_index=False).aggregate('mean')

        mu = get_service_rate(mw_thread)
        for client in CLIENTS:
            lam = tmp[tmp['client'] == client]['mw_th'].values[0]
            df_dict['lam'].append(lam)
            df_dict['client'].append(client)

        df_dict['mu'] += [mu] * num_client
        df_dict['mw_thread'] += [mw_thread] * num_client

    df = pd.DataFrame()
    for key, value in df_dict.items():
        df[key] = value

    df['rho'] = df['lam'] / df['mu']
    df['e_response_time'] = (1 / df['mu']) / (1 - df['rho']) * 1000
    df['e_queue_time'] = df['rho'] * df['e_response_time']
    df['mean_jobs'] = df['rho'] / (1 - df['rho'])
    df['mean_queue_jobs'] = df['rho'] * df['mean_jobs']

    print(df)
    df['my_e_response_time'] = 0
    df['my_e_queue_time'] = 0
    df['my_mean_jobs'] = 0
    df['my_mean_queue_jobs'] = 0

    tmp = data.groupby(['client', 'mw_thread'], as_index=False).aggregate('mean')
    for client, mw_thread in product(CLIENTS, MW_THREAD):
        inds = (tmp['client'] == client) & (tmp['mw_thread'] == mw_thread)
        df_inds = (df['client'] == client) & (df['mw_thread'] == mw_thread)

        df.loc[df_inds, 'my_e_response_time'] = tmp[inds]['mw_rt'].values[0]
        df.loc[df_inds, 'my_e_queue_time'] = tmp[inds]['r_internalQueueTimer'].values[0]
        df.loc[df_inds, 'my_mean_jobs'] = tmp[inds]['mw_systemRequests'].values[0]
        df.loc[df_inds, 'my_mean_queue_jobs'] = tmp[inds]['mw_queueLength'].values[0]

    return df


def print_latex(df, clients):
    inds = df['client'] == clients[0]
    for c in clients[1:]:
        inds = inds | (df['client'] == c)
    df = df[inds]

    lat_df = pd.DataFrame()
    lat_df[r'client'] = df['client'] * 6
    lat_df[r'workers'] = df['mw_thread']
    lat_df[r'$\lambda$'] = df['lam']
    lat_df[r'$\mu$'] = df['mu']
    lat_df[r'$\rho [\%]$'] = df['rho']

    lat_df[r'$E[r]$'] = df['e_response_time']
    lat_df[r'$E_M[r]$'] = df['my_e_response_time']

    lat_df[r'$E[q]$'] = df['e_queue_time']
    lat_df[r'$E_M[q]$'] = df['my_e_queue_time']

    lat_df[r'$E[n]$'] = df['mean_jobs']
    lat_df[r'$E_M[n]$'] = df['my_mean_jobs']*2

    lat_df[r'$E[n_q]$'] = df['mean_queue_jobs']
    lat_df[r'$E_M[n_q]$'] = df['my_mean_queue_jobs']*2

    lat_df = lat_df.sort_values(by=['client'])
    print(lat_df.round(2).to_latex(index=False, escape=False))


def main():
    df = pd.read_csv('stats_set.csv')
    df = compute(df)

    print_latex(df, [CLIENTS[3], CLIENTS[5], CLIENTS[6]])


if __name__ == '__main__':
    CLIENTS = (1, 2, 4, 8, 14, 22, 42, 64)
    MW_THREAD = (8, 16, 32, 64)
    RUN = ('1', '2', '3')
    main()
