import numpy as np
import seaborn as sns
from scipy import stats

import pandas as pd
import matplotlib.pyplot as plt


def plot_bucet(values, dst, x_label="Time difference [ns]", y_label='Elements per bucket'):
    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)

    ax = sns.distplot(values, kde=False)
    ax.set_ylim(bottom=0, top=300000)
    plt.xlabel(x_label)
    plt.ylabel(y_label)

    fig.show()
    fig.savefig(dst)


def main():
    file_name = '/home/maki/projects/asl-fall18-project/asl-scripts/08_experiment_poisson/MW1/MW_set_64_1_22/request_log_64_false.csv'
    df = pd.read_csv(file_name)
    df = df.sort_values(by=['id'])

    st = np.diff(df.startTime.values)

    df = df.sort_values(by=['leaveTime'])
    et = np.diff(df.leaveTime.values)
    # ax = sns.barplot(x=x_label, y=y_label, data=data, color=PALETTE[0])

    # ax = sns.barplot(y=st)
    plot_bucet(st[st<10], 'arrival.jpg')
    plot_bucet(et[et<10], 'leaving.jpg')


if __name__ == '__main__':
    PALETTE = sns.color_palette("tab10", 4)
    MARKER = "o"
    CI = 'sd'

    main()
