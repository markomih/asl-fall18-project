#!/usr/bin/env bash

#function populate_db {
#    test_time=800
#    PARAMS1="-s ${MW1_PRIVATE} --port=${MW_PORT} --protocol=memcache_text --key-maximum=10000 --data-size=4096 --clients=64 --threads=1 --test-time=${test_time} --ratio=1:0"
#    PARAMS2="-s ${MW2_PRIVATE} --port=${MW_PORT} --protocol=memcache_text --key-maximum=10000 --data-size=4096 --clients=64 --threads=1 --test-time=${test_time} --ratio=1:0"
##    PARAMS3="-s ${SERVER3_PRIVATE} --port=${SERVER_PORT} --protocol=memcache_text --key-maximum=10000 --data-size=4096 --clients=100 --threads=4 --test-time=200 --ratio=1:0"
#
#    CMD1="/home/maki/memtier_benchmark-master/memtier_benchmark ${PARAMS1}"
#    CMD2="/home/maki/memtier_benchmark-master/memtier_benchmark ${PARAMS2}"
##    CMD3="/home/maki/memtier_benchmark-master/memtier_benchmark ${PARAMS3}"
#
#    RUN_JAR_CMD1="java -jar ${MW_JAR_PATH} -l 0.0.0.0 -p ${MW_PORT} -t 64 -s false -m ${SERVER1_PRIVATE}:${SERVER_PORT} ${SERVER2_PRIVATE}:${SERVER_PORT} ${SERVER3_PRIVATE}:${SERVER_PORT}"
#
#    ssh ${MW1} "pkill java"
#    ssh ${MW2} "pkill java"
#    sleep 10s
#
#    ssh ${MW1} "${RUN_JAR_CMD1} &" &
#    ssh ${MW2} "${RUN_JAR_CMD1} &" &
#
#    sleep 10s
#    echo "done sleeping"
#    sleep 2s
#
#    ssh ${CLIENT1} "${CMD1}" &
#    ssh ${CLIENT2} "${CMD1}" &
#    ssh ${CLIENT3} "${CMD1}" &
#
#    ssh ${CLIENT3} "${CMD2}" &
#    ssh ${CLIENT3} "${CMD2}" &
#    ssh ${CLIENT3} "${CMD2}" &
#
#    sleep 800s
#    echo "Killing java"
#    ssh ${MW1} "pkill java"
#    ssh ${MW2} "pkill java"
#    wait
#
#    ssh ${MW1} "rm *.txt"
#    ssh ${MW1} "rm *.csv"
#
#    ssh ${MW2} "rm *.txt"
#    ssh ${MW2} "rm *.csv"
#
#    echo "DB populated"
#}

function ping_all {
    CMD1="ping -c 10 ${MW1_PRIVATE}"
    CMD2="ping -c 10 ${MW2_PRIVATE}"

    ssh ${CLIENT1} ${CMD1} > "client1_mw1.txt"
    ssh ${CLIENT2} ${CMD1} > "client2_mw1.txt"
    ssh ${CLIENT3} ${CMD1} > "client3_mw1.txt"

    ssh ${CLIENT1} ${CMD2} > "client1_mw2.txt"
    ssh ${CLIENT2} ${CMD2} > "client2_mw2.txt"
    ssh ${CLIENT3} ${CMD2} > "client3_mw2.txt"

    CMD_MW="ping -c 10 ${SERVER1_PRIVATE}"

    ssh ${MW1} ${CMD_MW} > "MW1_server1.txt"
    ssh ${MW2} ${CMD_MW} > "MW2_server1.txt"
}


CLIENT1=storexg5xzpqildpbosshpublicip1.westeurope.cloudapp.azure.com
CLIENT2=storexg5xzpqildpbosshpublicip2.westeurope.cloudapp.azure.com
CLIENT3=storexg5xzpqildpbosshpublicip3.westeurope.cloudapp.azure.com

SERVER1=storexg5xzpqildpbosshpublicip6.westeurope.cloudapp.azure.com
SERVER2=storexg5xzpqildpbosshpublicip7.westeurope.cloudapp.azure.com
SERVER3=storexg5xzpqildpbosshpublicip8.westeurope.cloudapp.azure.com

MW1=storexg5xzpqildpbosshpublicip4.westeurope.cloudapp.azure.com
MW2=storexg5xzpqildpbosshpublicip5.westeurope.cloudapp.azure.com

SERVER1_PRIVATE=10.0.0.7
SERVER2_PRIVATE=10.0.0.10
SERVER3_PRIVATE=10.0.0.8

MW1_PRIVATE=10.0.0.6
MW2_PRIVATE=10.0.0.11

SERVER_PORT=11211
MW_PORT=45231
MW_JAR_PATH="./mw.jar"

ping_all

#populate_db
#
#./run_all.sh
