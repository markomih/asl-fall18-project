from io import StringIO
import re
import numpy as np

import os
import pandas as pd
REMOTE_DIR = 'remote _before_rep'
REMOVE_LIMIT = (5, -5)


def parse_file(file_path, request_type='th', misses=False):
    # src_type: None for th,'set', or 'get'
    if request_type == 'set':
        matching = 'Sets'
        position = 4
    elif request_type == 'get' or misses:
        matching = 'Gets'
        position = 3 if misses else 4
    elif request_type == 'th':
        matching = 'Totals'
        position = 1
    else:
        raise Exception('incorrect request type')

    with open(file_path) as f:
        for line in f:
            if line.startswith(matching):
                data = [x for x in line.split(" ") if x != ""][position]
                return float(data)


def get_mw_dir(client, request_type, run, mw_thread):
    # return os.path.join('..', '03_experiment', REMOTE_DIR, 'MW1', 'MW_%s_%s_%s_%s' % (request_type, mw_thread, run,
    # client))
    return os.path.join('..', '03_experiment', REMOTE_DIR, 'MW1',
                        'MW_%s_%s_%s_%s' % (request_type, mw_thread, run, client))


def get_server_file(client, request_type, run, mw_thread):
    return os.path.join('..', '03_experiment', REMOTE_DIR, 'server1',
                        'server1_%s_%s_%s_%s.csv' % (mw_thread, request_type, run, client))


def get_client_files(number_of_clients, mw_thread, request_type, run):
    client_files = ['client1', 'client2', 'client3']
    dir_paths = [os.path.join('..', '03_experiment', REMOTE_DIR, c, request_type, run) for c in client_files]
    # dir_paths = [os.path.join('..', '03_experiment', REMOTE_DIR, c, request_type, run) for c in client_files]

    file_paths = []
    for dir_path in dir_paths:
        for root, _, files in os.walk(dir_path):
            for file in files:
                if re.match(r'log-%s-%s.txt' % (number_of_clients, mw_thread), file):
                    file_paths.append(os.path.join(root, file))
    return file_paths


def get_client_th(number_of_clients, mw_thread, request_type, run):
    files = get_client_files(number_of_clients, mw_thread, request_type, run)
    if (number_of_clients == 42):
        print(number_of_clients)
    return np.sum([parse_file(file, 'th') for file in files])


def get_client_rt(number_of_clients, mw_thread, request_type, run):
    files = get_client_files(number_of_clients, mw_thread, request_type, run)
    return np.mean([parse_file(file, request_type) for file in files])


def get_client_misses(number_of_clients, mw_thread, request_type, run):
    if request_type == 'set':
        return 0
    files = get_client_files(number_of_clients, mw_thread, request_type, run)
    # files = get_client_files(number_of_clients,mw_thread, 'set', run)
    return np.round(np.sum([parse_file(file, request_type, misses=True) for file in files]))


def get_mw_th(client, request_type, run, mw_thread, seconds=70):
    dir_path = get_mw_dir(client, request_type, run, mw_thread)
    file = os.path.join(dir_path, 'counter_log_%s_false.txt' % mw_thread)
    df = pd.read_csv(file)

    to_ret = df[request_type].max()
    if request_type == 'get':
        to_ret -= df['cachemisses'].max()
    to_ret = to_ret / seconds
    return to_ret


# def get_mw_rt(number_of_clients, request_type, run, mw_thread, seconds=70):
#     dir_path = get_mw_dir(number_of_clients, request_type, run, mw_thread)
#     file = os.path.join(dir_path, 'request_log_%s_false.csv' % mw_thread)
#     df = pd.read_csv(file)
#     df = df.loc[df['requestType'] == request_type.upper()]
#
#     df['RT'] = df['responseTimer']
#     df = df[['second', 'RT']]
#     # if mw_thread == 64 and run == '1' and number_of_clients == 4:
#     #     print(file)
#
#     # df = df.iloc[REMOVE_LIMIT[0]:REMOVE_LIMIT[1]]
#     df = df[df['second'] < seconds]
#     rt_by_sec = df.groupby(['second']).mean()
#     return rt_by_sec.mean()[0]


def get_mw_rt(number_of_clients, request_type, run, mw_thread, seconds=70):
    dir_path = get_mw_dir(number_of_clients, request_type, run, mw_thread)
    file = os.path.join(dir_path, 'request_log_%s_false.csv' % mw_thread)
    df = pd.read_csv(file)
    df = df.loc[df['requestType'] == request_type.upper()]

    # df['RT'] = df['responseTimer']
    # df = df[['second', 'RT']]
    # if mw_thread == 64 and run == '1' and number_of_clients == 4:
    #     print(file)

    df = df.iloc[REMOVE_LIMIT[0]:REMOVE_LIMIT[1]]
    df = df[df['second'] < seconds]
    rt_by_sec = df.groupby(['second']).mean()

    return abs(rt_by_sec['responseTimer'].mean()), \
           abs(rt_by_sec['internalQueueTimer'].mean()), \
           abs(rt_by_sec['processingTimer'].mean()), \
           abs(rt_by_sec['serviceTimer'].mean()), \
           abs(rt_by_sec['waitingServiceTimer'].mean()), \
           abs(rt_by_sec['returningResponseTimer'].mean())


def get_mw_misses(number_of_clients, request_type, run, mw_thread):
    if request_type == 'set':
        return 0

    dir_path = get_mw_dir(number_of_clients, request_type, run, mw_thread)
    file = os.path.join(dir_path, 'counter_log_%s_false.txt' % mw_thread)
    df = pd.read_csv(file)
    print(df['second'].max())
    return np.round(df['cachemisses'].max() / df['second'].max())


# def get_mw_queue_length(number_of_clients, request_type, run, mw_thread):
#     dir_path = get_mw_dir(number_of_clients, request_type, run, mw_thread)
#
#     file = os.path.join(dir_path, 'sys_log_%s_false.csv' % mw_thread)
#     df = pd.read_csv(file)
#
#     df = df.iloc[REMOVE_LIMIT[0]:REMOVE_LIMIT[1]]
#     return np.round(df['queueSize'].mean())


def get_mw_sys_info(number_of_clients, request_type, run, mw_thread):
    dir_path = get_mw_dir(number_of_clients, request_type, run, mw_thread)
    file = os.path.join(dir_path, 'sys_log_%s_false.csv' % mw_thread)
    df = pd.read_csv(file)
    df = df.iloc[REMOVE_LIMIT[0]:REMOVE_LIMIT[1]]

    return {
        'expectedArrivalRate': df['expectedArrivalRate'].mean(),
        'systemRequests': df['systemRequests'].mean(),
        'queueSize': df['queueSize'].mean(),
        'usedMemory': df['usedMemory'].mean(),
    }


#
# def get_mw_arrival_rate(number_of_clients, request_type, run, mw_thread):
#     dir_path = get_mw_dir(number_of_clients, request_type, run, mw_thread)
#     file = os.path.join(dir_path, 'sys_log_%s_false.csv' % mw_thread)
#     df = pd.read_csv(file)
#     return np.round(df['expectedArrivalRate'].sum() / df['second'].max())
#
#
# def get_mw_requests_in_system(number_of_clients, request_type, run, mw_thread):
#     dir_path = get_mw_dir(number_of_clients, request_type, run, mw_thread)
#     file = os.path.join(dir_path, 'sys_log_%s_false.csv' % mw_thread)
#     df = pd.read_csv(file)
#     df = df.iloc[REMOVE_LIMIT[0]:REMOVE_LIMIT[1]]
#
#     return np.round(df['systemRequests'].sum() / df['second'].max())

def parse_dstat(file):
    content = []
    with open(file, 'r') as f:
        for i, line in enumerate(f):
            if line.startswith(r'"usr"'):
                content = []
            content.append(line)
    content = '\n'.join(content)
    df = pd.read_csv(StringIO(content))

    return df


def get_dstat(number_of_clients, request_type, run, mw_thread):
    dir_path = get_mw_dir(number_of_clients, request_type, run, mw_thread)
    file = os.path.join(dir_path, 'dstat.csv')

    df = parse_dstat(file)
    df = df.iloc[REMOVE_LIMIT[0]:REMOVE_LIMIT[1]]
    return df['recv'].mean(), \
           df['send'].mean(), \
           df['idl'].mean()


def get_server_dstat(number_of_clients, request_type, run, mw_thread):
    server_path = get_server_file(number_of_clients, request_type, run, mw_thread)

    df = parse_dstat(server_path)

    df = df.iloc[REMOVE_LIMIT[0]:REMOVE_LIMIT[1]]
    return df['recv'].mean(), \
           df['send'].mean(), \
           df['idl'].mean()
