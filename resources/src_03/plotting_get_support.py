import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt


def plot_mw_send_to_client(df, dst, x_label='Number of clients', y_label='MBs sent from middleware to clients',
                           legend='threads'):
    data = pd.DataFrame()
    data[x_label] = df['client'] * CLIENT_FACTOR
    data[y_label] = df['client_recv']
    data[legend] = df['mw_thread']

    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)
    sns.lineplot(x=x_label, y=y_label, hue=legend, data=data, legend="full", ci=CI, marker=MARKER, palette=PALETTE)

    x_thicks = [x for x in sorted(plt.xticks()[0].tolist() + [data[x_label].min()]) if x >= data[x_label].min()]
    ax.set_xticks(x_thicks)

    ax.set_ylim(bottom=0, top=105)
    ax.set_xlim(left=0, right=data[x_label].max() + 1)

    plt.hlines(y=100, xmin=0, xmax=data[x_label].max() + 1, color='g', linestyle='--', label='bandwidth')
    plt.legend()

    ax.grid(True)
    fig.show()
    fig.savefig(dst)


def plot_server_to_mw(df, dst, x_label='Number of clients', y_label='MBs sent from server to middleware',
                      legend='threads'):
    data = pd.DataFrame()
    data[x_label] = df['client'] * CLIENT_FACTOR
    data[y_label] = df['server_send']
    data[legend] = df['mw_thread']

    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)
    sns.lineplot(x=x_label, y=y_label, hue=legend, data=data, legend="full", ci=CI, marker=MARKER, palette=PALETTE)

    x_thicks = [x for x in sorted(plt.xticks()[0].tolist() + [data[x_label].min()]) if x >= data[x_label].min()]
    ax.set_xticks(x_thicks)

    ax.set_ylim(bottom=0, top=105)
    ax.set_xlim(left=0, right=data[x_label].max() + 1)

    plt.hlines(y=12.5, xmin=0, xmax=data[x_label].max() + 1, color='g', linestyle='--', label='bandwidth')
    plt.legend()

    ax.grid(True)
    fig.show()
    fig.savefig(dst)


def plot_time_decomposition(df, dst, thread, x_label='Number of clients', y_label='Time request spend in MW [ms]'):
    data = df[df['mw_thread'] == thread]
    data[x_label] = df['client'] * CLIENT_FACTOR
    # data[y_label] = df['client_send']

    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)
    ax = sns.lineplot(x=x_label, y='mw_rt', data=data, legend=None, ci=CI, marker=MARKER, palette=PALETTE,
                      label='Total time')
    ax = sns.lineplot(x=x_label, y='r_internalQueueTimer', data=data, legend=None, ci=CI, marker=MARKER,
                      palette=PALETTE, label='Queueing time')
    ax = sns.lineplot(x=x_label, y='r_waitingServiceTimer', data=data, legend=None, ci=CI, marker=MARKER,
                      palette=PALETTE, label='Waiting memcached')

    plt.axvline(x=6, color='k', linestyle='--', label='Saturation')

    x_thicks = [x for x in sorted(plt.xticks()[0].tolist() + [data[x_label].min()]) if x >= data[x_label].min()]
    ax.set_xticks(x_thicks)

    plt.ylabel(y_label)
    plt.legend(loc=2)
    ax.set_ylim(bottom=0, top=80)
    ax.set_xlim(left=0, right=data[x_label].max() + 1)

    ax.grid(True)
    fig.show()
    fig.savefig(dst)


def main():
    get_df = pd.read_csv('stats_get.csv')

    # client send
    # scl = 1. / 2 ** 20  # in MBs
    # get_df['server_recv'] = get_df['server_recv'] * scl
    # get_df['server_send'] = get_df['server_send'] * scl
    # get_df['mw_recv'] = get_df['mw_recv'] * scl
    # get_df['mw_send'] = get_df['mw_send'] * scl
    #
    # get_df['client_send'] = get_df['mw_recv'] - get_df['server_send']
    # get_df['client_recv'] = get_df['mw_send'] - get_df['server_recv']
    #
    # plot_mw_send_to_client(get_df, '03_experiment_MwSendToClients_get.jpg')
    # plot_server_to_mw(get_df, '03_experiment_ServerToMW_get.jpg')

    thread = 64
    plot_time_decomposition(get_df, '03_experiment_rtDecomp%s_get.jpg' % thread, thread=thread)


if __name__ == '__main__':
    PALETTE = sns.color_palette("hls", 4)
    MARKER = "o"
    CI = 'sd'

    NUMBER_OF_CLIENTS = 3
    NUMBER_OF_INSTANCES = 1
    NUMBER_OF_THREADS = 2

    CLIENT_FACTOR = NUMBER_OF_CLIENTS * NUMBER_OF_INSTANCES * NUMBER_OF_THREADS
    main()
