import pandas as pd


def il(N, X, R, Z):
    return (N / X) / (R + Z)


def workload(request_type):
    if request_type == 'set':
        return 'Write'
    return 'Read'


def print_interactive_law():
    tmp = {'Workload': [], 'Workers': []}
    for client in [1, 2, 4, 8, 14, 22, 42, 64]:
        tmp['%s clients' % (client * CLIENT_FACTOR)] = []

    for request_type in ['set', 'get']:
        df = pd.read_csv('stats_%s.csv' % request_type)
        clients = sorted(set(df['client'].values.tolist()))
        threads = sorted(set(df['mw_thread'].values.tolist()))

        for thread in threads:
            tmp['Workload'].append(workload(request_type))
            tmp['Workers'].append(thread)
            for client in clients:
                N = client * CLIENT_FACTOR

                tmp_df = df[(df['client'] == client) & (df['mw_thread'] == thread)]
                X = tmp_df['mw_th'].mean() / 1000
                R = tmp_df['mw_rt'].mean()
                # Rc = tmp_df['c_rt'].mean()
                # Z = abs(R - Rc)
                Z = 1
                print(thread, N, '\t%.2f' % il(N, X, R, Z))

                tmp['%s clients' % N].append(il(N, X, R, Z))

    print(pd.DataFrame(tmp).round(2).to_latex(index=False))


def main():
    print_interactive_law()


if __name__ == '__main__':
    NUMBER_OF_CLIENTS = 3
    NUMBER_OF_INSTANCES = 1
    NUMBER_OF_THREADS = 2

    CLIENT_FACTOR = NUMBER_OF_CLIENTS * NUMBER_OF_INSTANCES * NUMBER_OF_THREADS

    main()
