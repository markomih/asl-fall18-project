2         Threads
2         Connections per thread
70        Seconds


ALL STATS
========================================================================
Type        Ops/sec     Hits/sec   Misses/sec      Latency       KB/sec 
------------------------------------------------------------------------
Sets           0.00          ---          ---      0.00000         0.00 
Gets         985.74       985.74         0.00      4.04700      3992.80 
Waits          0.00          ---          ---      0.00000          --- 
Totals       985.74       985.74         0.00      4.04700      3992.80 


Request Latency Distribution
Type     <= msec         Percent
------------------------------------------------------------------------
---
GET       0.810         0.00
GET       0.820         0.00
GET       0.850         0.01
GET       0.880         0.01
GET       0.890         0.01
GET       0.900         0.01
GET       0.910         0.02
GET       0.920         0.02
GET       0.930         0.03
GET       0.940         0.03
GET       0.950         0.05
GET       0.960         0.07
GET       0.970         0.08
GET       0.980         0.09
GET       0.990         0.10
GET       1.000         0.22
GET       1.100         0.88
GET       1.200         2.31
GET       1.300         5.01
GET       1.400         8.93
GET       1.500        13.92
GET       1.600        19.54
GET       1.700        25.75
GET       1.800        31.76
GET       1.900        37.38
GET       2.000        42.51
GET       2.100        46.98
GET       2.200        50.70
GET       2.300        53.80
GET       2.400        56.44
GET       2.500        58.78
GET       2.600        60.72
GET       2.700        62.46
GET       2.800        63.90
GET       2.900        65.14
GET       3.000        66.15
GET       3.100        66.99
GET       3.200        67.75
GET       3.300        68.38
GET       3.400        68.91
GET       3.500        69.34
GET       3.600        69.80
GET       3.700        70.21
GET       3.800        70.57
GET       3.900        70.83
GET       4.000        71.11
GET       4.100        71.39
GET       4.200        71.66
GET       4.300        71.86
GET       4.400        72.09
GET       4.500        72.33
GET       4.600        72.50
GET       4.700        72.68
GET       4.800        72.88
GET       4.900        73.05
GET       5.000        73.23
GET       5.100        73.39
GET       5.200        73.54
GET       5.300        73.69
GET       5.400        73.84
GET       5.500        73.98
GET       5.600        74.12
GET       5.700        74.29
GET       5.800        74.44
GET       5.900        74.58
GET       6.000        74.72
GET       6.100        74.90
GET       6.200        75.08
GET       6.300        75.23
GET       6.400        75.37
GET       6.500        75.53
GET       6.600        75.70
GET       6.700        75.85
GET       6.800        76.00
GET       6.900        76.22
GET       7.000        76.39
GET       7.100        76.60
GET       7.200        76.81
GET       7.300        77.05
GET       7.400        77.28
GET       7.500        77.49
GET       7.600        77.77
GET       7.700        77.99
GET       7.800        78.24
GET       7.900        78.53
GET       8.000        78.84
GET       8.100        79.15
GET       8.200        79.51
GET       8.300        79.91
GET       8.400        80.30
GET       8.500        80.75
GET       8.600        81.16
GET       8.700        81.65
GET       8.800        82.15
GET       8.900        82.67
GET       9.000        83.21
GET       9.100        83.89
GET       9.200        84.44
GET       9.300        85.07
GET       9.400        85.85
GET       9.500        86.58
GET       9.600        87.38
GET       9.700        88.22
GET       9.800        89.13
GET       9.900        90.05
GET      10.000        94.48
GET      11.000        98.77
GET      12.000        99.51
GET      13.000        99.65
GET      14.000        99.76
GET      15.000        99.82
GET      16.000        99.85
GET      17.000        99.88
GET      18.000        99.89
GET      19.000        99.90
GET      20.000        99.91
GET      21.000        99.91
GET      22.000        99.93
GET      23.000        99.95
GET      24.000        99.97
GET      25.000        99.99
GET      26.000        99.99
GET     220.000       100.00
---
