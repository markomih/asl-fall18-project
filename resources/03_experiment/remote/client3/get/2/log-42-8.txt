2         Threads
42        Connections per thread
70        Seconds


ALL STATS
========================================================================
Type        Ops/sec     Hits/sec   Misses/sec      Latency       KB/sec 
------------------------------------------------------------------------
Sets           0.00          ---          ---      0.00000         0.00 
Gets         979.98       979.98         0.00     85.70000      3969.51 
Waits          0.00          ---          ---      0.00000          --- 
Totals       979.98       979.98         0.00     85.70000      3969.51 


Request Latency Distribution
Type     <= msec         Percent
------------------------------------------------------------------------
---
GET      42.000         0.00
GET      43.000         0.00
GET      44.000         0.01
GET      45.000         0.02
GET      46.000         0.03
GET      47.000         0.05
GET      48.000         0.07
GET      49.000         0.08
GET      50.000         0.10
GET      51.000         0.11
GET      52.000         0.12
GET      53.000         0.14
GET      54.000         0.16
GET      55.000         0.16
GET      56.000         0.18
GET      57.000         0.19
GET      58.000         0.20
GET      59.000         0.21
GET      60.000         0.24
GET      61.000         0.27
GET      62.000         0.30
GET      63.000         0.37
GET      64.000         0.43
GET      65.000         0.61
GET      66.000         0.89
GET      67.000         1.08
GET      68.000         1.25
GET      69.000         1.33
GET      70.000         1.39
GET      71.000         1.52
GET      72.000         1.69
GET      73.000         1.92
GET      74.000         2.20
GET      75.000         2.56
GET      76.000         2.85
GET      77.000         3.30
GET      78.000         3.99
GET      79.000         5.79
GET      80.000        11.82
GET      81.000        25.09
GET      82.000        36.63
GET      83.000        44.00
GET      84.000        47.88
GET      85.000        50.55
GET      86.000        52.82
GET      87.000        55.53
GET      88.000        60.20
GET      89.000        68.65
GET      90.000        81.52
GET      91.000        92.53
GET      92.000        96.66
GET      93.000        98.06
GET      94.000        98.68
GET      95.000        98.99
GET      96.000        99.23
GET      97.000        99.38
GET      98.000        99.51
GET      99.000        99.61
GET     100.000        99.78
GET     110.000        99.88
GET     220.000        99.88
GET     230.000        99.88
GET     240.000        99.89
GET     250.000        99.90
GET     260.000        99.92
GET     270.000        99.93
GET     280.000        99.94
GET     290.000        99.95
GET     300.000        99.98
GET     310.000       100.00
---
