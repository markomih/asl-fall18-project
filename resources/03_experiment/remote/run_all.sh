#!/usr/bin/env bash


CLIENT1=storexg5xzpqildpbosshpublicip1.westeurope.cloudapp.azure.com
CLIENT2=storexg5xzpqildpbosshpublicip2.westeurope.cloudapp.azure.com
CLIENT3=storexg5xzpqildpbosshpublicip3.westeurope.cloudapp.azure.com


SERVER1=storexg5xzpqildpbosshpublicip6.westeurope.cloudapp.azure.com
SERVER1_PRIVATE=10.0.0.7
SERVER1_PORT=11211

MW1=storexg5xzpqildpbosshpublicip4.westeurope.cloudapp.azure.com
MW1_PRIVATE=10.0.0.6
MW_PORT=45231

CLIENT_DIR='03_experiment'

#specifying MW
MW_THREADS=( 8 64)
MW_JAR_PATH="./mw.jar"
SHARDED="false"

# specifying clients
run=( 1 2 3 )

THREADS=2
DATA_SIZE=4096
KEY_MAX=10000

TEST_TIME=70
DSTAT_TIME=70

MW_WAITING_TIME=10
TOTAL_TIME=85


#################### SET ####################
clients=( 64 )
#clients=( 1 2 4 8 14 22 42 64 )
TYPE='set'
SET_RATIO=1
GET_RATIO=0

MW_DSTAT_CMD="dstat --output dstat.csv 1 ${DSTAT_TIME}"

#ssh ${SERVER1} "rm *.csv"
## prepare directories
#for experiment in "${run[@]}"
#do
#        MKDIR="mkdir -p ${CLIENT_DIR}/${TYPE}/${experiment}"
#        RMFILES="rm ${CLIENT_DIR}/${TYPE}/${experiment}/*"
#
#        ssh ${CLIENT1} "${MKDIR};${RMFILES}"
#        ssh ${CLIENT2} "${MKDIR};${RMFILES}"
#        ssh ${CLIENT3} "${MKDIR};${RMFILES}"
#done

for mw_thread in "${MW_THREADS[@]}"
do
    for experiment in "${run[@]}"
    do
        CDDIR="cd ${CLIENT_DIR}/${TYPE}/${experiment}"

        for client in "${clients[@]}"
        do
            DSTAT_CMD_SERVER1="dstat --output server1_${mw_thread}_${TYPE}_${experiment}_${client}.csv 1 ${DSTAT_TIME}"

            PARAMS="-s ${MW1_PRIVATE} --port=${MW_PORT} --clients=${client} --threads=${THREADS} --test-time=${TEST_TIME} --protocol=memcache_text --ratio=${SET_RATIO}:${GET_RATIO} --key-maximum=${KEY_MAX} --data-size=${DATA_SIZE} --out-file=log-${client}-${mw_thread}.txt"
            CMD="/home/maki/memtier_benchmark-master/memtier_benchmark ${PARAMS}"
            # RUN MW here
            RUN_JAR_CMD="java -jar ${MW_JAR_PATH} -l 0.0.0.0 -p ${MW_PORT} -t ${mw_thread} -s ${SHARDED} -m ${SERVER1_PRIVATE}:${SERVER1_PORT}"
            ssh ${MW1} "${RUN_JAR_CMD}" &
#                MW_PID=$!
            sleep ${MW_WAITING_TIME}s

            # RUN CLients
            echo "executing: $CMD"
            echo "run=$experiment; client=$client"
            ssh ${CLIENT1} "${CDDIR};${CMD};" &
            ssh ${CLIENT2} "${CDDIR};${CMD};" &
            ssh ${CLIENT3} "${CDDIR};${CMD};" &

            ssh ${MW1} "${MW_DSTAT_CMD}" &
            ssh ${SERVER1} "${DSTAT_CMD_SERVER1}" &

            echo "Sleep before Killing java"
            sleep ${TOTAL_TIME}s

            echo "Killing java"
            ssh ${MW1} "pkill java"
            wait

            # copy MW files and delete rest
            DST_DIR="./MW1/MW_${TYPE}_${mw_thread}_${experiment}_${client}"
            mkdir -p ${DST_DIR}
            scp -r ${MW1}:*.txt ./${DST_DIR}
            scp -r ${MW1}:*.csv ./${DST_DIR}
            sleep 2s

            ssh ${MW1} "rm *.txt"
            ssh ${MW1} "rm *.csv"

            echo "DONE $client, $experiment, $mw_thread"
            echo
        done
    done
done


#################### GET ####################

#TYPE='get'
#SET_RATIO=0
#GET_RATIO=1
#
## prepare directories
#for experiment in "${run[@]}"
#do
#        MKDIR="mkdir -p ${CLIENT_DIR}/${TYPE}/${experiment}"
#        RMFILES="rm ${CLIENT_DIR}/${TYPE}/${experiment}/*"
#
#        ssh ${CLIENT1} "${MKDIR};${RMFILES}"
#        ssh ${CLIENT2} "${MKDIR};${RMFILES}"
#        ssh ${CLIENT3} "${MKDIR};${RMFILES}"
#done
#
#for mw_thread in "${MW_THREADS[@]}"
#do
#    for experiment in "${run[@]}"
#    do
#        CDDIR="cd ${CLIENT_DIR}/${TYPE}/${experiment}"
#
#        for client in "${clients[@]}"
#        do
#            DSTAT_CMD_SERVER1="dstat --output server1_${mw_thread}_${TYPE}_${experiment}_${client}.csv 1 ${DSTAT_TIME}"
#
#            PARAMS="-s ${MW1_PRIVATE} --port=${MW_PORT} --clients=${client} --threads=${THREADS} --test-time=${TEST_TIME} --protocol=memcache_text --ratio=${SET_RATIO}:${GET_RATIO} --key-maximum=${KEY_MAX} --data-size=${DATA_SIZE} --out-file=log-${client}-${mw_thread}.txt"
#            CMD="/home/maki/memtier_benchmark-master/memtier_benchmark ${PARAMS}"
#            # RUN MW here
#            RUN_JAR_CMD="java -jar ${MW_JAR_PATH} -l 0.0.0.0 -p ${MW_PORT} -t ${mw_thread} -s ${SHARDED} -m ${SERVER1_PRIVATE}:${SERVER1_PORT}"
#            ssh ${MW1} "${RUN_JAR_CMD}" &
##                MW_PID=$!
#            sleep ${MW_WAITING_TIME}s
#
#            # RUN CLients
#            echo "executing: $CMD"
#            echo "run=$experiment; client=$client"
#            ssh ${CLIENT1} "${CDDIR};${CMD};" &
#            ssh ${CLIENT2} "${CDDIR};${CMD};" &
#            ssh ${CLIENT3} "${CDDIR};${CMD};" &
#
#            ssh ${MW1} "${MW_DSTAT_CMD}" &
#            ssh ${SERVER1} "${DSTAT_CMD_SERVER1}" &
#
#            echo "Sleep before Killing java"
#            sleep ${TOTAL_TIME}s
#
#            echo "Killing java"
#            ssh ${MW1} "pkill java"
#            wait
#
#            # copy MW files and delete rest
#            DST_DIR="./MW1/MW_${TYPE}_${mw_thread}_${experiment}_${client}"
#            mkdir -p ${DST_DIR}
#            scp -r ${MW1}:*.txt ./${DST_DIR}
#            scp -r ${MW1}:*.csv ./${DST_DIR}
#            sleep 2s
#
#            ssh ${MW1} "rm *.txt"
#            ssh ${MW1} "rm *.csv"
#
#            echo "DONE $client, $experiment, $mw_thread"
#            echo
#        done
#    done
#done

########### CPY files ################
mkdir -p server1
scp -r ${SERVER1}:*.csv ./server1/

scp -r ${CLIENT1}:${CLIENT_DIR} client1
scp -r ${CLIENT2}:${CLIENT_DIR} client2
scp -r ${CLIENT3}:${CLIENT_DIR} client3


########### STOPPING ################
#az vm stop --resource-group asl-group --name Server1
#az vm stop --resource-group asl-group --name Middleware1
#az vm stop --resource-group asl-group --name Client1
#az vm stop --resource-group asl-group --name Client2
#az vm stop --resource-group asl-group --name Client3
#
#az vm deallocate --resource-group asl-group --name Server1
#az vm deallocate --resource-group asl-group --name Middleware1
#az vm deallocate --resource-group asl-group --name Client1
#az vm deallocate --resource-group asl-group --name Client2
#az vm deallocate --resource-group asl-group --name Client3