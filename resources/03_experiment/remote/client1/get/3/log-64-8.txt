2         Threads
64        Connections per thread
70        Seconds


ALL STATS
========================================================================
Type        Ops/sec     Hits/sec   Misses/sec      Latency       KB/sec 
------------------------------------------------------------------------
Sets           0.00          ---          ---      0.00000         0.00 
Gets         981.89       981.89         0.00    130.35500      3977.24 
Waits          0.00          ---          ---      0.00000          --- 
Totals       981.89       981.89         0.00    130.35500      3977.24 


Request Latency Distribution
Type     <= msec         Percent
------------------------------------------------------------------------
---
GET      11.000         0.00
GET      12.000         0.01
GET      15.000         0.01
GET      20.000         0.01
GET      22.000         0.01
GET      27.000         0.02
GET      28.000         0.02
GET      30.000         0.02
GET      31.000         0.03
GET      32.000         0.05
GET      33.000         0.07
GET      34.000         0.08
GET      35.000         0.08
GET      36.000         0.08
GET      37.000         0.09
GET      38.000         0.09
GET      40.000         0.09
GET      41.000         0.09
GET      42.000         0.10
GET      45.000         0.10
GET      47.000         0.10
GET      54.000         0.11
GET      56.000         0.11
GET      57.000         0.11
GET      62.000         0.11
GET      64.000         0.11
GET      65.000         0.11
GET      66.000         0.12
GET      67.000         0.12
GET      68.000         0.12
GET      69.000         0.14
GET      70.000         0.17
GET      71.000         0.22
GET      72.000         0.25
GET      73.000         0.27
GET      74.000         0.31
GET      75.000         0.33
GET      76.000         0.34
GET      77.000         0.37
GET      78.000         0.38
GET      79.000         0.38
GET      80.000         0.39
GET      81.000         0.39
GET      82.000         0.40
GET      83.000         0.40
GET      84.000         0.40
GET      85.000         0.40
GET      86.000         0.41
GET      87.000         0.41
GET      88.000         0.43
GET      89.000         0.45
GET      90.000         0.48
GET      91.000         0.51
GET      92.000         0.57
GET      93.000         0.67
GET      94.000         0.78
GET      95.000         1.02
GET      96.000         1.19
GET      97.000         1.31
GET      98.000         1.37
GET      99.000         1.41
GET     100.000         1.60
GET     110.000         2.52
GET     120.000         6.42
GET     130.000        75.26
GET     140.000        99.53
GET     150.000        99.82
GET     160.000        99.82
GET     290.000        99.83
GET     300.000        99.89
GET     310.000        99.91
GET     320.000        99.93
GET     340.000        99.94
GET     350.000        99.97
GET     360.000        99.99
GET     370.000        99.99
GET     380.000       100.00
---
