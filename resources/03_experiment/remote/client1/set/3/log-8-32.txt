2         Threads
8         Connections per thread
70        Seconds


ALL STATS
========================================================================
Type        Ops/sec     Hits/sec   Misses/sec      Latency       KB/sec 
------------------------------------------------------------------------
Sets        3175.57          ---          ---      5.03300     12816.69 
Gets           0.00         0.00         0.00      0.00000         0.00 
Waits          0.00          ---          ---      0.00000          --- 
Totals      3175.57         0.00         0.00      5.03300     12816.69 


Request Latency Distribution
Type     <= msec         Percent
------------------------------------------------------------------------
SET       1.300         0.00
SET       1.400         0.05
SET       1.500         0.29
SET       1.600         0.89
SET       1.700         2.03
SET       1.800         3.81
SET       1.900         6.23
SET       2.000         9.33
SET       2.100        12.80
SET       2.200        16.54
SET       2.300        20.55
SET       2.400        24.39
SET       2.500        28.24
SET       2.600        31.84
SET       2.700        35.12
SET       2.800        38.15
SET       2.900        40.94
SET       3.000        43.46
SET       3.100        45.68
SET       3.200        47.63
SET       3.300        49.34
SET       3.400        50.75
SET       3.500        51.95
SET       3.600        53.04
SET       3.700        53.93
SET       3.800        54.69
SET       3.900        55.36
SET       4.000        55.97
SET       4.100        56.49
SET       4.200        56.92
SET       4.300        57.29
SET       4.400        57.65
SET       4.500        57.94
SET       4.600        58.22
SET       4.700        58.50
SET       4.800        58.73
SET       4.900        58.97
SET       5.000        59.18
SET       5.100        59.41
SET       5.200        59.61
SET       5.300        59.80
SET       5.400        59.97
SET       5.500        60.13
SET       5.600        60.29
SET       5.700        60.45
SET       5.800        60.61
SET       5.900        60.78
SET       6.000        60.96
SET       6.100        61.15
SET       6.200        61.35
SET       6.300        61.55
SET       6.400        61.78
SET       6.500        62.01
SET       6.600        62.31
SET       6.700        62.65
SET       6.800        63.03
SET       6.900        63.52
SET       7.000        64.10
SET       7.100        64.78
SET       7.200        65.62
SET       7.300        66.60
SET       7.400        67.73
SET       7.500        68.99
SET       7.600        70.44
SET       7.700        71.98
SET       7.800        73.67
SET       7.900        75.42
SET       8.000        77.31
SET       8.100        79.16
SET       8.200        80.94
SET       8.300        82.64
SET       8.400        84.28
SET       8.500        85.80
SET       8.600        87.23
SET       8.700        88.50
SET       8.800        89.67
SET       8.900        90.66
SET       9.000        91.60
SET       9.100        92.41
SET       9.200        93.08
SET       9.300        93.69
SET       9.400        94.22
SET       9.500        94.67
SET       9.600        95.08
SET       9.700        95.45
SET       9.800        95.78
SET       9.900        96.08
SET      10.000        97.28
SET      11.000        98.41
SET      12.000        99.01
SET      13.000        99.39
SET      14.000        99.57
SET      15.000        99.66
SET      16.000        99.74
SET      17.000        99.82
SET      18.000        99.87
SET      19.000        99.91
SET      20.000        99.93
SET      21.000        99.95
SET      22.000        99.95
SET      23.000        99.96
SET      24.000        99.96
SET      25.000        99.96
SET      26.000        99.96
SET      27.000        99.97
SET      28.000        99.97
SET      29.000        99.97
SET      30.000        99.97
SET      31.000        99.98
SET      32.000        99.98
SET      33.000        99.98
SET      34.000        99.98
SET      35.000        99.98
SET      36.000        99.98
SET      37.000        99.98
SET      38.000        99.98
SET      39.000        99.98
SET      40.000        99.99
SET      42.000        99.99
SET      43.000        99.99
SET      44.000        99.99
SET      45.000        99.99
SET      47.000        99.99
SET      48.000        99.99
SET      50.000        99.99
SET      51.000        99.99
SET     100.000       100.00
---
---
