2         Threads
22        Connections per thread
70        Seconds


ALL STATS
========================================================================
Type        Ops/sec     Hits/sec   Misses/sec      Latency       KB/sec 
------------------------------------------------------------------------
Sets        3431.00          ---          ---     12.81900     13847.59 
Gets           0.00         0.00         0.00      0.00000         0.00 
Waits          0.00          ---          ---      0.00000          --- 
Totals      3431.00         0.00         0.00     12.81900     13847.59 


Request Latency Distribution
Type     <= msec         Percent
------------------------------------------------------------------------
SET       1.700         0.00
SET       1.900         0.00
SET       2.000         0.00
SET       2.100         0.01
SET       2.200         0.01
SET       2.300         0.01
SET       2.400         0.01
SET       2.500         0.02
SET       2.600         0.02
SET       2.700         0.02
SET       2.800         0.03
SET       2.900         0.03
SET       3.000         0.03
SET       3.100         0.04
SET       3.200         0.04
SET       3.300         0.05
SET       3.400         0.06
SET       3.500         0.08
SET       3.600         0.08
SET       3.700         0.09
SET       3.800         0.11
SET       3.900         0.14
SET       4.000         0.17
SET       4.100         0.19
SET       4.200         0.22
SET       4.300         0.24
SET       4.400         0.27
SET       4.500         0.31
SET       4.600         0.35
SET       4.700         0.39
SET       4.800         0.44
SET       4.900         0.48
SET       5.000         0.54
SET       5.100         0.60
SET       5.200         0.67
SET       5.300         0.75
SET       5.400         0.84
SET       5.500         0.94
SET       5.600         1.04
SET       5.700         1.17
SET       5.800         1.29
SET       5.900         1.43
SET       6.000         1.58
SET       6.100         1.75
SET       6.200         1.93
SET       6.300         2.13
SET       6.400         2.32
SET       6.500         2.52
SET       6.600         2.73
SET       6.700         2.96
SET       6.800         3.20
SET       6.900         3.46
SET       7.000         3.73
SET       7.100         4.01
SET       7.200         4.33
SET       7.300         4.65
SET       7.400         4.96
SET       7.500         5.28
SET       7.600         5.62
SET       7.700         6.00
SET       7.800         6.36
SET       7.900         6.73
SET       8.000         7.11
SET       8.100         7.47
SET       8.200         7.80
SET       8.300         8.16
SET       8.400         8.48
SET       8.500         8.79
SET       8.600         9.10
SET       8.700         9.39
SET       8.800         9.69
SET       8.900         9.98
SET       9.000        10.27
SET       9.100        10.54
SET       9.200        10.81
SET       9.300        11.12
SET       9.400        11.43
SET       9.500        11.74
SET       9.600        12.04
SET       9.700        12.39
SET       9.800        12.78
SET       9.900        13.20
SET      10.000        15.85
SET      11.000        26.89
SET      12.000        46.82
SET      13.000        67.24
SET      14.000        80.88
SET      15.000        87.73
SET      16.000        91.46
SET      17.000        93.96
SET      18.000        95.80
SET      19.000        97.14
SET      20.000        98.04
SET      21.000        98.67
SET      22.000        99.08
SET      23.000        99.32
SET      24.000        99.48
SET      25.000        99.60
SET      26.000        99.67
SET      27.000        99.71
SET      28.000        99.76
SET      29.000        99.82
SET      30.000        99.85
SET      31.000        99.88
SET      32.000        99.92
SET      33.000        99.93
SET      34.000        99.94
SET      35.000        99.95
SET      36.000        99.95
SET      37.000        99.96
SET      38.000        99.96
SET      39.000        99.97
SET      40.000        99.97
SET      41.000        99.97
SET      42.000        99.97
SET      43.000        99.97
SET      44.000        99.97
SET      45.000        99.97
SET      46.000        99.97
SET      47.000        99.98
SET      51.000        99.98
SET      52.000        99.98
SET      54.000        99.98
SET      55.000        99.98
SET      57.000        99.98
SET      59.000        99.98
SET      60.000        99.98
SET      62.000        99.98
SET      63.000        99.98
SET      64.000        99.98
SET      69.000        99.98
SET     270.000        99.98
SET     300.000        99.98
SET     320.000        99.98
SET     360.000        99.98
SET     370.000        99.99
SET     380.000        99.99
SET     390.000        99.99
SET     400.000        99.99
SET     410.000        99.99
SET     420.000        99.99
SET     430.000        99.99
SET     440.000        99.99
SET     450.000        99.99
SET     470.000       100.00
SET     480.000       100.00
---
---
