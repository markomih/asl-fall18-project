import pandas as pd
import numpy as np
import subprocess
import re
import copy


def get_s(df, workers, client):
    tmp = df[(df['mw_thread'] == workers) & (df['client'] == client)]
    tmp = tmp.groupby('client').aggregate('mean')
    # print('RT=', tmp.c_rt, '\nTH=', tmp.mw_th, '\nqueue=', tmp.r_internalQueueTimer, '\nQ=', tmp.mw_systemRequests)

    l = {
        'rtt': np.mean(tmp.c_rt - tmp.mw_rt),
        'mw': 0.001,
        'worker':
            (tmp.r_internalQueueTimer + tmp.r_processingTimer + tmp.r_serviceTimer + tmp.r_waitingServiceTimer).values[
                0],
        'ret': tmp.r_returningResponseTimer.values[0]
    }
    return '[%f,%f,%f,%f]' % (l['rtt'], l['mw'], l['worker'], l['ret']), [l['rtt'], l['mw'], l['worker'], l['ret']]


def get_valeus(df, workers=64, clients=1):
    s, s_val = get_s(df, workers, clients)
    clients = clients * 6
    cmd = 'octave --eval "cd /home/maki/octave/queueing-1.2.6; [RT, TH, Q, U]=net1(%s,%s,%s)"' % (workers, clients, s)
    # ret = os.system(cmd)
    ret = subprocess.run(cmd, shell=True, stdout=subprocess.PIPE, universal_newlines=True).stdout
    print(ret)
    ret = re.sub(' +', ' ', ret)
    ret = ret.split('=')
    rt = float(ret[1].replace('\nTH', ''))
    th = float(ret[2].replace('\nQ', ''))

    q = [float(x) for x in ret[3].replace('\n', '').replace('U', '').strip().split(' ')]
    u = [float(x) for x in ret[4].replace('\n', '').replace('U', '').strip().split(' ')]
    print('Sum(u)=', sum(u))
    return rt, th, q, u, s_val


def flatten(l):
    return sum(l, [])


def main():
    tmp = {
        'Workload': [],
        'Workers': [],
        'Clients': [],
        'RT': [],
        'TH': [],
        'RT_M': [],
        'TH_M': [],
    }
    full_tmp = {}
    for i in range(4):
        full_tmp['D_%s' % (i + 1)] = []

    for i in range(len(LOAD)):
        file = 'stats_%s.csv' % ('get' if LOAD[i] == 'Read' else 'set')
        df = pd.read_csv(file)

        tmp['Workload'].append(LOAD[i])
        tmp['Workers'].append(THREAD[i])
        tmp['Clients'].append(CLIENT[i] * 6)

        rt, th, q, u, s = get_valeus(df, workers=THREAD[i], clients=CLIENT[i])

        tmp['RT'].append(rt * 1000)
        tmp['TH'].append(th)

        tmp['RT_M'].append(df[(df['client'] == CLIENT[i]) & (df['mw_thread'] == THREAD[i])].mean()['c_rt'])
        tmp['TH_M'].append(df[(df['client'] == CLIENT[i]) & (df['mw_thread'] == THREAD[i])].mean()['c_th'])
        qm = df[(df['client'] == CLIENT[i]) & (df['mw_thread'] == THREAD[i])].mean()['mw_systemRequests']

        for j in range(len(s)):
            full_tmp['D_%s' % (j + 1)].append(s[j])
            full_tmp['D_%s' % (j + 1)].append(u[j])
            full_tmp['D_%s' % (j + 1)].append(q[j])
            full_tmp['D_%s' % (j + 1)].append(DEL_NUM if j + 1 != 3 else qm)

    df = pd.DataFrame(tmp)
    print(df.round(2).to_latex(escape=False))

    indexes = flatten([['S_%s' % i, 'U_%s' % i, 'Q_%s' % i, 'Q_{m%s}' % i] for i in range(1, 5)])
    df_full = pd.DataFrame(full_tmp, index=pd.Index(indexes))

    df_full = df_full.round(3)
    latex = df_full.to_latex(escape=False)
    print(latex.replace('%.3f' % DEL_NUM, ''))


if __name__ == '__main__':
    LOAD = ('Write', 'Write', 'Read', 'Read')
    THREAD = (8, 64, 8, 64)
    CLIENT = (2, 8, 1, 1)
    DEL_NUM = 8888
    main()

# s='[0.838160,0.001000,1.271957,0.018742]'
