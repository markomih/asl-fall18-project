import os
from itertools import product

import pandas as pd
import numpy as np

from utils import get_mw_dir


def get_mw_max_th(df_count):
    return np.diff(df_count['set'].values).max()


def get_mw_service_rate(mw_thread):  # df for one th
    max_th_list = []
    for run in RUN:
        tmp_th = []
        for client in CLIENTS:
            dir_path = get_mw_dir(client, 'set', run, mw_thread)
            file = os.path.join(dir_path, 'counter_log_%s_false.txt' % mw_thread)
            df1 = pd.read_csv(file)
            tmp_th.append(get_mw_max_th(df1))
        max_th_list.append(max(tmp_th))

    return int(np.mean(max_th_list))


def get_worker_max_th(df_count, mw_thread):
    seconds = df_count['second'].max()

    th_thread = df_count.groupby('threadId').aggregate('count')['id'].max() / seconds
    return th_thread


def get_worker_service_rate(mw_thread):
    max_th_list = []
    for run in RUN:
        tmp_th = []
        for client in CLIENTS:
            dir_path1 = get_mw_dir(client, 'set', run, mw_thread)
            file1 = os.path.join(dir_path1, 'request_log_%s_false.csv' % mw_thread)
            df1 = pd.read_csv(file1)
            tmp_th.append(get_worker_max_th(df1, mw_thread))
        max_th_list.append(max(tmp_th))

    return int(np.mean(max_th_list))


def compute(data):
    mw_thread = 64
    client = 42

    tmp = data[data['mw_thread'] == mw_thread]
    tmp = tmp.groupby(['client'], as_index=False).aggregate('mean')

    mu_mw = get_mw_service_rate(mw_thread)
    mu_worker = get_worker_service_rate(mw_thread)

    lam = tmp[tmp['client'] == client]['mw_th'].values[0]

    print(mu_mw, mu_worker, lam)
    print(1 / mu_mw, 1 / mu_worker)


def main():
    df = pd.read_csv('stats_set.csv')
    compute(df)


if __name__ == '__main__':
    CLIENTS = (1, 2, 4, 8, 14, 22, 42, 64)
    MW_THREAD = (8, 16, 32, 64)
    RUN = ('1', '2', '3')
    main()
