#!/usr/bin/env bash

#CLIENTS=6
THREADS=2
SERVER='localhost'
PORT=11211

#--multi-key-get=10

clients=( 2 4 6 8 10 12 14 16 18 20 22 24 26 28 30 32 34 )
run=( 1 2 3 )

for client in "${clients[@]}"
do
#    memtier_benchmark -s ${SERVER} --port=${PORT} --clients=${client} --threads=${THREADS} --protocol=memcache_text --ratio=1:10 --expiry-range=9999-10000 --key-maximum=1000 --hide-histogram -D
    # set mode
    memtier_benchmark -s ${SERVER} --port=${PORT} --clients=${client} --threads=${THREADS} --protocol=memcache_text --ratio=1:0 --test-time=120 --key-maximum=1000 \
    --expiry-range=15000-20000 --hide-histogram --out-file=log-set-${client}.txt --client-stats=log-set.client-${client}

    # get mode
#    memtier_benchmark -s ${SERVER} --port=${PORT} --clients=${client} --threads=${THREADS} --protocol=memcache_text --ratio=0:1 --key-maximum=1000 \
#    --expiry-range=9999-10000 --hide-histogram -D --out-file=log-get-${client}.txt --client-stats=log-get.client-${client}

done


