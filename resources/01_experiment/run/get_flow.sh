#!/usr/bin/env bash

CLIENT1=storeml4kjdlgl3y3csshpublicip1.westeurope.cloudapp.azure.com
CLIENT2=storeml4kjdlgl3y3csshpublicip2.westeurope.cloudapp.azure.com
CLIENT3=storeml4kjdlgl3y3csshpublicip3.westeurope.cloudapp.azure.com

SERVER1=storeml4kjdlgl3y3csshpublicip6.westeurope.cloudapp.azure.com
SERVER1_PORT=11211
SERVER1_PRIVATE=10.0.0.11

THREADS=2
TEST_TIME=70
DSTAT_TIME=60
DATA_SIZE=4096


SET_RATIO=0
GET_RATIO=1



client=42


CMD_SRC="sudo tcpdump -i any src port 11211 -v -X -c 1000000 -s 30 -n -w src_${client}.cap"
CMD_DST="sudo tcpdump -i any src port 11211 -v -X -c 1000000 -s 30 -n -w dst_${client}.cap"

PARAMS="-s ${SERVER1_PRIVATE} --port=${SERVER1_PORT} --protocol=memcache_text --key-maximum=1000 --expiry-range=15000-20000 --hide-histogram --data-size=${DATA_SIZE} --clients=${client} --threads=${THREADS} --test-time=${TEST_TIME} --ratio=${SET_RATIO}:${GET_RATIO} --out-file=log-${client}.txt"
CMD="/home/maki/memtier_benchmark-master/memtier_benchmark ${PARAMS}"

echo $CMD
#echo "run=$experiment; client=$client"
ssh ${SERVER1} "${CMD_SRC}" &
ssh ${SERVER1} "${CMD_DST}" &

ssh ${CLIENT1} "${CMD}" &
ssh ${CLIENT2} "${CMD}" &
ssh ${CLIENT3} "${CMD}" &


sleep ${TEST_TIME}
wait
echo "DONE"

scp ${SERVER1}:src_${client}.cap src_${client}.cap
scp ${SERVER1}:dst_${client}.cap dst_${client}.cap
