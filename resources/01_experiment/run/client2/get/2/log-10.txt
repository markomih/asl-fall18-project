2         Threads
10        Connections per thread
70        Seconds


ALL STATS
========================================================================
Type        Ops/sec     Hits/sec   Misses/sec      Latency       KB/sec 
------------------------------------------------------------------------
Sets           0.00          ---          ---      0.00000         0.00 
Gets        1061.60      1061.60         0.00     18.83500      4300.09 
Waits          0.00          ---          ---      0.00000          --- 
Totals      1061.60      1061.60         0.00     18.83500      4300.09 


Request Latency Distribution
Type     <= msec         Percent
------------------------------------------------------------------------
---
GET       0.290         0.00
GET       0.300         0.00
GET       0.310         0.00
GET       0.320         0.01
GET       0.340         0.01
GET       0.390         0.01
GET       0.430         0.01
GET       0.440         0.01
GET       0.450         0.01
GET       0.460         0.02
GET       0.470         0.02
GET       0.480         0.02
GET       0.490         0.03
GET       0.500         0.03
GET       0.510         0.03
GET       0.520         0.04
GET       0.530         0.04
GET       0.540         0.04
GET       0.550         0.05
GET       0.560         0.06
GET       0.570         0.06
GET       0.580         0.06
GET       0.590         0.07
GET       0.600         0.08
GET       0.610         0.08
GET       0.620         0.10
GET       0.630         0.10
GET       0.640         0.11
GET       0.650         0.12
GET       0.660         0.13
GET       0.670         0.13
GET       0.680         0.14
GET       0.690         0.15
GET       0.700         0.15
GET       0.710         0.16
GET       0.720         0.18
GET       0.730         0.19
GET       0.740         0.20
GET       0.750         0.21
GET       0.760         0.22
GET       0.770         0.23
GET       0.780         0.24
GET       0.790         0.25
GET       0.800         0.25
GET       0.810         0.26
GET       0.820         0.26
GET       0.830         0.27
GET       0.840         0.28
GET       0.860         0.28
GET       0.870         0.29
GET       0.880         0.30
GET       0.890         0.30
GET       0.900         0.31
GET       0.910         0.31
GET       0.920         0.32
GET       0.930         0.32
GET       0.940         0.32
GET       0.950         0.34
GET       0.960         0.34
GET       0.970         0.34
GET       0.980         0.35
GET       0.990         0.35
GET       1.000         0.37
GET       1.100         0.40
GET       1.200         0.42
GET       1.300         0.44
GET       1.400         0.51
GET       1.500         0.56
GET       1.600         0.59
GET       1.700         0.60
GET       1.800         0.61
GET       1.900         0.61
GET       2.200         0.61
GET       2.300         0.62
GET       2.500         0.62
GET       3.000         0.62
GET       3.100         0.63
GET       3.900         0.63
GET       4.000         0.63
GET       4.100         0.64
GET       4.200         0.64
GET       4.300         0.64
GET       4.400         0.64
GET       4.700         0.65
GET       4.800         0.65
GET       6.800         0.65
GET       7.500         0.65
GET       8.100         0.66
GET       8.400         0.66
GET       8.600         0.66
GET       8.800         0.67
GET       9.200         0.67
GET       9.400         0.67
GET       9.500         0.67
GET       9.800         0.67
GET       9.900         0.68
GET      10.000         0.68
GET      11.000         0.74
GET      12.000         0.79
GET      13.000         0.91
GET      14.000         1.29
GET      15.000        17.46
GET      16.000        83.24
GET      17.000        83.71
GET      18.000        83.88
GET      19.000        84.02
GET      20.000        84.13
GET      21.000        84.15
GET      22.000        84.16
GET      23.000        84.17
GET      24.000        84.18
GET      25.000        84.18
GET      26.000        84.20
GET      27.000        84.27
GET      28.000        84.35
GET      29.000        84.48
GET      30.000        84.83
GET      31.000        95.80
GET      32.000        96.19
GET      33.000        96.23
GET      34.000        96.25
GET      35.000        96.26
GET      36.000        96.27
GET      37.000        96.28
GET      38.000        96.28
GET      40.000        96.28
GET      42.000        96.29
GET      44.000        96.30
GET      45.000        96.32
GET      46.000        96.96
GET      47.000        98.90
GET      48.000        98.90
GET      49.000        98.90
GET      50.000        98.91
GET      51.000        98.91
GET      56.000        98.91
GET      61.000        98.91
GET      62.000        99.59
GET      63.000        99.70
GET      64.000        99.70
GET      65.000        99.71
GET      77.000        99.72
GET      78.000        99.88
GET      79.000        99.88
GET      91.000        99.89
GET      92.000        99.89
GET      93.000        99.91
GET      94.000        99.94
GET     110.000        99.96
GET     120.000        99.98
GET     130.000        99.98
GET     160.000        99.99
GET     220.000       100.00
---
