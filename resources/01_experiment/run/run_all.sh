#!/usr/bin/env bash


########## SETs


CLIENT1=storeml4kjdlgl3y3csshpublicip1.westeurope.cloudapp.azure.com
CLIENT2=storeml4kjdlgl3y3csshpublicip2.westeurope.cloudapp.azure.com
CLIENT3=storeml4kjdlgl3y3csshpublicip3.westeurope.cloudapp.azure.com

SERVER1=storeml4kjdlgl3y3csshpublicip6.westeurope.cloudapp.azure.com
SERVER1_PORT=11211
SERVER1_PRIVATE=10.0.0.11


TEST_TIME=70
THREADS=2
DATA_SIZE=4096
KEY_MAX=10000

#Populate db
PARAMS="-s ${SERVER1_PRIVATE} --port=${SERVER1_PORT} --protocol=memcache_text --key-maximum=${KEY_MAX} --data-size=${DATA_SIZE} --clients=100 --threads=${THREADS} --test-time=100 --ratio=1:0"
CMD="/home/maki/memtier_benchmark-master/memtier_benchmark ${PARAMS}"
$(ssh ${CLIENT1} "${CMD}") &
$(ssh ${CLIENT2} "${CMD}") &
$(ssh ${CLIENT3} "${CMD}") &
wait
echo "DB populated"

SERVER_DIR="01_experiment"

DSTAT_TIME=60

SET_RATIO=1
GET_RATIO=0
clients=( 1 2 4 6 8 10 14 22 32 42 64 )
run=( 1 2 3 )

TYPE="set"

ssh ${SERVER1} "rm *.csv"
# prepare directories
for experiment in "${run[@]}"
do
        MKDIR="mkdir -p ${SERVER_DIR}/${TYPE}/${experiment}"
        RMFILES="rm ${SERVER_DIR}/${TYPE}/${experiment}/*"
        ssh ${CLIENT1} "${MKDIR};${RMFILES}"
        ssh ${CLIENT2} "${MKDIR};${RMFILES}"
        ssh ${CLIENT3} "${MKDIR};${RMFILES}"
done


for experiment in "${run[@]}"
do
    for client in "${clients[@]}"
    do
        CDDIR="cd ${SERVER_DIR}/${TYPE}/${experiment}"
        PARAMS="-s ${SERVER1_PRIVATE} --port=${SERVER1_PORT} --protocol=memcache_text --key-maximum=${KEY_MAX} --data-size=${DATA_SIZE} --clients=${client} --threads=${THREADS} --test-time=${TEST_TIME} --ratio=${SET_RATIO}:${GET_RATIO} --out-file=log-${client}.txt"
        CMD="/home/maki/memtier_benchmark-master/memtier_benchmark ${PARAMS}"

        echo $CMD
        echo "run=$experiment; client=$client"
        $(ssh ${CLIENT1} "${CDDIR};${CMD};") &
        $(ssh ${CLIENT2} "${CDDIR};${CMD};") &
        $(ssh ${CLIENT3} "${CDDIR};${CMD};") &

        # RUN Dstat Server 1
        DSTAT_CMD_SERVER1="dstat --output server1_${TYPE}_${experiment}_${client}.csv 1 ${DSTAT_TIME}"
        $(ssh ${SERVER1} "${DSTAT_CMD_SERVER1}") &

        echo "WAITING"
        wait
        echo "DONE"
        echo

    done
done


# GETs
SET_RATIO=0
GET_RATIO=1
#clients=( 1 2 4 6 8 10 14 22 32 42 64 )
TYPE="get"

# prepare directories
for experiment in "${run[@]}"
do
        MKDIR="mkdir -p ${SERVER_DIR}/${TYPE}/${experiment}"
        RMFILES="rm ${SERVER_DIR}/${TYPE}/${experiment}/*"
        ssh ${CLIENT1} "${MKDIR};${RMFILES}"
        ssh ${CLIENT2} "${MKDIR};${RMFILES}"
        ssh ${CLIENT3} "${MKDIR};${RMFILES}"
done



for experiment in "${run[@]}"
do
    for client in "${clients[@]}"
    do
        CDDIR="cd ${SERVER_DIR}/${TYPE}/${experiment}"
        PARAMS="-s ${SERVER1_PRIVATE} --port=${SERVER1_PORT} --protocol=memcache_text --key-maximum=${KEY_MAX} --data-size=${DATA_SIZE} --clients=${client} --threads=${THREADS} --test-time=${TEST_TIME} --ratio=${SET_RATIO}:${GET_RATIO} --out-file=log-${client}.txt"
        CMD="/home/maki/memtier_benchmark-master/memtier_benchmark ${PARAMS}"

        echo $CMD
        echo "run=$experiment; client=$client"
        $(ssh ${CLIENT1} "${CDDIR};${CMD};") &
        $(ssh ${CLIENT2} "${CDDIR};${CMD};") &
        $(ssh ${CLIENT3} "${CDDIR};${CMD};") &

        # RUN Dstat Server 1
        DSTAT_CMD_SERVER1="dstat --output server1_${TYPE}_${experiment}_${client}.csv 1 ${DSTAT_TIME}"
        $(ssh ${SERVER1} "${DSTAT_CMD_SERVER1}") &

        echo "WAITING"
        wait
        echo "DONE"
        echo

    done
done


# copy files
mkdir -p server1
scp -r ${SERVER1}:*.csv ./server1/

#scp ${SERVER1}:stats.txt server1.stats.txt
scp -r ${CLIENT1}:01_experiment client1
scp -r ${CLIENT2}:01_experiment client2
scp -r ${CLIENT3}:01_experiment client3


echo "stopping VMS all"
# STOP all

# STOP VMs

az vm stop --resource-group asl-group --name Server1
az vm stop --resource-group asl-group --name Client1
az vm stop --resource-group asl-group --name Client2
az vm stop --resource-group asl-group --name Client3

az vm deallocate --resource-group asl-group --name Server1
az vm deallocate --resource-group asl-group --name Client1
az vm deallocate --resource-group asl-group --name Client2
az vm deallocate --resource-group asl-group --name Client3
